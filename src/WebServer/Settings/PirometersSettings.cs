﻿using System;

namespace WebServer.Settings
{
    public class PirometersSettings
    {

        public int PirNumber { get; set; }
        public float DetectionKoeff { get; set; }
        public float TemperatureKoeff { get; set; }
        public bool ScanLineReverse { get; set; }
        public bool ForewardDistanceReverse { get; set; }
        public bool BackwardDistanceReverse { get; set; }
        public bool  InscribeRectArea { get; set; }
        public int DistanceToIngot { get; set; }
        public int RectIncrementEachSize { get; set; }
        public int ScanAngleDegree { get; set; }
        public int Tmin { get; set; }
        public int Tmax { get; set; }


        public float GetXCoord(int i)
        {
            float XCoord;
            var halfWidth =  (float)(((float)DistanceToIngot + 35) * Math.Tan((ScanAngleDegree * 0.5) * Math.PI / 180));
            
            if (ScanLineReverse)
            {
                XCoord = (float)(((float)DistanceToIngot + 35) * Math.Tan((ScanAngleDegree * (0.5 -  0.001 * (i+1))) * Math.PI / 180)) + halfWidth;
            }
            else
            {
                XCoord = (float)(((float)DistanceToIngot + 35) * Math.Tan((ScanAngleDegree * (-0.5 + 0.001 * i)) * Math.PI / 180)) + halfWidth;
            }
            return XCoord;
        }
    }
}
