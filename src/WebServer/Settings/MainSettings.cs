﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebServer.Settings
{
    public class MainSettings
    {
        public bool InitDB { get; set; }
        public bool CommaFormat { get; set; }
        public bool SavePreviewCSVinSCANS { get; set; }
        public string filelocation { get; set; }
        public int CountOfFilesToStay { get; set; }
        public int CountOfDaysInDBToStay { get; set; }
        public int CountOfDaysPicsInDBToStay { get; set; }
        public int CountOfDaysInDBScansToStay { get; set; }
        public string pngfilelocation { get; set; }
        public string LandScanServerIP { get; set; }
        public int LandScanServerPort { get; set; }
        public int PdiExchangeTimeIntervalMs { get; set; }
        public int PdiTryConts { get; set; }
    }   
}
