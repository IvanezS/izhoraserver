﻿using Mapster;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Data.Models;
using Data.Repositories;
using System.Collections.Generic;
using System.Net.Mime;
using System.Threading.Tasks;
using WebServer.Models;

namespace WebServer.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ScansController : ControllerBase
    {
        private readonly ScanRepository _scan;

        public ScansController(ScanRepository scan)
        {
            _scan = scan;
        }

        /// <summary>
        /// Получить список сканов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<IEnumerable<ScansShortResponse>>> GetList()
        {
            var list = await _scan.GetAllAsync();
            if (list == null) return NotFound();
            var resp = list.Adapt(new List<ScansShortResponse>());//
            return Ok(resp);
        }

        /// <summary>
        /// Получить скан по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetFiltered")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<IEnumerable<ScansShortResponse>>> GetListFiltered(bool mat_id_option, bool pir_num_option, int mat_id, int pir_num)
        {
            var item = await _scan.GetFilteredAsync(mat_id_option, pir_num_option, mat_id, pir_num);
            if (item == null) return NotFound();
            var resp = item.Adapt(new List<ScansShortResponse>());//
            return Ok(resp);
        }

        /// <summary>
        /// Получить скан по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<Scan>> GetById(int id)
        {
            var item = await _scan.GetByIdAsync(id);
            if (item == null) return NotFound();
            return Ok(item);
        }


        /// <summary>
        /// Возвращаем картинку с изначальной термограммой из SCANS по Id
        /// </summary>
        /// <param name="scan_Id">номер сканирования</param>
        /// <returns></returns>
        [HttpGet("GetScanPict/{scan_Id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetScanPict(int scan_Id)
        {
            var scan = await _scan.GetByIdAsync(scan_Id);
            if (scan != null) return File(scan.PICT_DEFAULT, "image/jpg");
            return NotFound();
        }

        /// <summary>
        /// Удалить скан
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<Scan>> DeleteById(int id)
        {
            try
            {
                await _scan.DeleteAsync(id);
            }
            catch
            {
                return NotFound();
            }
            return Ok();
        }

        /// <summary>
        /// Создать скан
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        public async Task<IActionResult> CreateAsync (Scan s)
        {
            await _scan.CreateAsync(s);
            return CreatedAtAction(nameof(GetById), new { id = s.ID }, s);
        }

        /// <summary>
        /// Изменить скан
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<Scan>> UpdateById(Scan s)
        {
            try
            {
                await _scan.UpdateAsync(s);
            }
            catch
            {
                return NotFound();
            }
            return Ok();
        }

    }
}
