﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Data.Models;
using Data.Repositories;
using System.Collections.Generic;
using System.Net.Mime;
using System.Threading.Tasks;
using WebServer.Models;
using WebServer.Settings;

namespace WebServer.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AreasSettingsController : ControllerBase
    {
        private readonly IRepository<AreasSettings> _arset;

        public AreasSettingsController(IRepository<AreasSettings> arset)
        {
            _arset = arset;
        }

        /// <summary>
        /// Получить список настроек областей
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<IEnumerable<AreasSettings>>> GetList()
        {
            var list = await _arset.GetAllAsync();
            if (list == null) return NotFound();
            return Ok(list);
        }

        /// <summary>
        /// Получить настройку областей
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<AreasSettings>> GetById(int id)
        {
            var item = await _arset.GetByIdAsync(id);
            if (item == null) return NotFound();
            return Ok(item);
        }

        /// <summary>
        /// Удалить настройку областей
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<AreasSettings>> DeleteById(int id)
        {
            try
            {
                await _arset.DeleteAsync(id);
            }
            catch
            {
                return NotFound();
            }
            return Ok();
        }

        /// <summary>
        /// Создать настройку областей
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> CreateAsync (AreasSettings s)
        {

            List<AreasSettings> list = (List<AreasSettings>)await _arset.GetAllAsync();
            bool isExist = list.Exists(x => x.ARIA_COL_QUANT == s.ARIA_COL_QUANT && x.ARIA_ROW_QUANT == s.ARIA_ROW_QUANT);
            bool valid = s.ARIA_COL_QUANT >= 1 && s.ARIA_COL_QUANT <= 10 && s.ARIA_ROW_QUANT >= 1 && s.ARIA_ROW_QUANT <= 10;
            //Не консистентно, но в силу малого кол-ва клиентов соизволили
            if (!isExist && valid)
            {
                await _arset.CreateAsync(s);
                return CreatedAtAction(nameof(GetById), new { id = s.ID }, s);
            }
            else
            {
                return BadRequest("Введены некорректные данные");
            }

        }

        /// <summary>
        /// Изменить настройку областей
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<AreasSettings>> UpdateById(AreasSettings s)
        {
            try
            {
                await _arset.UpdateAsync(s);
            }
            catch
            {
                return NotFound();
            }
            return Ok();
        }

    }
}
