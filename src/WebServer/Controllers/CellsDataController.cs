﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Data.Models;
using Data.Repositories;
using System.Collections.Generic;
using System.Net.Mime;
using System.Threading.Tasks;
using WebServer.Models;
using WebServer.Settings;

namespace WebServer.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CellsDataController : ControllerBase
    {
        private readonly IRepository<CellsData> _celdata;

        public CellsDataController(IRepository<CellsData> celdata)
        {
            _celdata = celdata;
        }

        /// <summary>
        /// Получить список записей ячеек
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<IEnumerable<CellsData>>> GetList()
        {
            var list = await _celdata.GetAllAsync();
            if (list == null) return NotFound();
            return Ok(list);
        }

        /// <summary>
        /// Получить запись ячеек
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<CellsData>> GetById(int id)
        {
            var item = await _celdata.GetByIdAsync(id);
            if (item == null) return NotFound();
            return Ok(item);
        }

        /// <summary>
        /// Удалить запись ячеек
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<CellsData>> DeleteById(int id)
        {
            try
            {
                await _celdata.DeleteAsync(id);
            }
            catch
            {
                return NotFound();
            }
            return Ok();
        }

        /// <summary>
        /// Создать запись ячеек
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        public async Task<IActionResult> CreateAsync (CellsData s)
        {
            await _celdata.CreateAsync(s);
            return CreatedAtAction(nameof(GetById), new { id = s.ID }, s);
        }

        /// <summary>
        /// Изменить запись ячеек
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<CellsData>> UpdateById(CellsData s)
        {
            try
            {
                await _celdata.UpdateAsync(s);
            }
            catch
            {
                return NotFound();
            }
            return Ok();
        }

    }
}
