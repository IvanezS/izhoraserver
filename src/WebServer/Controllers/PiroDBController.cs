﻿using Microsoft.AspNetCore.ApiAuthorization.IdentityServer;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PLC_S7_Exchange.Data;
using System.Threading.Tasks;
using WebServer.Models;

namespace WebServer.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PiroDBController : ControllerBase
    {
        //private readonly PiroDataBlock _pdb;
        private readonly TimedHostedService _ths;
        private readonly PDIHostedService _phs;

        public PiroDBController(TimedHostedService ths, PDIHostedService phs)
        {
            _ths = ths;
            _phs = phs;

        }


        /// <summary>
        /// Получить датаблок из PLC ULS
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetPLCDB")]
        public async Task<ActionResult<PiroDataBlock>> GetAsync()
        {
            
            return Ok(_ths.GetPiroDataBlock());
        }


        /// <summary>
        /// Получить датаблок из PLC ULS
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetStatus")]
        public async Task<ActionResult<StatusResponse>> GetStatus()
        {

            StatusResponse sr = new StatusResponse()
            {
                OPCconnectOK = true,
                PLCconnectOK = _ths.PlcConnectOK(),
                WSconnectOK = _phs.LandConnectOK()
            };

            return Ok(sr);
        }


    }
}
