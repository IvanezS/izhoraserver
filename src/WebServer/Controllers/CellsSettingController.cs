﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Data.Models;
using Data.Repositories;
using System.Collections.Generic;
using System.Net.Mime;
using System.Threading.Tasks;
using WebServer.Models;
using WebServer.Settings;

namespace WebServer.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CellsSettingsController : ControllerBase
    {
        private readonly IRepository<CellSettings> _celset;

        public CellsSettingsController(IRepository<CellSettings> celset)
        {
            _celset = celset;
        }

        /// <summary>
        /// Получить список настроек ячеек
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<IEnumerable<CellSettings>>> GetList()
        {
            var list = await _celset.GetAllAsync();
            if (list == null) return NotFound();
            return Ok(list);
        }

        /// <summary>
        /// Получить настройку ячеек
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<CellSettings>> GetById(int id)
        {
            var item = await _celset.GetByIdAsync(id);
            if (item == null) return NotFound();
            return Ok(item);
        }

        /// <summary>
        /// Удалить настройку ячеек
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<CellSettings>> DeleteById(int id)
        {
            try
            {
                await _celset.DeleteAsync(id);
            }
            catch
            {
                return NotFound();
            }
            return Ok();
        }

        /// <summary>
        /// Создать настройку ячеек
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> CreateAsync (CellSettings s)
        {
            List<CellSettings> list = (List<CellSettings>)await _celset.GetAllAsync();
            bool isExist = list.Exists(x => x.CELL_HEIGHT == s.CELL_HEIGHT && x.CELL_WIDTH == s.CELL_WIDTH);
            bool valid = s.CELL_HEIGHT >= 10 && s.CELL_HEIGHT <= 300 && s.CELL_WIDTH >= 10 && s.CELL_WIDTH <= 300;
            //Не консистентно, но в силу малого кол-ва клиентов соизволили
            if (!isExist && valid)
            {
                await _celset.CreateAsync(s);
                return CreatedAtAction(nameof(GetById), new { id = s.ID }, s);
            }
            else
            {
                return BadRequest("Введены некорректные данные");
            }
        }

        /// <summary>
        /// Изменить настройку ячеек
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<CellSettings>> UpdateById(CellSettings s)
        {
            try
            {
                await _celset.UpdateAsync(s);
            }
            catch
            {
                return NotFound();
            }
            return Ok();
        }

    }
}
