﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using WebServer.Models;

namespace WebServer.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TFieldsController : ControllerBase
    {
        private readonly WaitCSVHostedService _lhs;

        public TFieldsController(WaitCSVHostedService lhs)
        {
            _lhs = lhs;
        }

        /// <summary>
        /// Обновление ячеек в соответствии с актуальными настройками пирометра данного скана
        /// </summary>
        /// <param name="scan_Id"></param>
        /// <returns></returns>
        [HttpPost("UpdCalk/{scan_Id}")]
        public async Task<IActionResult> UpdCalkAsync(int scan_Id)
        {
            await _lhs.UpdateAvarageTempFieldAsync(scan_Id);
            return Ok();
        }
        
    }
}
