﻿using Mapster;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Data.Abstractions;
using Data.Models;
using Data.Repositories;
using System.Collections.Generic;
using System.Net.Mime;
using System.Threading.Tasks;
using WebServer.Models;

namespace WebServer.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CalkDataController : ControllerBase
    {
        private readonly CalkDataRepository _calkdata;
        private readonly Funcs _funcs;

        public CalkDataController(CalkDataRepository calkdata, Funcs funcs)
        {
            _calkdata = calkdata;
            _funcs = funcs;
        }

        /// <summary>
        /// Получить список расчётов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<IEnumerable<CalkData>>> GetList()
        {
            var list = await _calkdata.GetAllAsync();
            if (list == null) return NotFound();
            return Ok(list);
        }

        /// <summary>
        /// Получить список расчётов по id сканирования
        /// </summary>
        /// <param name="scan_Id">Id сканирования</param>
        /// <returns></returns>
        [HttpGet("GetByScan/{scan_Id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<IEnumerable<CalkData>>> GetListScan(int scan_Id)
        {
            var list = await _calkdata.GetByScanIdAsync(scan_Id);
            if (list == null) return NotFound();
            return Ok(list);
        }


        /// <summary>
        /// Получить список расчётов по id прямоугольника заготовки
        /// </summary>
        /// <param name="rect_Id">Id сканирования</param>
        /// <returns></returns>
        [HttpGet("GetByRect/{rect_Id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<IEnumerable<CalkData>>> GetListRect(int rect_Id)
        {
            var list = await _calkdata.GetByRectIdAsync(rect_Id);
            if (list == null) return NotFound();
            return Ok(list);
        }



        /// <summary>
        /// Получить список расчётов по id ячеек заготовки
        /// </summary>
        /// <param name="celldata_Id">Id сканирования</param>
        /// <returns></returns>
        [HttpGet("GetByCell/{celldata_Id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<IEnumerable<CalkData>>> GetListCell(int celldata_Id)
        {
            var list = await _calkdata.GetByCellIdAsync(celldata_Id);
            if (list == null) return NotFound();
            return Ok(list);
        }



        /// <summary>
        /// Получить список расчётов по id настройки
        /// </summary>
        /// <param name="set_Id">Id сканирования</param>
        /// <returns></returns>
        [HttpGet("GetBySet/{set_Id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<IEnumerable<CalkData>>> GetListSet(int set_Id)
        {
            var list = await _calkdata.GetBySettings_IdAsync(set_Id);
            if (list == null) return NotFound();
            return Ok(list);
        }

        /// <summary>
        /// Получить расчёт по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<CalkData>> GetById(int id)
        {
            var item = await _calkdata.GetByIdAsync(id);
            if (item == null) return NotFound();
            return Ok(item);
        }

        /// <summary>
        /// Удалить расчёт по Id
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<CalkData>> DeleteById(int id)
        {
            try
            {
                await _calkdata.DeleteAsync(id);
            }
            catch
            {
                return NotFound();
            }
            return Ok();
        }

        /// <summary>
        /// Создать расчёт
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        public async Task<IActionResult> CreateAsync (CreateOrEditCalkData s)
        {
            await _calkdata.CreateAsync(s.Adapt(new CalkData()));
            return CreatedAtAction(nameof(GetById), new { id = s.Id }, s);
        }

        /// <summary>
        /// Изменить расчёт по Id
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<CalkData>> UpdateById(CreateOrEditCalkData s)
        {
            try
            {
                await _calkdata.UpdateAsync(s.Adapt(new CalkData()));
            }
            catch
            {
                return NotFound();
            }
            return Ok();
        }

        /// <summary>
        /// Возвращаем картинку с определённой заготовкой по номеру расчёта
        /// </summary>
        /// <param name="calk_data_Id">номер расчёта</param>
        /// <returns></returns>
        [HttpGet("GetRectPict/{calk_data_Id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetRectPictAsync(int calk_data_Id)
        {

            var cd = await _calkdata.GetByIdAsync(calk_data_Id);
            if (cd != null) return File(cd.RECTDATA.PICT_RECT, "image/jpg");
            return NotFound();

        }

        /// <summary>
        /// Возвращаем закодированные вектора заготовки по номеру расчёта
        /// </summary>
        /// <param name="calk_data_Id">номер расчёта</param>
        /// <returns></returns>
        [HttpGet("GetRect/{calk_data_Id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetRectAsync(int calk_data_Id)
        {
            var cd = await _calkdata.GetByIdAsync(calk_data_Id);
            if (cd != null) return Ok(cd.RECTDATA.RECT_VECTORS);
            return NotFound();
        }

        /// <summary>
        /// Возвращаем декодированные вектора заготовки по номеру расчёта
        /// </summary>
        /// <param name="calk_data_Id">номер расчёта</param>
        /// <returns></returns>
        [HttpGet("GetRectDecode/{calk_data_Id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<IEnumerable<Vector3Ser>>> GetRectDecodeAsync(int calk_data_Id)
        {
            var cd = await _calkdata.GetByIdAsync(calk_data_Id);
            if (cd == null) return NotFound();
            var v = _funcs.DecodeByteArray444(cd.RECTDATA.RECT_VECTORS);
            var vv = v.Adapt(new List<Vector3Ser>());
            return Ok(vv);
        }

        /// <summary>
        /// Возвращаем картинку с ячейками по номеру расчёта
        /// </summary>
        /// <param name="calk_data_Id">номер расчёта</param>
        /// <returns></returns>
        [HttpGet("GetCellsPict/{calk_data_Id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetCellsPictAsync(int calk_data_Id)
        {
            var cd = await _calkdata.GetByIdAsync(calk_data_Id);
            if (cd != null) return File(cd.CELLSDATA.PICT_CELLS, "image/jpg");
            return NotFound();
        }

        /// <summary>
        /// Возвращаем закодированные вектора ячеек по номеру расчёта
        /// </summary>
        /// <param name="calk_data_Id">номер расчёта</param>
        /// <returns></returns>
        [HttpGet("GetCells/{calk_data_Id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetCellsAsync(int calk_data_Id)
        {
            var cd = await _calkdata.GetByIdAsync(calk_data_Id);
            if (cd != null) return Ok(cd.CELLSDATA.CELLS_VECTORS);
            return NotFound();
        }

        /// <summary>
        /// Возвращаем декодированные вектора ячеек по номеру расчёта
        /// </summary>
        /// <param name="calk_data_Id">номер расчёта</param>
        /// <returns></returns>
        [HttpGet("GetCellsDecoded/{calk_data_Id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<IEnumerable<Vector3Ser>>> GetCellsDecodedAsync(int calk_data_Id)
        {
            var cd = await _calkdata.GetByIdAsync(calk_data_Id);
            if (cd == null) return NotFound();
            var v = _funcs.DecodeByteArray224(cd.CELLSDATA.CELLS_VECTORS);
            var vv = v.Adapt(new List<Vector3Ser>());
            return Ok(vv);

        }


    }
}
