﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Data.Models;
using Data.Repositories;
using System.Collections.Generic;
using System.Net.Mime;
using System.Threading.Tasks;
using WebServer.Models;
using WebServer.Settings;

namespace WebServer.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CutSettingsController : ControllerBase
    {
        private readonly IRepository<CutSettings> _cutset;

        public CutSettingsController(IRepository<CutSettings> cutset)
        {
            _cutset = cutset;
        }

        /// <summary>
        /// Получить список настроек обрезки прямоугольника заготовки
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<IEnumerable<CutSettings>>> GetList()
        {
            var list = await _cutset.GetAllAsync();
            if (list == null) return NotFound();
            return Ok(list);
        }

        /// <summary>
        /// Получить настройку обрезки прямоугольника заготовки
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<CutSettings>> GetById(int id)
        {
            var item = await _cutset.GetByIdAsync(id);
            if (item == null) return NotFound();
            return Ok(item);
        }

        /// <summary>
        /// Удалить настройку обрезки прямоугольника заготовки
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<CutSettings>> DeleteById(int id)
        {
            try
            {
                await _cutset.DeleteAsync(id);
            }
            catch
            {
                return NotFound();
            }
            return Ok();
        }

        /// <summary>
        /// Создать настройку обрезки прямоугольника заготовки
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> CreateAsync (CutSettings s)
        {
            List<CutSettings> list = (List<CutSettings>)await _cutset.GetAllAsync();
            bool isExist = list.Exists(x => x.CUT_BOTTOM == s.CUT_BOTTOM && x.CUT_LEFT == s.CUT_LEFT && x.CUT_RIGHT == s.CUT_RIGHT && x.CUT_TOP == s.CUT_TOP);
            bool valid = s.CUT_TOP >= 0 && s.CUT_TOP <= 1000 && s.CUT_RIGHT >= 0 && s.CUT_RIGHT <= 1000 && s.CUT_LEFT >= 0 && s.CUT_LEFT <= 1000 && s.CUT_BOTTOM >= 0 && s.CUT_BOTTOM <= 1000;
            //Не консистентно, но в силу малого кол-ва клиентов соизволили
            if (!isExist && valid)
            {
                await _cutset.CreateAsync(s);
                return CreatedAtAction(nameof(GetById), new { id = s.ID }, s);
            }
            else
            {
                return BadRequest("Введены некорректные данные");
            }

        }

        /// <summary>
        /// Изменить настройку обрезки прямоугольника заготовки
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<CutSettings>> UpdateById(CutSettings s)
        {
            try
            {
                await _cutset.UpdateAsync(s);
            }
            catch
            {
                return NotFound();
            }
            return Ok();
        }

    }
}
