﻿using Mapster;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Data.Models;
using Data.Repositories;
using System.Collections.Generic;
using System.Net.Mime;
using System.Threading.Tasks;
using WebServer.Models;

namespace WebServer.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AreasDataController : ControllerBase
    {
        private readonly AreasDataRepository _ardata;

        public AreasDataController(AreasDataRepository ardata)
        {
            _ardata = ardata;
        }

        /// <summary>
        /// Получить список областей
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<IEnumerable<AreasData>>> GetList()
        {
            var list = await _ardata.GetAllAsync();
            if (list == null) return NotFound();
            return Ok(list);
        }

        /// <summary>
        /// Получить список областей
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetByCalk/{CalkData_Id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<IEnumerable<AreasData>>> GetList(int CalkData_Id)
        {
            var list = await _ardata.GetWithCalkAsync(CalkData_Id);
            if (list == null) return NotFound();
            return Ok(list);
        }

        /// <summary>
        /// Получить область
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<AreasData>> GetById(int id)
        {
            var item = await _ardata.GetByIdAsync(id);
            if (item == null) return NotFound();
            return Ok(item);
        }

        /// <summary>
        /// Удалить области
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<AreasData>> DeleteById(int id)
        {
            try
            {
                await _ardata.DeleteAsync(id);
            }
            catch
            {
                return NotFound();
            }
            return Ok();
        }

        /// <summary>
        /// Создать область
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        public async Task<IActionResult> CreateAsync (CreateOrEditAreasData s)
        {
            await _ardata.CreateAsync(s.Adapt(new AreasData()));
            return CreatedAtAction(nameof(GetById), new { id = s.ID }, s);
        }

        /// <summary>
        /// Изменить область
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<AreasData>> UpdateById(CreateOrEditAreasData s)
        {
            try
            {
                await _ardata.UpdateAsync(s.Adapt(new AreasData()));
            }
            catch
            {
                return NotFound();
            }
            return Ok();
        }

    }
}
