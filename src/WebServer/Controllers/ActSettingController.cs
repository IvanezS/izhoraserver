﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Data.Models;
using Data.Repositories;
using System.Collections.Generic;
using System.Net.Mime;
using System.Threading.Tasks;
using WebServer.Models;
using WebServer.Settings;

namespace WebServer.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ActSettingsController : ControllerBase
    {
        private readonly ActSettingsRepository _actset;

        public ActSettingsController(ActSettingsRepository actset)
        {
            _actset = actset;
        }

        /// <summary>
        /// Получить список настроек пирометра
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<IEnumerable<ActSettings>>> GetList()
        {
            var list = await _actset.GetAllAsync();
            if (list == null) return NotFound();
            return Ok(list);
        }

        /// <summary>
        /// Получить настроку пирометра по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<ActSettings>> GetById(int id)
        {
            var item = await _actset.GetByIdAsync(id);
            if (item == null) return NotFound();
            return Ok(item);
        }

        /// <summary>
        /// Получить настроку пирометра по CalkData_Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetByCalk/{CalkData_Id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<ActSettings>> GetByCalk(int CalkData_Id)
        {
            var item = await _actset.GetByCalkAsync(CalkData_Id);
            if (item == null) return NotFound();
            return Ok(item);
        }

        /// <summary>
        /// Удалить настроку пирометра
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<ActSettings>> DeleteById(int id)
        {
            try
            {
                await _actset.DeleteAsync(id);
            }
            catch
            {
                return NotFound();
            }
            return Ok();
        }

        /// <summary>
        /// Создать настройку пирометра
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        public async Task<IActionResult> CreateAsync (CreateOrEditActSettings s)
        {
            await _actset.CreateAsync(new ActSettings()
                {
                    PIR_NUMBER = s.PIR_NUMBER,
                    SETTINGS_ID = s.SETTINGS_ID
                }
            );
            return CreatedAtAction(nameof(GetById), new { id = s.ID }, s);
        }

        /// <summary>
        /// Изменить настройку пирометра
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<ActSettings>> UpdateById(CreateOrEditActSettings s)
        {
            try
            {
                await _actset.UpdateAsync(new ActSettings()
                    { 
                        ID = s.ID,
                        PIR_NUMBER = s.PIR_NUMBER,
                        SETTINGS_ID = s.SETTINGS_ID
                    }
                );
            }
            catch
            {
                return NotFound();
            }
            return Ok();
        }

    }
}
