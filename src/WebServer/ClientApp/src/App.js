import React, { Component } from 'react';
import { Route } from 'react-router';
import { Layout } from './components/Layout';
import { Home } from './components/Home';
import { FetchData } from './components/FetchData';
import { Counter } from './components/Counter';
import { TFields } from './components/TFields';
import { TFieldsList } from './components/TFieldsList';
import { SettingsPir } from './components/SettingsPir';
import AuthorizeRoute from './components/api-authorization/AuthorizeRoute';
import ApiAuthorizationRoutes from './components/api-authorization/ApiAuthorizationRoutes';
import { ApplicationPaths } from './components/api-authorization/ApiAuthorizationConstants';
import {toast} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import './custom.css'

toast.configure()


export default class App extends Component {
  static displayName = App.name;

  render () {
    return (
      <Layout>
        <Route exact path='/' component={Home} />
        <Route path='/counter' component={Counter} />
        <Route path='/TFields/:id' component={TFields} />
        <Route path='/TFieldsList' component={TFieldsList} />
        <Route path='/SettingsPir' component={SettingsPir} />
        {/* <AuthorizeRoute path='/TFields/:id' component={TFields} />
        <AuthorizeRoute path='/TFieldsList' component={TFieldsList} />
        <AuthorizeRoute path='/SettingsPir' component={SettingsPir} /> */}
        <AuthorizeRoute path='/fetch-data' component={FetchData} />
        <Route path={ApplicationPaths.ApiAuthorizationPrefix} component={ApiAuthorizationRoutes} />
      </Layout>
    );
  }
}
