import React, { Component } from 'react';
import { Collapse, Container, Navbar, NavbarBrand, NavbarToggler, NavItem, NavLink, Badge } from 'reactstrap';
import { Link } from 'react-router-dom';
import { LoginMenu } from './api-authorization/LoginMenu';
import './NavMenu.css';

export class NavMenu extends Component {
  static displayName = NavMenu.name;

  constructor (props) {
    super(props);

    this.toggleNavbar = this.toggleNavbar.bind(this);
    this.state = {
      collapsed: true,
      Status:{
        "plCconnectOK": false,
        "wSconnectOK": false,
        "opCconnectOK": false
      }
    };
  }

  componentDidMount() {
    this.interval = setInterval(() => { this.GetStatus(); }, 1000);
  }


  async GetStatus() {
    await fetch('/PiroDB/GetStatus')
    .then((res) => {
      res.json()
        .then((data) => {
          console.log("Status :", data)
          this.setState(prevState => ({ ...prevState, Status: data }))
        })

    })
  }



  toggleNavbar () {
    this.setState({
      collapsed: !this.state.collapsed
    });
  }

  render () {
    return (
      <header>
        <Navbar className="navbar-expand-sm navbar-toggleable-sm ng-white border-bottom box-shadow mb-3" light>
          <Container>
            <NavbarBrand tag={Link} to="/">ПО Интеграции</NavbarBrand>
            <NavbarToggler onClick={this.toggleNavbar} className="mr-2" />
            <Collapse className="d-sm-inline-flex flex-sm-row-reverse" isOpen={!this.state.collapsed} navbar>

              <ul className="navbar-nav flex-grow">
                <NavItem>
                  <NavLink tag={Link} className="text-dark" to="/TFieldsList">Список сканов</NavLink>
                </NavItem>
                <NavItem>
                  <NavLink tag={Link} className="text-dark" to="/SettingsPir">Настройки</NavLink>
                </NavItem>

                {/* <NavItem>
                  <NavLink tag={Link} className="text-dark" to="/counter">Counter</NavLink>
                </NavItem>
                <NavItem>
                  <NavLink tag={Link} className="text-dark" to="/fetch-data">Fetch data</NavLink>
                </NavItem> */}
                <LoginMenu>
                </LoginMenu>
              </ul>

            </Collapse>

          </Container>
          
           {this.state.Status.plCconnectOK ? "" : <Badge color="danger">PLC ULS</Badge> }
           {this.state.Status.opCconnectOK ? "" : <Badge color="danger">OPC DA</Badge> }
           {this.state.Status.wSconnectOK ? "" : <Badge color="danger">Landscan</Badge> }

        </Navbar>
      </header>
    );
  }
}
