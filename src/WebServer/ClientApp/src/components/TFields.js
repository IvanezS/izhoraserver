import React, { Component } from 'react';
import authService from './api-authorization/AuthorizeService'
import {
  Card, CardBody, Col, Row, Jumbotron, Button, Table, Spinner
} from 'reactstrap';
// Importing toastify module
import { toast } from 'react-toastify';

// Import toastify css file
import 'react-toastify/dist/ReactToastify.css';

// toast-configuration method, 
// it is compulsory method.
toast.configure()


export class TFields extends Component {

  constructor(props) {
    super(props);
    this.state = {
      image1: '1',
      image2: '2',
      loading1: true,
      loading2: true,
      loading3: true,
      loading: true,
      loadingAreas: true,
      loadingCalk: false,
      pnum: 1,
      CalkData: [{}],
      scaN_ID: this.props.match.params.id,
      SCAN: '',
      PirSet: [{}],
      AreasData: [],

    };
    //this.SetParams = this.SetParams.bind(this);
    this.GetCalkDataByScanId=this.GetCalkDataByScanId.bind(this)
  }



  componentDidMount() {
    debugger
    this.GetPiroSettings();
    this.GetScanById(this.state.scaN_ID)
    this.GetCalkDataByScanId(this.state.scaN_ID);
    this.GetTemper();


  }

  handleChange(e) {
    debugger
    const { name, value } = e.target;
    this.setState(prevState => ({ ...prevState, [name]: Number(value), pnum: 1 }))
  }


  render() {
    let contents1 = this.state.loading1
      ? <p><em>Loading...</em></p>
      : this.state.image1
    let contents2 = this.state.loading2
      ? <p><em>Loading...</em></p>
      : this.state.image2

    return (
      <div>

        <Jumbotron>
          <h1 className="display-4">Скан №{this.state.scaN_ID}</h1>
          <p className="lead">Просмотр термограмм с определением заготовки, а также с произведенными расчётами</p>
        </Jumbotron>

        <Card>
          <CardBody>
            {/* <h1 id="tabelLabel" >Скан №{this.state.scaN_ID}</h1> */}
            <h5 id="tabelLabel" >Пирометр №{this.state.SCAN.piR_NUMBER}</h5>
            <h5 id="tabelLabel" >Номер заготовки: {this.state.SCAN.maT_ID}</h5>
            <h5 id="tabelLabel" >Номер прохода: {this.state.SCAN.pasS_NO}</h5>
            <h5 id="tabelLabel" >Исходный файл: {this.state.SCAN.csV_FILE_NAME}</h5>
            <h5 id="tabelLabel" >Начало сканирования: {this.state.SCAN.dT_CSV}</h5>
            <h5 id="tabelLabel" >Направление сканирования: {this.state.SCAN.forwarD_DIRECTION ? "ВПЕРЁД" : "НАЗАД"}</h5>
            <div class="container testimonial-group" >
              <div class="row">
                {contents1}
              </div>
            </div>
            <p></p>
            <h5 id="tabelLabel" >Определение прямоугольника заготовки:</h5>
            <div class="container testimonial-group" >
              <div class="row">
                {contents2}
              </div>
            </div>
          </CardBody>

        </Card>

        {
          this.state.CalkData.map((el, index) => {
            let contents = !this.state['loading' + el.id]
              ? <p><em>Loading...</em></p>
              : this.state['image' + el.id]
            debugger
            let areas = this.state.AreasData && this.state.AreasData.filter(ar=>ar.calkdatA_ID === el.id);  
            //areas.sort((a, b) => a.row_Num.localeCompare(b.row_Num) || b.col_Num - a.col_Num);
            areas.sort(function (a, b) {
              return a.roW_NUM - b.roW_NUM || a.coL_NUM - b.coL_NUM;
          });
            return (
              <div>
                <Card>
                  <CardBody>

                    <Row>
                      <Col>
                        <h2 id="tabelLabel"{...index}>Расчётные данные</h2>
                        <h5 id="tabelLabel"{...index}>id: {el.id}, время: {el.calk_dt}</h5>
                        <h5 id="tabelLabel"{...index}>Габариты: {el.rectdata && el.rectdata.ingoT_HEIGHT} x {el.rectdata && el.rectdata.ingoT_WIDTH} мм</h5>
                        <h5 id="tabelLabel"{...index} >Ячейки: {el.cellsdata && el.cellsdata.coL_QUANT} x {el.cellsdata && el.cellsdata.roW_QUANT} шт</h5>

                      </Col>
                      <Col>
                        <h2 id="tabelLabel"{...index} >Применённые настройки: </h2>
                        <h5 id="tabelLabel"{...index} >размер ячейки ({el.setttings && el.setttings.cellsettings.celL_HEIGHT} x {el.setttings && el.setttings.cellsettings.celL_WIDTH}) мм</h5>
                        <h5 id="tabelLabel"{...index}> обрезка: слева: {el.setttings && el.setttings.cutsettings.cuT_LEFT} мм,  справа: {el.setttings && el.setttings.cutsettings.cuT_RIGHT} мм, сверху: {el.setttings && el.setttings.cutsettings.cuT_TOP} мм, снизу: {el.setttings && el.setttings.cutsettings.cuT_BOTTOM} мм</h5>
                        <h5 id="tabelLabel"{...index}>Разбиение на области: ({el.setttings && el.setttings.areasettings.ariA_COL_QUANT} x {el.setttings && el.setttings.areasettings.ariA_ROW_QUANT}) шт</h5>
                      </Col>
                    </Row>
                    <div class="container testimonial-group" >
                      <div class="row">
                        {contents}
                      </div>
                    </div>
                    <p>
                    <Table size="sm" bordered>
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Координаты (лев. верх. угла) области</th>
                          <th>Размер области, шт х шт</th>
                          <th>Тмин</th>
                          <th>Tсред</th>
                          <th>Тмакс</th>
                          <th>Тип</th>

                                                  </tr>
                      </thead>
                      <tbody>
                     {/* {this.state.AreasData  && JSON.stringify(areas)}  */}
                     {this.state.AreasData  && areas.map
                      (function (ar, ind) {
                        return (
                          <tr key={ind}>
                            <td>{ar.roW_NUM}.{ar.coL_NUM}</td>
                            <td>X={ar.celL_UP_LEFT_X}, Y={ar.celL_UP_LEFT_Y}</td>
                            <td>{ar.areA_WIDTH}x{ar.areA_HEIGHT}</td>
                            <td>{ar.mint.toFixed(1)}</td>
                            <td>{ar.avgt.toFixed(1)}</td>
                            <td>{ar.maxt.toFixed(1)}</td>
                            {ar.typE_AREA === 0 ? <td>область</td> : ''}
                            {ar.typE_AREA === 1 ? <td>обрез. область</td> : ''}
                            {ar.typE_AREA === 2 ? <td>опис. прямо-ник</td> : ''}
                            {ar.typE_AREA === 3 ? <td>впис. прямо-ник</td> : ''}
                          </tr>
                        )
                      })}
                    
                     </tbody>
                     </Table>
                    </p>
                  </CardBody>
                </Card>
              </div>
            )
          })
        }
        <Button color="primary" onClick={() => this.updateCalkulation(this.state.scaN_ID)}>Рассчитать с актуальными настройками пирометра</Button>
        {this.state.loadingCalk ? <Spinner></Spinner> : ''}
      </div>
    );
  }

  async GetTemper() {
    const token = await authService.getAccessToken();
    await fetch('Scans/GetScanPict/' + this.state.scaN_ID, {
      headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
    }).then((res) => {
      res.blob()
        .then((img) => {
          console.log(img)
          this.setState(prevState => ({ ...prevState, image1: <img src={window.webkitURL.createObjectURL(img)}  height="500" alt="1" />, loading1: false }));

        })
    })
  }


  async GetScanById(scaN_ID) {
    const token = await authService.getAccessToken();
    await fetch('Scans/' + scaN_ID, {
      headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
    }).then((res) => {
      res.json()
        .then((data) => {
          console.log(data)
          this.setState(prevState => ({ ...prevState, SCAN: data }))
        })

    })
  }


  async GetCalkDataByScanId(scaN_ID) {
    const token = await authService.getAccessToken();
    await fetch('CalkData/GetByScan/' + scaN_ID, {
      headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
    }).then((res) => {
      res.clone().json()
        .then((data) => {
          console.log("CalkData/GetByScan/", data)
          this.setState(prevState => ({ ...prevState, CalkData: data }))

          this.GetTemperBox(data[0].id);
          //this.GetAreas(data[0].id);

          data.forEach(element => {
            this.GetTemperPixelate(element.id)
            this.GetAreas(element.id)
          });

        })
      return res.json()
    })

  }


  async GetPiroSettings() {
    const token = await authService.getAccessToken();
    await fetch('/ActSettings', {
      headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
    }).then((res) => {
      res.json()
        .then((data) => {
          console.log(data)
          this.setState(prevState => ({ ...prevState, PirSet: data }))
        })

    })
  }



  async GetTemperBox(calk_data_id) {
    const token = await authService.getAccessToken();
    await fetch('CalkData/GetRectPict/' + calk_data_id, {
      headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
    }).then((res) => {
      res.blob()
        .then((img) => {
          console.log(img)
          this.setState(prevState => ({ ...prevState, image2: <img src={window.webkitURL.createObjectURL(img)} height="500" alt="2" />, loading2: false }));

        })
    })
  }

  async GetAreas(calk_data_id) {
    const token = await authService.getAccessToken();
    await fetch('/AreasData/GetByCalk/' + calk_data_id, {
      headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
    }).then((res) => {
      res.json()
        .then((data) => {
          console.log("AreasData", data)
  //        this.setState(prevState => ({ ...prevState, AreasData: data }))

          this.setState(prevState => ({
            AreasData: [...prevState.AreasData, ...data]
          }))

        })
    })
  }

  async GetTemperPixelate(calk_data_id) {
    const token = await authService.getAccessToken();
    await fetch('CalkData/GetCellsPict/' + calk_data_id, {
      headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
    }).then((res) => {
      res.blob()
        .then((img) => {
          console.log("image" + calk_data_id, img)
          //return img;
          this.setState(prevState => ({ ...prevState, ["image" + calk_data_id]: <img src={window.webkitURL.createObjectURL(img)}  height="250" alt={calk_data_id} />, ["loading" + calk_data_id]: true }));
        })
    })
  }



  async updateCalkulation(scaN_ID) {
    this.setState(prevState => ({ ...prevState, loadingCalk: true }))
    const token = await authService.getAccessToken();
    await fetch('TFields/UpdCalk/' + scaN_ID,
      {
        headers: !token ? {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        } : {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`,
          // "Content-Type": "application/x-www-form-urlencoded"
        },
        mode: 'cors',
        cache: 'default',
        method: "POST",
        //body: JSON.stringify({
        //  "scaN_ID": scaN_ID,
        //})//"pnum=1&x_count=20&y_count=10&x_perc=5&y_perc=5"
      })
      .then((res) => {
        if (res.status === 200) toast.success('Успешно записалось')
        else toast.error('Не удалось записать')
        this.setState(prevState => ({ ...prevState, loadingCalk: false }))
        window.location.reload();
        // res.json()
        //   .then((re) => {
        //     console.log("re", re)
        //     window.location.reload();
        //   })
      })
  }


}
