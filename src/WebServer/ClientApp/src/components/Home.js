import React, { Component } from 'react';

export class Home extends Component {
  static displayName = Home.name;

  render () {
    return (
      <div>
        <h1>ПО ИНТЕГРАЦИИ</h1>
        <p>Добро пожаловать на веб интерфейс программы интерации ПО Lanscan WCA и АСУ ССМ</p>
      </div>
    );
  }
}
