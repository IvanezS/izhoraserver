import React, { Component } from 'react';
import authService from './api-authorization/AuthorizeService'
import {
  Table, Button, Input, InputGroup, InputGroupAddon, Container, Card, CardHeader, Row, Col, Jumbotron
} from 'reactstrap';
// Importing toastify module
import { toast } from 'react-toastify';

// Import toastify css file
import 'react-toastify/dist/ReactToastify.css';

// toast-configuration method, 
// it is compulsory method.
toast.configure()


export class SettingsPir extends Component {

  constructor(props) {
    super(props);
    this.state = {

      PirSet: [{}],
      Sett: [{}],
      AreaSett: [{}],
      AreaSettCom: [{}],
      CellSett: [{}],
      CutSett: [{}],
      x_count: 5,
      y_count: 3,
      x_cell: 50,
      y_cell: 50,
      cuT_LEFT: 100,
      cuT_TOP: 100,
      cuT_RIGHT: 100,
      cuT_BOTTOM: 100,
      AreaSettId: 0,
      CellSettId: 0,
      CutSettId: 0,
      SettId0: 0,
      SettId1: 0,
      SettId2: 0,
      SettId3: 0,


    };
    this.redir = this.redir.bind(this);
    this.GetPiroSettings = this.GetPiroSettings.bind(this);
    this.GetSettings = this.GetSettings.bind(this);
    this.GetAreasSettings = this.GetAreasSettings.bind(this);
    this.GetCellSettings = this.GetCellSettings.bind(this);
    this.GetCutSettings = this.GetCutSettings.bind(this);

  }



  componentDidMount() {

    this.GetPiroSettings();
    this.GetSettings();
    this.GetAreasSettings();
    this.GetCellSettings();
    this.GetCutSettings();
  }

  handleChange(e) {
    debugger
    const { name, value } = e.target;
    this.setState(prevState => ({ ...prevState, [name]: Number(value) }))
  }

  redir(id) {
    this.props.history.push("/TFields/" + id)
  }


  render() {

    var that = this



    return (
      <div>

        <Jumbotron>
          <h1 className="display-4">Настройки</h1>
          <p className="lead">Настройки для обработки термограмм, которые привязываются к каждому из пирометров</p>
        </Jumbotron>

        <Row>

          <Col>
            <Card>
              <CardHeader tag="h5" style={{ backgroundColor: '#ff0000' }}>СБОРКА НАСТРОЕК ДЛЯ ПИРОМЕТРА №1</CardHeader>
              <Container>
                <p></p>
                <InputGroup>
                  <Input type="select" name="SettId0" id="SettId0" value={this.state.SettId0} onChange={this.handleChange.bind(this)}>
                    <option value="0">Выберите настройку для пирометра</option>
                    {this.state.Sett && this.state.Sett.map(function (el, index) {
                      return (
                        <option key={index} value={el.id}>
                          Области: ({el.areasettings && el.areasettings.ariA_COL_QUANT}х{el.areasettings && el.areasettings.ariA_ROW_QUANT}) шт, Ячейка: ({el.cellsettings && el.cellsettings.celL_WIDTH}х{el.cellsettings && el.cellsettings.celL_HEIGHT}) мм, Обрезка: ({el.cutsettings && el.cutsettings.cuT_LEFT}, {el.cutsettings && el.cutsettings.cuT_RIGHT}, {el.cutsettings && el.cutsettings.cuT_TOP}, {el.cutsettings && el.cutsettings.cuT_BOTTOM}) мм
                        </option>
                      )
                    })
                    }
                  </Input>

                  <InputGroupAddon addonType="append">
                    <Button color="secondary" onClick={() => this.PutActSettings(1, this.state.SettId0)}>Изменить</Button>
                  </InputGroupAddon>
                </InputGroup>
                <p></p>
              </Container>
            </Card>
          </Col>

          <Col>
            <Card>
              <CardHeader tag="h5" style={{ backgroundColor: '#ff0000' }}>СБОРКА НАСТРОЕК ДЛЯ ПИРОМЕТРА №2</CardHeader>
              <Container>
                <p></p>
                <InputGroup>
                  <Input type="select" name="SettId1" id="SettId1" value={this.state.SettId1} onChange={this.handleChange.bind(this)}>
                    <option value="0">Выберите настройку для пирометра</option>
                    {that.state.Sett && that.state.Sett.map(function (el, index) {
                      return (
                        <option key={index} value={el.id}>
                          Области: ({el.areasettings && el.areasettings.ariA_COL_QUANT}х{el.areasettings && el.areasettings.ariA_ROW_QUANT}) шт, Ячейка: ({el.cellsettings && el.cellsettings.celL_WIDTH}х{el.cellsettings && el.cellsettings.celL_HEIGHT}) мм, Обрезка: ({el.cutsettings && el.cutsettings.cuT_LEFT}, {el.cutsettings && el.cutsettings.cuT_RIGHT}, {el.cutsettings && el.cutsettings.cuT_TOP}, {el.cutsettings && el.cutsettings.cuT_BOTTOM}) мм
                        </option>
                      )
                    })
                    }
                  </Input>

                  <InputGroupAddon addonType="append">
                    <Button color="secondary" onClick={() => this.PutActSettings(2, this.state.SettId1)}>Изменить</Button>
                  </InputGroupAddon>
                </InputGroup>
                <p></p>
              </Container>
            </Card>
          </Col>

        </Row>


        <p></p>

        <Row>

          <Col>
            <Card>
              <CardHeader tag="h5" style={{ backgroundColor: '#ff0000' }}>СБОРКА НАСТРОЕК ДЛЯ ПИРОМЕТРА №3</CardHeader>
              <Container>
                <p></p>
                <InputGroup>
                  <Input type="select" name="SettId2" id="SettId2" value={this.state.SettId2} onChange={this.handleChange.bind(this)}>
                    <option value="0">Выберите настройку для пирометра</option>
                    {this.state.Sett && this.state.Sett.map(function (el, index) {
                      return (
                        <option key={index} value={el.id}>
                          Области: ({el.areasettings && el.areasettings.ariA_COL_QUANT}х{el.areasettings && el.areasettings.ariA_ROW_QUANT}) шт, Ячейка: ({el.cellsettings && el.cellsettings.celL_WIDTH}х{el.cellsettings && el.cellsettings.celL_HEIGHT}) мм, Обрезка: ({el.cutsettings && el.cutsettings.cuT_LEFT}, {el.cutsettings && el.cutsettings.cuT_RIGHT}, {el.cutsettings && el.cutsettings.cuT_TOP}, {el.cutsettings && el.cutsettings.cuT_BOTTOM}) мм
                        </option>
                      )
                    })
                    }
                  </Input>

                  <InputGroupAddon addonType="append">
                    <Button color="secondary" onClick={() => this.PutActSettings(3, this.state.SettId2)}>Изменить</Button>
                  </InputGroupAddon>
                </InputGroup>
                <p></p>
              </Container>
            </Card>
          </Col>

          <Col>
            <Card>
              <CardHeader tag="h5" style={{ backgroundColor: '#ff0000' }}>СБОРКА НАСТРОЕК ДЛЯ ПИРОМЕТРА №4</CardHeader>
              <Container>
                <p></p>
                <InputGroup>
                  <Input type="select" name="SettId3" id="SettId3" value={this.state.SettId3} onChange={this.handleChange.bind(this)}>
                    <option value="0">Выберите настройку для пирометра</option>
                    {that.state.Sett && that.state.Sett.map(function (el, index) {
                      return (
                        <option key={index} value={el.id}>
                          Области: ({el.areasettings && el.areasettings.ariA_COL_QUANT}х{el.areasettings && el.areasettings.ariA_ROW_QUANT}) шт, Ячейка: ({el.cellsettings && el.cellsettings.celL_WIDTH}х{el.cellsettings && el.cellsettings.celL_HEIGHT}) мм, Обрезка: ({el.cutsettings && el.cutsettings.cuT_LEFT}, {el.cutsettings && el.cutsettings.cuT_RIGHT}, {el.cutsettings && el.cutsettings.cuT_TOP}, {el.cutsettings && el.cutsettings.cuT_BOTTOM}) мм
                        </option>
                      )
                    })
                    }
                  </Input>

                  <InputGroupAddon addonType="append">
                    <Button color="secondary" onClick={() => this.PutActSettings(4, this.state.SettId3)}>Изменить</Button>
                  </InputGroupAddon>
                </InputGroup>
                <p></p>
              </Container>
            </Card>
          </Col>

        </Row>

        <p></p>


        <Card>
          <CardHeader tag="h5" style={{ backgroundColor: '#ede29a' }}>СБОРКИ НАСТРОЕК</CardHeader>
          <Container>
            <p></p>
            <InputGroup>
              <Input type="select" name="AreaSettId" id="AreaSettId" value={this.state.AreaSettId} onChange={this.handleChange.bind(this)}>
                <option value="0">Выберите разбиение на области</option>
                {this.state.AreaSett && this.state.AreaSett.map(function (ol, index) {
                  return (
                    <option key={index} value={ol.id}>
                      {ol.id}: ({ol.ariA_COL_QUANT}x{ol.ariA_ROW_QUANT}) шт
                    </option>
                  )
                })
                }
              </Input>
              <Input type="select" name="CellSettId" id="CellSettId" value={this.state.CellSettId} onChange={this.handleChange.bind(this)}>
                <option value="0">Выберите размер ячейки</option>
                {this.state.CellSett && this.state.CellSett.map(function (ol, index) {
                  return (
                    <option key={index} value={ol.id}>
                      {ol.id}: ({ol.celL_WIDTH}x{ol.celL_HEIGHT}) мм
                    </option>
                  )
                })
                }
              </Input>
              <Input type="select" name="CutSettId" id="CutSettId" value={this.state.CutSettId} onChange={this.handleChange.bind(this)}>
                <option value="0">Выберите обрезку прямоугольника</option>
                {this.state.CutSett && this.state.CutSett.map(function (ol, index) {
                  return (
                    <option key={index} value={ol.id}>
                      {ol.id}: ({ol.cuT_LEFT}, {ol.cuT_RIGHT}, {ol.cuT_TOP}, {ol.cuT_BOTTOM}) мм
                    </option>
                  )
                })
                }
              </Input>
              <InputGroupAddon addonType="append">
                <Button color="secondary" onClick={this.PostSettings.bind(this)}>Добавить в список</Button>
              </InputGroupAddon>
            </InputGroup>


            <p></p>
            <h5>Список сборок настроек</h5>
            <Table size="sm" bordered>
              <thead>
                <tr>

                  <th>id</th>
                  <th>Разбиение на области</th>
                  <th>Размеры ячейки </th>
                  <th>Обрезка прямугольника</th>
                </tr>
              </thead>
              <tbody>
                {that.state.Sett && that.state.Sett
                  // .filter(data => {
                  //   if (search === null) {
                  //     return data
                  //   } else {
                  //     if (data.itemName.toLowerCase().includes(search.searchtext.toLowerCase())) return data
                  //   }
                  // })
                  .map(function (el, index) {
                    return (
                      <tr key={index}>
                        <td>{el.id}</td>
                        <td> ({el.areasettings && el.areasettings.ariA_COL_QUANT}х{el.areasettings && el.areasettings.ariA_ROW_QUANT}) шт</td>
                        <td> ({el.cellsettings && el.cellsettings.celL_WIDTH}х{el.cellsettings && el.cellsettings.celL_HEIGHT}) мм</td>
                        <td> ({el.cutsettings && el.cutsettings.cuT_LEFT}, {el.cutsettings && el.cutsettings.cuT_RIGHT}, {el.cutsettings && el.cutsettings.cuT_TOP}, {el.cutsettings && el.cutsettings.cuT_BOTTOM}) мм</td>
                      </tr>
                    )
                  })}
              </tbody>
            </Table>
          </Container>
        </Card>
        <p></p>
        <Card>
          <CardHeader tag="h5" style={{ backgroundColor: '#9acbed' }}>РАЗБИЕНИЕ НА ОБЛАСТИ</CardHeader>
          <Container>
            <p></p>
            <InputGroup>
              <Input type="number" min={1} max={10} name="x_count" id="x_count" value={this.state.x_count} onChange={this.handleChange.bind(this)} title="Кол-во разбиений по ширине" placeholder="Кол-во разбиений X" />
              <Input type="number" min={1} max={10} name="y_count" id="y_count" value={this.state.y_count} onChange={this.handleChange.bind(this)} title="Кол-во разбиений по высоте" placeholder="Кол-во разбиений Y" />
              <InputGroupAddon addonType="append">
                <Button color="secondary" onClick={this.PostAreaSettings.bind(this)}>Добавить в список</Button>
              </InputGroupAddon>
            </InputGroup>
            <p></p>
            <h5>Список разбиений на области</h5>
            <Table size="sm" bordered>
              <thead>
                <tr>

                  <th>id</th>
                  <th>Кол-во столбцов, шт</th>
                  <th>Кол-во строк, шт</th>
                </tr>
              </thead>
              <tbody>

                {that.state.AreaSett && that.state.AreaSett
                  // .filter(data => {
                  //   if (search === null) {
                  //     return data
                  //   } else {
                  //     if (data.itemName.toLowerCase().includes(search.searchtext.toLowerCase())) return data
                  //   }
                  // })
                  .map(function (el, index) {
                    return (
                      <tr key={index}>

                        <td>{el.id}</td>
                        <td>{el.ariA_COL_QUANT}</td>
                        <td>{el.ariA_ROW_QUANT}</td>
                      </tr>
                    )
                  })}
              </tbody>
            </Table>
          </Container>
        </Card>
        <p></p>
        <Card>
          <CardHeader tag="h5" style={{ backgroundColor: '#a2ed9a' }}>РАЗМЕРЫ ЯЧЕЙКИ</CardHeader>
          <Container>
            <p></p>

            <InputGroup>
              <Input type="number" min={10} max={300} name="y_cell" id="y_cell" value={this.state.y_cell} onChange={this.handleChange.bind(this)} title="Ширина ячейки, мм" placeholder="Ширина ячейки, мм" />
              <Input type="number" min={10} max={300} name="x_cell" id="x_cell" value={this.state.x_cell} onChange={this.handleChange.bind(this)} title="Высота ячейки, мм" placeholder="Высота ячейки, мм" />
              <InputGroupAddon addonType="append">
                <Button color="secondary" onClick={this.PostCellSettings.bind(this)}>Добавить в список</Button>
              </InputGroupAddon>
            </InputGroup>
            <p></p>
            <h5>Список размеров ячейки</h5>

            <Table size="sm" bordered>
              <thead>
                <tr>

                  <th>id</th>
                  <th>Ширина, мм</th>
                  <th>Высота, мм</th>
                </tr>
              </thead>
              <tbody>

                {that.state.CellSett && that.state.CellSett
                  // .filter(data => {
                  //   if (search === null) {
                  //     return data
                  //   } else {
                  //     if (data.itemName.toLowerCase().includes(search.searchtext.toLowerCase())) return data
                  //   }
                  // })
                  .map(function (el, index) {
                    return (
                      <tr key={index}>

                        <td>{el.id}</td>
                        <td>{el.celL_WIDTH}</td>
                        <td>{el.celL_HEIGHT}</td>
                      </tr>
                    )
                  })}
              </tbody>
            </Table>

          </Container>
        </Card>
        <p></p>
        <Card>
          <CardHeader tag="h5" style={{ backgroundColor: '#e99aed' }} >ОБРЕЗКА ПРЯМОУГОЛЬНИКА</CardHeader>
          <Container>
            <p></p>

            <InputGroup>
              <Input type="number" min={0} max={1000} name="cuT_LEFT" id="cuT_LEFT" value={this.state.cuT_LEFT} onChange={this.handleChange.bind(this)} title="Обрезка слева, мм" placeholder="Обрезка слева, мм" />
              <Input type="number" min={0} max={1000} name="cuT_RIGHT" id="cuT_RIGHT" value={this.state.cuT_RIGHT} onChange={this.handleChange.bind(this)} title="Обрезка справа, мм" placeholder="Обрезка справа, мм" />
              <Input type="number" min={0} max={1000} name="cuT_TOP" id="cuT_TOP" value={this.state.cuT_TOP} onChange={this.handleChange.bind(this)} title="Обрезка сверху, мм" placeholder="Обрезка сверху, мм" />
              <Input type="number" min={0} max={1000} name="cuT_BOTTOM" id="cuT_BOTTOM" value={this.state.cuT_BOTTOM} onChange={this.handleChange.bind(this)} title="Обрезка снизу, мм" placeholder="Обрезка снизу, мм" />

              <InputGroupAddon addonType="append">
                <Button color="secondary" onClick={this.PostCutSettings.bind(this)}>Добавить в список</Button>
              </InputGroupAddon>
            </InputGroup>
            <p></p>
            <h5>Список обрезок прямоугольника заготовки</h5>
            <Table size="sm" bordered>
              <thead>
                <tr>

                  <th>id</th>
                  <th>Слева, мм</th>
                  <th>Справа, мм</th>
                  <th>Сверху, мм</th>
                  <th>Снизу, мм</th>
                </tr>
              </thead>
              <tbody>

                {that.state.CutSett && that.state.CutSett
                  // .filter(data => {
                  //   if (search === null) {
                  //     return data
                  //   } else {
                  //     if (data.itemName.toLowerCase().includes(search.searchtext.toLowerCase())) return data
                  //   }
                  // })
                  .map(function (el, index) {
                    return (
                      <tr key={index}>

                        <td>{el.id}</td>
                        <td>{el.cuT_LEFT}</td>
                        <td>{el.cuT_RIGHT}</td>
                        <td>{el.cuT_TOP}</td>
                        <td>{el.cuT_BOTTOM}</td>
                      </tr>
                    )
                  })}
              </tbody>
            </Table>

          </Container>
          <p></p>
        </Card>
        <p></p>
      </div >

    );
  }



  async GetPiroSettings() {
    const token = await authService.getAccessToken();
    await fetch('/ActSettings', {
      headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
    }).then((res) => {
      res.json()
        .then((data) => {

          console.log(data)
          this.setState(prevState => ({ ...prevState, PirSet: data }))
          data.forEach(element => {
            if (element.piR_NUMBER === 1) this.setState(prevState => ({ ...prevState, SettId0: element.settingS_ID }))
            if (element.piR_NUMBER === 2) this.setState(prevState => ({ ...prevState, SettId1: element.settingS_ID }))
            if (element.piR_NUMBER === 3) this.setState(prevState => ({ ...prevState, SettId2: element.settingS_ID }))
            if (element.piR_NUMBER === 4) this.setState(prevState => ({ ...prevState, SettId3: element.settingS_ID }))

          });

        })

    })
  }


  async GetSettings() {
    const token = await authService.getAccessToken();
    await fetch('/Settings', {
      headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
    }).then((res) => {
      res.json()
        .then((data) => {
          console.log(data)
          this.setState(prevState => ({ ...prevState, Sett: data }))
        })

    })
  }



  async GetAreasSettings() {
    const token = await authService.getAccessToken();
    await fetch('/AreasSettings', {
      headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
    }).then((res) => {
      res.json()
        .then((data) => {
          console.log(data)
          this.setState(prevState => ({ ...prevState, AreaSett: data }))
        })

    })
  }

  async GetCellSettings() {
    const token = await authService.getAccessToken();
    await fetch('/CellsSettings', {
      headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
    }).then((res) => {
      res.json()
        .then((data) => {
          console.log(data)
          this.setState(prevState => ({ ...prevState, CellSett: data }))
        })

    })
  }

  async GetCutSettings() {
    const token = await authService.getAccessToken();
    await fetch('/CutSettings', {
      headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
    }).then((res) => {
      res.json()
        .then((data) => {
          console.log(data)
          this.setState(prevState => ({ ...prevState, CutSett: data }))
        })

    })
  }


  async PostAreaSettings() {
    debugger
    const token = await authService.getAccessToken();
    await fetch('/AreasSettings',
      {
        headers: !token ? {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        } : {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`,
        },
        mode: 'cors',
        cache: 'default',
        method: "POST",
        body: JSON.stringify({
          "id": 0,
          "ariA_ROW_QUANT": this.state.y_count,
          "ariA_COL_QUANT": this.state.x_count
        })
      })
      .then((res) => {
        if (res.status === 201) toast.success('Успешно записалось')
        if (res.status === 400) toast.error('Введены некорректные данные')
        if (res.status !== 400 && res.status !== 201) toast.error('Не удалось записать')
        res.json()
          .then((re) => {
            console.log("re", re)
            this.GetSettings()
            this.GetAreasSettings()
          })
      })
  }


  async PostCellSettings() {
    debugger
    const token = await authService.getAccessToken();
    await fetch('/CellsSettings',
      {
        headers: !token ? {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        } : {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`,
        },
        mode: 'cors',
        cache: 'default',
        method: "POST",
        body: JSON.stringify({
          "id": 0,
          "celL_HEIGHT": this.state.x_cell,
          "celL_WIDTH": this.state.y_cell,

        })
      })
      .then((res) => {
        if (res.status === 201) toast.success('Успешно записалось')
        if (res.status === 400) toast.error('Введены некорректные данные')
        if (res.status !== 400 && res.status !== 201) toast.error('Не удалось записать')
        res.json()
          .then((re) => {
            console.log("re", re)
            this.GetSettings()
            this.GetCellSettings()
          })
      })
  }

  async PostCutSettings() {
    debugger
    const token = await authService.getAccessToken();
    await fetch('/CutSettings',
      {
        headers: !token ? {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        } : {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`,
        },
        mode: 'cors',
        cache: 'default',
        method: "POST",
        body: JSON.stringify({
          "id": 0,
          "cuT_LEFT": this.state.cuT_LEFT,
          "cuT_RIGHT": this.state.cuT_RIGHT,
          "cuT_TOP": this.state.cuT_TOP,
          "cuT_BOTTOM": this.state.cuT_BOTTOM
        })
      })
      .then((res) => {
        if (res.status === 201) toast.success('Успешно записалось')
        if (res.status === 400) toast.error('Введены некорректные данные')
        if (res.status !== 400 && res.status !== 201) toast.error('Не удалось записать')
        res.json()
          .then((re) => {
            console.log("re", re)
            this.GetSettings()
            this.GetCutSettings()
          })
      })
  }

  async PostSettings() {
    debugger
    const token = await authService.getAccessToken();
    await fetch('/Settings',
      {
        headers: !token ? {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        } : {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`,
        },
        mode: 'cors',
        cache: 'default',
        method: "POST",
        body: JSON.stringify({
          "id": 0,
          "cell_settings_id": this.state.CellSettId,
          "cut_settings_id": this.state.CutSettId,
          "areas_settings_id": this.state.AreaSettId
        })
      })
      .then((res) => {
        if (res.status === 201) toast.success('Успешно записалось')
        if (res.status === 400) toast.error('Введены некорректные данные')
        if (res.status !== 400 && res.status !== 201) toast.error('Не удалось записать')
        res.json()
          .then((re) => {
            console.log("re", re)
            this.GetSettings()
          })
      })
  }

  async PutActSettings(pirNum, index) {
    debugger
    var PirSetId = 0;
    this.state.PirSet.forEach(element => {
      if (element.piR_NUMBER === pirNum) PirSetId = element.id;
    }); 

    const token = await authService.getAccessToken();
    await fetch('/ActSettings/' + PirSetId,
      {
        headers: !token ? {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        } : {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`,
        },
        mode: 'cors',
        cache: 'default',
        method: "PUT",
        body: JSON.stringify({
          "id": PirSetId,
          "piR_NUMBER": pirNum,
          "settingS_ID": Number(index)
        })
      })
      .then((res) => {
        if (res.status === 200) toast.success('Успешно записалось')
        else toast.error('Не удалось записать')
        res.json()
          .then((re) => {
            console.log("re", re)
            this.GetPiroSettings()
          })
      })
  }




}
