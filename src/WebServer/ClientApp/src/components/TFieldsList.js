import React, { Component } from 'react';
import authService from './api-authorization/AuthorizeService'
import {
  Table, Button, Jumbotron, Input, InputGroup, InputGroupText, Card, CardHeader, Spinner
} from 'reactstrap';
import CardBody from 'reactstrap/lib/CardBody';
import $ from 'jquery';


export class TFieldsList extends Component {

  constructor(props) {
    super(props);
    this.state = {

      SCANS: [{}],
      maT_ID: 0,
      PIR_Numb: 1,
      mat_id_option: false,
      pir_num_option: false,
      loading: false

    };
    this.redir = this.redir.bind(this);

  }



  componentDidMount() {

    this.GetScans();
  }

  redir(id) {
    this.props.history.push("/TFields/" + id)
  }

  handleChange(e) {
    debugger
    const { name, value } = e.target;
    this.setState(prevState => ({ ...prevState, [name]: Number(value) }))
  }

  handleCheckbox(e) {
    debugger
    const { name, value } = e.target;
    if (value === "false") {
    this.setState(prevState => ({ ...prevState, [name]: true }))
  }else{
    this.setState(prevState => ({ ...prevState, [name]: false }))
  }
}
  render() {

    var that = this
    return (
      <div>

        <Jumbotron>
          <h1 className="display-4">Список сканов</h1>
          <p className="lead">Таблица произведенных сканирований с рассчётами</p>
        </Jumbotron>

        <Card>
          <CardHeader>
            Фильтр по mat_id и/или по номеру пирометра
            {this.state.loading ? <Spinner></Spinner> : ''}
          </CardHeader>

          <CardBody>

          <InputGroup>
            <InputGroupText>
              <Input
                addon
                aria-label="Checkbox for following text input"
                type="checkbox"
                name="mat_id_option" id="mat_id_option" value={this.state.mat_id_option} onChange={this.handleCheckbox.bind(this)}
              />
            </InputGroupText>
            <Input type="number" min={0} name="maT_ID" id="maT_ID" value={this.state.maT_ID} onChange={this.handleChange.bind(this)} title="Номер заготовки (mat_id)" placeholder="Номер заготовки (mat_id)" />
          </InputGroup>
          <br />
          <InputGroup>
            <InputGroupText>
              <Input
                addon
                aria-label="Checkbox for following text input"
                type="checkbox"
                name="pir_num_option" id="pir_num_option" value={this.state.pir_num_option} onChange={this.handleCheckbox.bind(this)} title="Номер пирометра" placeholder="Номер пирометра"
              />
            </InputGroupText>
            <Input type="number" min={1} max={4} name="PIR_Numb" id="PIR_Numb" value={this.state.PIR_Numb} onChange={this.handleChange.bind(this)} title="Номер пирометра" placeholder="Номер пирометра" />
          </InputGroup>
          <br />

          <Button color="secondary" onClick={this.GetScansFiltered.bind(this)}>Применить</Button>
          </CardBody>
        </Card>
        <Table size="sm" bordered>
          <thead>
            <tr>
              <th>id</th>
              <th>№ пирометра</th>
              <th>№ заготовки</th>
              <th>№ прохода</th>
              <th>Направление</th>
              <th>Дата</th>
              <th>Дата CSV</th>
              <th>Действия</th>
            </tr>
          </thead>
          <tbody>
            {this.state.SCANS
              // .filter(data => {
              //   if (search === null) {
              //     return data
              //   } else {
              //     if (data.itemName.toLowerCase().includes(search.searchtext.toLowerCase())) return data
              //   } 
              // })
              .map(function (el, index) {
                return (
                  <tr key={index}>
                    <td>{el.id}</td>
                    <td>{el.piR_NUMBER}</td>
                    <td>{el.maT_ID}</td>

                    <td>{el.pasS_NO}</td>
                    <td>{el.forwarD_DIRECTION ? "вперед" : "назад"}</td>
                    <td>{new Date(el.dT_FIX).toLocaleString()}</td>
                    <td>{new Date(el.dT_CSV).toLocaleString()}</td>
                    <td>
                      <Button color="link" onClick={() => that.redir(el.id)} >перейти</Button>
                    </td>
                  </tr>
                )
              })}

          </tbody>
        </Table>
      </div>


    );
  }

  async GetScans() {
    const token = await authService.getAccessToken();
    await fetch('/Scans', {
      headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
    }).then((res) => {
      res.json()
        .then((data) => {
          console.log(data)
          this.setState(prevState => ({ ...prevState, SCANS: data }))
        })

    })
  }

  async GetScansFiltered() {
    const token = await authService.getAccessToken();
    this.setState(prevState => ({ ...prevState, loading: true }))
    await fetch('Scans/GetFiltered?' + $.param({
      mat_id_option: this.state.mat_id_option,
      pir_num_option: this.state.pir_num_option,
      mat_id:this.state.maT_ID,
      pir_num: this.state.PIR_Numb}), 
      {headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
    })
  .then((res) => {
      res.json()
        .then((data) => {
          console.log(data)
          this.setState(prevState => ({ ...prevState, loading: false, SCANS: data }))
        })

    })
  }

}
