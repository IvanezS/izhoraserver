using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.SpaServices.ReactDevelopmentServer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Data.Abstractions;
using Data.Data;
using Data.Repositories;
using PLC_S7_Exchange.Core;
using PLC_S7_Exchange.Data;
using PLC_S7_Exchange.Settings;
using WebServer.Data;
using WebServer.Models;
using WebServer.Settings;
using System.Collections.Generic;

namespace WebServer
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        private MainSettings _mainSettings { get; set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.Configure<MainSettings>(Configuration.GetSection("MainSettings"));
            services.Configure<PlcSettings>(Configuration.GetSection("PlcSettings"));
            services.Configure<List<PirometersSettings>>(Configuration.GetSection("PirometersSettings"));

            services.AddScoped<IDbContextInit, DbContextInit>();

            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseOracle(Configuration.GetConnectionString("OracleDBConnection"))
                .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking)
                );

            services.AddDbContext<OracleDBcontext>(options =>
                options.UseOracle(Configuration.GetConnectionString("OracleDBConnection"))
                .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking)
                );

            services.AddScoped(typeof(IRepository<>), typeof(EfRepository<>));
            services.AddScoped<ActSettingsRepository>();
            services.AddScoped<SettingsRepository>();
            services.AddScoped<CalkDataRepository>();
            services.AddScoped<AreasDataRepository>();
            services.AddScoped<ScanRepository>();
            services.AddScoped<Funcs>();
            services.AddScoped<DbContextFactory>();

            services.AddDatabaseDeveloperPageExceptionFilter();

            services.AddDefaultIdentity<ApplicationUser>(options => options.SignIn.RequireConfirmedAccount = true)
                .AddEntityFrameworkStores<ApplicationDbContext>();

            services.AddIdentityServer()
                .AddApiAuthorization<ApplicationUser, ApplicationDbContext>();

            services.AddAuthentication()
                .AddIdentityServerJwt();

            services.AddOpenApiDocument(options =>
            {
                options.Title = "API �� �������������� ����������";
                options.Version = "1.0";
            });

            services.AddSingleton<PiroDataBlock>();
            services.AddSingleton<PlcS7Exchange>();
            services.AddSingleton<WaitCSVHostedService>();
            services.AddHostedService<WaitCSVHostedService>(provider => provider.GetService<WaitCSVHostedService>());
            services.AddSingleton<TimedHostedService>();
            services.AddHostedService<TimedHostedService>(provider => provider.GetService<TimedHostedService>());
            services.AddSingleton<PDIHostedService>();
            services.AddHostedService<PDIHostedService>(provider => provider.GetService<PDIHostedService>());
            services.AddHostedService<OneMinuteHostedService>();
            services.AddHostedService<OneDayHostedService>();

            services.AddControllersWithViews();
            services.AddRazorPages();
            services.Configure<ForwardedHeadersOptions>(options =>
            {
                options.ForwardedHeaders =
                    ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto;
            });
            // In production, the React files will be served from this directory
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/build";
            });

            services.AddCors(options =>
            {
                options.AddDefaultPolicy(
                    builder =>
                    {
                        builder.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin();
                    });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IDbContextInit dbContextInitializer, IOptions<MainSettings> mainsettings)
        {
            _mainSettings = mainsettings.Value;
            app.UseForwardedHeaders();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseMigrationsEndPoint();
                
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
                
            }
            app.UseCors(builder =>
                 builder
                   .AllowAnyHeader()
                   .AllowAnyMethod()
                   .AllowCredentials()
            );
            app.UseOpenApi();
            app.UseSwaggerUi3(x =>
            {
                x.DocExpansion = "list";
            });
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseSpaStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseIdentityServer();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller}/{action=Index}/{id?}");
                endpoints.MapRazorPages();
            });

            app.UseSpa(spa =>
            {
                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment())
                {
                    spa.UseReactDevelopmentServer(npmScript: "start");
                }
            });

            if (_mainSettings.InitDB)
            {
                dbContextInitializer.DbInit();
            }
        }
    }
}
