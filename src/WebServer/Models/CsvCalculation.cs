﻿using CsvHelper;
using Data.Abstractions;
using Data.Models;
using Data.Repositories;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using WebServer.Settings;

namespace WebServer.Models
{
    public class CsvCalculation : IDisposable
    {
        private bool disposedValue;
        private MainSettings _mainSettings { get; }
        private List<PirometersSettings> _pirSettings { get; }
        private readonly ILogger<CsvCalculation> _logger;

        int CSVpirNUM { get; set; }
        int CSVmatID { get; set; }
        int CSVpassID { get; set; }
        bool Dire { get; set; }
        DateTime _DT_CSV { get; set; }

        float T_min { get; set; }
        float T_max { get; set; } 
        float G_INGOT_HEIGHT { get; set; } 
        float G_INGOT_WIDTH { get; set; } 

        float[,] save_ma { get; set; }

        int MaxDistanceCSV { get; set; } 

        public IServiceProvider _scopeFactory;
        //int[,] Pixarr { get; set; }
        Rectangle InscribedRect { get; set; }
        Rectangle CircumscribedRect { get; set; }
        public int SCAN_ID { get; }

        private readonly ActSettingsRepository _ActSet;
        private readonly ScanRepository _SCAN;
        private readonly CalkDataRepository _CalkData;
        private readonly IRepository<AreasData> _AreasData;
        private readonly IRepository<CellsData> _CellsData;
        private readonly IRepository<RectData> _RectData;
        private readonly Funcs _funcs;

        /// <summary>
        /// Список настроек пирометров
        /// </summary>
        private List<ActSettings> PirSettingsList { get; set; }


        string filename { get; set; }
        string fullpath { get; set; }

        public CsvCalculation(string fpath, string fn, IServiceProvider scopeFactory, ILogger<CsvCalculation> logger, MainSettings mainsettings, List<PirometersSettings> pirSettings)
        {
            _logger = logger;
            _mainSettings = mainsettings;
            _pirSettings = pirSettings;

            _scopeFactory = scopeFactory;
            var scope = _scopeFactory.CreateScope();
            _ActSet = scope.ServiceProvider.GetRequiredService<ActSettingsRepository>();
            _SCAN = scope.ServiceProvider.GetRequiredService<ScanRepository>();
            _CellsData = scope.ServiceProvider.GetRequiredService<IRepository<CellsData>>();
            _AreasData = scope.ServiceProvider.GetRequiredService<IRepository<AreasData>>();
            _CalkData = scope.ServiceProvider.GetRequiredService<CalkDataRepository>();
            _RectData = scope.ServiceProvider.GetRequiredService<IRepository<RectData>>();
            _funcs = scope.ServiceProvider.GetRequiredService<Funcs>();

            PirSettingsList = _ActSet.GetAllAsync().Result.ToList();
            
            filename = fn;
            fullpath = fpath;


        }

        public CsvCalculation(int scan_id, IServiceProvider scopeFactory, ILogger<CsvCalculation> logger, MainSettings mainsettings, List<PirometersSettings> pirSettings)
        {
            _logger = logger;
            _mainSettings = mainsettings;
            _pirSettings = pirSettings;

            _scopeFactory = scopeFactory;
            var scope = _scopeFactory.CreateScope();
            _ActSet = scope.ServiceProvider.GetRequiredService<ActSettingsRepository>();

            _SCAN = scope.ServiceProvider.GetRequiredService<ScanRepository>();
            _CellsData = scope.ServiceProvider.GetRequiredService<IRepository<CellsData>>();
            _AreasData = scope.ServiceProvider.GetRequiredService<IRepository<AreasData>>();
            _CalkData = scope.ServiceProvider.GetRequiredService<CalkDataRepository>();
            _RectData = scope.ServiceProvider.GetRequiredService<IRepository<RectData>>();
            _funcs = scope.ServiceProvider.GetRequiredService<Funcs>();

            PirSettingsList = _ActSet.GetAllAsync().Result.ToList();

            SCAN_ID = scan_id;
        }

        public void FirstCalculation()
        {

            //Выведем инфу о созданном файле
            _logger.LogInformation("\n--------------------------------\nFile created: " + filename + "\n--------------------------------");

            //Альтернативный способ по имени файла
            try
            {
                string[] words = filename.Split('_');
                CSVpirNUM = int.Parse(words[0]);
                CSVmatID = int.Parse(words[1]);
                CSVpassID = int.Parse(words[2]);
                Dire = words[3].Contains("+") ? true : false;
            }
            catch
            {
                throw new Exception("\nWrong name of CSV file. Should be NUM_NUM_NUM_ANY.CSV");
            }


            //Преобразуем данные CSV файла в 2D массив
            var matrix = GetArray(fullpath);

            //Прописать логику определения номера пирометра по названию файла
            int pirNum = CSVpirNUM;
            PirSettingsList = _ActSet.GetAllAsync().Result.ToList();
            ActSettings pirSet = PirSettingsList.Where(x => x.PIR_NUMBER == pirNum).FirstOrDefault();

            var pirs = _pirSettings.Where(x => x.PirNumber == pirNum).FirstOrDefault();
            
            _logger.LogInformation("\nPyrometer" + pirNum + " ---> CSV converted " + DateTime.Now.ToString());
            //Преобразуем 2D массив в список 3D векторов
            var mav = ConvertToVectorMatrix(matrix, pirs);
            _logger.LogInformation("\nPyrometer" + pirNum + " ---> 3D Vectors array created " + DateTime.Now.ToString());




            using (var memb = DrawMatrixBoxEmgu(mav, 0.1, pirSet, pirs))
            {
                if (memb.Pict1 != null && memb.Pict2 != null)
                {

                    //Сохраним в базу изменения
                    var scan = Save_SCAN(pirNum, memb.Pict1, filename).Result;

                    //Обрежем ненужные точки из массива
                    List<Vector3> cuttedMav = CutVectorsByRotatedRect(mav, memb.RR, pirs);
                    if (cuttedMav == null) throw new Exception("No points detected in countour");

                    //Поворачиваем список 3D векторов на угол выделенного прямоугольника - выпрямляем
                    float angle = memb.RR.Angle;
                    int rot_direction = -1;
                    G_INGOT_HEIGHT = memb.RR.Size.Height;
                    G_INGOT_WIDTH = memb.RR.Size.Width;
                    if (angle > 45) { 
                        angle = 90 - angle; 
                        rot_direction = 1; 
                        G_INGOT_HEIGHT = memb.RR.Size.Width;
                        G_INGOT_WIDTH = memb.RR.Size.Height;
                    }

                    List<Vector3> rotatedCuttedMav = angle != 90 && angle != 0 ? RotateMatrix(cuttedMav, (float)((angle * rot_direction) * Math.PI / 180)) : cuttedMav;
                    _logger.LogInformation("\nPyrometer" + pirSet.PIR_NUMBER + " ---> Array rotated at " + angle + " degrees, " + DateTime.Now.ToString());

                    //Сохраним в базу изменения
                    var rd = Save_RECT_DATA(rotatedCuttedMav, pirSet.PIR_NUMBER, memb.Pict2, memb.RR).Result;

                    //Пересчитаем ячейки и области
                    MakeCalculations(pirSet, rd.ID, rotatedCuttedMav, scan.ID, pirs);

                }
                else
                {
                    throw new Exception("There are problems to detect rectangle of ingot");

                }

            }
        }

        /// <summary>
        /// Функция преобразования данных файла в 2D массив
        /// </summary>
        /// <param name="filename">Путь к файлу</param>
        /// <returns></returns>
        public List<List<double>> GetArray(string filename)
        {
            while (IsFileLocked(new FileInfo(filename))) { Task.Delay(500); }
            string readLine = "";
            string[] words;
            int pirnum = 0;
            List<List<double>> firstMatrix = new List<List<double>>();
            using (FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read))
            {
                using (var reader = new StreamReader(filename, Encoding.UTF8, false, 4096))
                {
                    for (int i = 0; i < 13; i++)
                    {

                        readLine = reader.ReadLine();
                        if (readLine.Contains("Time sampled"))
                        {
                            string TimeStr = readLine.Replace("Time sampled:;", "").Replace(";", "");
                            _DT_CSV = DateTime.Parse(TimeStr);
                        }


                        if (readLine.Contains("Product"))
                        {
                            string productStr = readLine.Replace("Product:", "").Replace(";", "");
                            words = productStr.Split('_');
                            pirnum = int.Parse(words[0]);
                        }

                        if (readLine.Contains("Length in mm:"))
                        {
                            string LenStr = readLine.Replace("Length in mm:", "").Replace(";", "");
                            if (pirnum > 2 && Convert.ToInt32(LenStr) < 900) throw new Exception("Scan is too short");
                        }
                    }
                    using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
                    {
                        var xxx = csv.GetRecords<dynamic>().ToList();

                        foreach (var word in xxx)
                        {
                            IDictionary<string, object> propertyValues = word;

                            foreach (var property in propertyValues.Keys)
                            {
                                string NoSpaces = propertyValues[property].ToString().Replace(" ", "");
                                string NoDoubleComma = NoSpaces.ToString().Replace(";;", ";");
                                if (_mainSettings.CommaFormat)
                                {
                                    string NoDots = NoDoubleComma.Replace(".", ",");
                                    words = NoDots.ToString().Split(';');
                                }
                                else
                                {
                                    words = NoDoubleComma.ToString().Split(';');
                                }


                                firstMatrix.Add(Array.ConvertAll(words, Double.Parse).ToList());
                            }
                        }

                        csv.Dispose();
                    }
                    reader.DiscardBufferedData();
                    reader.Close();
                    reader.Dispose();
                }
                fs.Close();
            }
            return firstMatrix;
        }

        private bool IsFileLocked(FileInfo file)
        {
            FileStream stream = null;

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            //file is not locked
            return false;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects)
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                disposedValue = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~CsvCalculation()
        // {
        //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        public void MakeCalculations(ActSettings pirSet, int rd_id, List<Vector3> rotatedCuttedMav, int scan_Id, PirometersSettings pirs)
        {

            //Обрабатываем список 3D векторов для получения средних значений
            List<Vector3> mo = PixelizeArray(rotatedCuttedMav, pirSet, pirs);
            _logger.LogInformation("\nPyrometer" + pirSet.PIR_NUMBER + " ---> Array pixelized " + DateTime.Now.ToString());

            //Рисуем поле средних температур
            byte[] Pict3 = DrawPixelateMatrixCV(mo,  false, 0.1, pirSet, pirs);
            _logger.LogInformation("\nPyrometer" + pirSet.PIR_NUMBER + " ---> Picture 3 created " + DateTime.Now.ToString());

            //Сохраним в базу изменения
            var cd = Save_CELLS_DATA(mo, pirSet.PIR_NUMBER, Pict3).Result;

            //Сохраним в базу изменения
            var calkd = Save_CALK_DATA(pirSet.PIR_NUMBER, scan_Id, cd.ID, rd_id, pirSet.SETTINGS_ID).Result;

            //Сохраним в базу изменения
            Save_AREAS_DATA(mo, pirSet, calkd.ID, pirs).Wait();

            _logger.LogInformation("\nPyrometer" + pirSet.PIR_NUMBER + " ---> FINISH!!! " + DateTime.Now.ToString() + "\n========================================");


        }

        public async Task<Scan> Save_SCAN(int PirNum, byte[] Pict, string fn)
        {

            //Сохраним скан
            Scan NewScan = new Scan()
            {
                PIR_NUMBER = PirNum,
                DT_FIX = DateTime.Now,
                FORWARD_DIRECTION = Dire,
                MAT_ID = CSVmatID,
                PASS_NO = CSVpassID,
                PICT_DEFAULT = _mainSettings.SavePreviewCSVinSCANS ? Pict : null,
                CSV_FILE_NAME = fn,
                DT_CSV = _DT_CSV
            };
            try
            {
                await _SCAN.CreateAsync(NewScan);
                _logger.LogInformation("\nPyrometer " + PirNum + " ---> New row in SCANS table added " + DateTime.Now.ToString());

            }
            catch (Exception ee)
            {
                _logger.LogError(ee.ToString());
            }
            return NewScan;

        }

        public async Task Save_AREAS_DATA(List<Vector3> PixMat, ActSettings pirSet, int CalkData_Id, PirometersSettings pirs)
        {
            List<AreasData> lad = new List<AreasData>();

            var x_max = (int)PixMat.Max(x => x.X);
            var y_max = (int)PixMat.Max(x => x.Y);
            int IncEachSize = _pirSettings.Where(x => x.PirNumber == pirSet.PIR_NUMBER).FirstOrDefault().RectIncrementEachSize;

            var PixMatSelected = PixMat.Where(d => d.X >= CircumscribedRect.X && d.X <= CircumscribedRect.X + CircumscribedRect.Width
                                                && d.Y >= CircumscribedRect.Y && d.Y <= CircumscribedRect.Y + CircumscribedRect.Height);
            lad.Add(new AreasData()
            {
                CALKDATA_ID = CalkData_Id,
                AREA_HEIGHT = CircumscribedRect.Height,// origin_y_offset_bottom - origin_y_offset_top,
                AREA_WIDTH = CircumscribedRect.Width, //origin_x_offset_right - origin_x_offset_left,
                CELL_UP_LEFT_X = CircumscribedRect.X,// origin_x_offset_left,
                CELL_UP_LEFT_Y = CircumscribedRect.Y,// origin_y_offset_top,
                ROW_NUM = 0,
                COL_NUM = 0,
                AVGT = PixMatSelected.Average(s => s.Z),
                MINT = PixMatSelected.Min(s => s.Z),
                MAXT = PixMatSelected.Max(s => s.Z),
                TYPE_AREA = 2
            });

            var PixMatInscribed = PixMatSelected.Where(d => d.X >= InscribedRect.X && d.X <= InscribedRect.X + InscribedRect.Width
                                                         && d.Y >= InscribedRect.Y && d.Y <= InscribedRect.Y + InscribedRect.Height);

            lad.Add(new AreasData()
            {
                CALKDATA_ID = CalkData_Id,
                AREA_HEIGHT = InscribedRect.Height,// origin_y_offset_bottom - origin_y_offset_top,
                AREA_WIDTH = InscribedRect.Width, //origin_x_offset_right - origin_x_offset_left,
                CELL_UP_LEFT_X = InscribedRect.X,// origin_x_offset_left,
                CELL_UP_LEFT_Y = InscribedRect.Y,// origin_y_offset_top,
                ROW_NUM = 0,
                COL_NUM = 0,
                AVGT = PixMatInscribed.Average(s => s.Z),
                MINT = PixMatInscribed.Min(s => s.Z),
                MAXT = PixMatInscribed.Max(s => s.Z),
                TYPE_AREA = 3
            });

            int area_x_offset_left = 0;
            int area_x_offset_right = 0;
            int area_y_offset_top = 0;
            int area_y_offset_bottom = 0;

            if (pirs.InscribeRectArea)
            {
                area_x_offset_left = InscribedRect.X + (int)Math.Round((pirSet.SETTTINGS.CUTSETTINGS.CUT_LEFT) / (decimal)pirSet.SETTTINGS.CELLSETTINGS.CELL_WIDTH);
                area_x_offset_right = InscribedRect.X + InscribedRect.Width - (int)Math.Round((pirSet.SETTTINGS.CUTSETTINGS.CUT_RIGHT) / (decimal)pirSet.SETTTINGS.CELLSETTINGS.CELL_WIDTH);
                area_y_offset_top = InscribedRect.Y + (int)Math.Round((pirSet.SETTTINGS.CUTSETTINGS.CUT_TOP) / (decimal)pirSet.SETTTINGS.CELLSETTINGS.CELL_HEIGHT);
                area_y_offset_bottom = InscribedRect.Y + InscribedRect.Height - (int)Math.Round((pirSet.SETTTINGS.CUTSETTINGS.CUT_BOTTOM) / (decimal)pirSet.SETTTINGS.CELLSETTINGS.CELL_HEIGHT);
            }
            else 
            {
                area_x_offset_left = (int)Math.Round((pirSet.SETTTINGS.CUTSETTINGS.CUT_LEFT + IncEachSize) / (decimal)pirSet.SETTTINGS.CELLSETTINGS.CELL_WIDTH);
                area_x_offset_right = x_max - (int)Math.Round((pirSet.SETTTINGS.CUTSETTINGS.CUT_RIGHT + IncEachSize) / (decimal)pirSet.SETTTINGS.CELLSETTINGS.CELL_WIDTH);
                area_y_offset_top =  (int)Math.Round((pirSet.SETTTINGS.CUTSETTINGS.CUT_TOP + IncEachSize) / (decimal)pirSet.SETTTINGS.CELLSETTINGS.CELL_HEIGHT);
                area_y_offset_bottom = y_max - (int)Math.Round((pirSet.SETTTINGS.CUTSETTINGS.CUT_BOTTOM + IncEachSize) / (decimal)pirSet.SETTTINGS.CELLSETTINGS.CELL_HEIGHT);
            }
            PixMatSelected = PixMat.Where(d => d.X >= area_x_offset_left && d.X <= area_x_offset_right
                                            && d.Y >= area_y_offset_top  && d.Y <= area_y_offset_bottom);
            lad.Add(new AreasData()
            {
                CALKDATA_ID = CalkData_Id,
                AREA_HEIGHT = area_y_offset_bottom - area_y_offset_top,
                AREA_WIDTH = area_x_offset_right - area_x_offset_left,
                CELL_UP_LEFT_X = area_x_offset_left,
                CELL_UP_LEFT_Y = area_y_offset_top,
                ROW_NUM = 0,
                COL_NUM = 0,
                AVGT = PixMatSelected.Average(s => s.Z),
                MINT = PixMatSelected.Min(s => s.Z),
                MAXT = PixMatSelected.Max(s => s.Z),
                TYPE_AREA = 1
            });

            int area_height = (int)(area_y_offset_bottom - area_y_offset_top) / pirSet.SETTTINGS.AREASETTINGS.ARIA_ROW_QUANT;
            int area_width = (int)(area_x_offset_right - area_x_offset_left) / pirSet.SETTTINGS.AREASETTINGS.ARIA_COL_QUANT;

            if (area_height > 0 && area_width > 0)
            {

                float AGVT_OLD = 0, MINT_OLD = 0, MAXT_OLD = 0, agvt_cur = 0, mint_cur = 0, maxt_cur = 0;

                for (int x = 0; x < pirSet.SETTTINGS.AREASETTINGS.ARIA_COL_QUANT; x++)
                {
                    int DifArea_width = (x == pirSet.SETTTINGS.AREASETTINGS.ARIA_COL_QUANT - 1) ? (area_x_offset_right - area_x_offset_left - x * area_width) : area_width;
                    var PixMatSelectedX = PixMat.Where(d => d.X >= area_x_offset_left + x * area_width && d.X <= area_x_offset_left + x * area_width + DifArea_width);

                    for (int y = 0; y < pirSet.SETTTINGS.AREASETTINGS.ARIA_ROW_QUANT; y++)
                    {

                        int DifArea_height = (y == pirSet.SETTTINGS.AREASETTINGS.ARIA_ROW_QUANT - 1) ? (area_y_offset_bottom - area_y_offset_top - y * area_height) : area_height;

                        try
                        {
                            var PixMatSelectedXY = PixMatSelectedX.Where(d => d.Y >= area_y_offset_top  + y * area_height && d.Y <= area_y_offset_top  + y * area_height + DifArea_height);
                            agvt_cur = PixMatSelectedXY.Average(s => s.Z);
                            mint_cur = PixMatSelectedXY.Min(s => s.Z);
                            maxt_cur = PixMatSelectedXY.Max(s => s.Z);

                        }
                        catch
                        {
                            agvt_cur = AGVT_OLD;
                            mint_cur = MINT_OLD;
                            maxt_cur = MAXT_OLD;
                        }
                        AGVT_OLD = agvt_cur;
                        MINT_OLD = mint_cur;
                        MAXT_OLD = maxt_cur;

                        lad.Add(new AreasData()
                        {

                            CALKDATA_ID = CalkData_Id,
                            AREA_HEIGHT = DifArea_height,
                            AREA_WIDTH = DifArea_width,
                            CELL_UP_LEFT_X = area_x_offset_left + x * area_width,
                            CELL_UP_LEFT_Y = area_y_offset_top + y * area_height,
                            ROW_NUM = y,
                            COL_NUM = x,
                            AVGT = agvt_cur,
                            MINT = mint_cur,
                            MAXT = maxt_cur,
                            TYPE_AREA = 0
                        });
                    }
                }
                try
                {
                    await _AreasData.CreateRangeAsync(lad);
                    _logger.LogInformation("\nPyrometer" + pirSet.PIR_NUMBER + " ---> New rows in AREAS_DATA table added " + DateTime.Now.ToString());

                }
                catch (Exception ee)
                {
                    _logger.LogError(ee.ToString());
                }
            }


        }

        /// <summary>
        /// Обновление ячеек в соответствии с актуальными настройками пирометра данного скана
        /// </summary>
        /// <returns></returns>
        public async Task UpdateAvarageTempFieldAsync()
        {
            //Получим текущий скан
            Scan sc = await _SCAN.GetByIdAsync(SCAN_ID);

            //Обновим направление
            Dire = sc.FORWARD_DIRECTION;

            //Обновим настройки пирометров
            PirSettingsList = _ActSet.GetAllAsync().Result.ToList();

            //Выберем текущие настройки пирометра
            ActSettings pirSet = PirSettingsList.Where(x => x.PIR_NUMBER == sc.PIR_NUMBER).FirstOrDefault();
            var pirs = _pirSettings.Where(x => x.PirNumber == sc.PIR_NUMBER).FirstOrDefault();

            var CDs = await _CalkData.GetByScanIdAsync(SCAN_ID);
            var CD = CDs.FirstOrDefault();

            //Десериализуем массив данных
            List<Vector3> rotatedMav = _funcs.DecodeByteArray444(CD.RECTDATA.RECT_VECTORS);

            //Пересчитаем ячейки и области
            MakeCalculations(pirSet, CD.RECTDATA.ID, rotatedMav, CD.SCAN_ID, pirs/*, false*/);


        }

        public async Task<RectData> Save_RECT_DATA(List<Vector3> rotatedMav, int PirNum, byte[] Pict, RotatedRect rectangle)
        {
            RectData rd = new RectData()
            {
                RECT_VECTORS = _funcs.CodeToByteArray444(rotatedMav),
                PICT_RECT = Pict,
                INGOT_HEIGHT = G_INGOT_HEIGHT,//rectangle.Size.Height,//rotatedMav.Max(x => x.X) - rotatedMav.Min(x => x.X), 
                INGOT_WIDTH = G_INGOT_WIDTH//rectangle.Size.Width//rotatedMav.Max(x => x.Y) - rotatedMav.Min(x => x.Y),
            };
            try
            {
                await _RectData.CreateAsync(rd);
                _logger.LogInformation("Pyrometer" + PirNum + " ---> A row in RECT_DATA table added " + DateTime.Now.ToString());

            }
            catch (Exception ee)
            {
                _logger.LogError(ee.ToString());
            }
            return rd;
        }

        public async Task<CellsData> Save_CELLS_DATA(List<Vector3> mo, int PirNum, byte[] Pict)
        {

            CellsData cd = new CellsData()
            {
                CELLS_VECTORS = _funcs.CodeToByteArray224_sorted(save_ma),
                PICT_CELLS = Pict,
                COL_QUANT = (int)mo.Max(x => x.X),
                ROW_QUANT = (int)mo.Max(x => x.Y)
            };
            try
            {
                await _CellsData.CreateAsync(cd);
                _logger.LogInformation("Pyrometer" + PirNum + " ---> A row in CELL_DATA table added " + DateTime.Now.ToString());

            }
            catch (Exception ee)
            {
                _logger.LogError(ee.ToString());
            }
            return cd;
        }

        public async Task<CalkData> Save_CALK_DATA(int PirNum, int Scan_Id, int CellData_Id, int Rect_data_id, int Settings_Id)
        {

            CalkData cd = new CalkData()
            {
                CELLDATA_ID = CellData_Id,
                SCAN_ID = Scan_Id,
                RECT_DATA_ID = Rect_data_id,
                SETTINGS_ID = Settings_Id,
                CALK_DT = DateTime.Now,
            };
            try
            {
                await _CalkData.CreateAsync(cd);
                _logger.LogInformation("Pyrometer" + PirNum + " ---> A row in CALK_DATA table added " + DateTime.Now.ToString());

            }
            catch (Exception ee)
            {
                _logger.LogError(ee.ToString());
            }
            return cd;

        }


        /// <summary>
        /// Преобразование исходной считанной матрицы в массив 3D векторов
        /// </summary>
        /// <param name="m">Исходная считанная матрица из CSV файла</param>
        /// <param name="pirs">Настройка конкретного пирометра</param>
        /// <returns></returns>
        public List<Vector3> ConvertToVectorMatrix(List<List<double>> m, PirometersSettings pirs)
        {
            MaxDistanceCSV = 0;
            double old_Distance = -1;
            List<List<double>> m_filtered = new List<List<double>>();
            for (int i = 0; i < m.Count; i++)
            {
                if ((i < m.Count - 1) && (m[i + 1][2] - m[i][2] > MaxDistanceCSV)) MaxDistanceCSV = (int)(m[i + 1][2] - m[i][2]);
                if (old_Distance != m[i][2])
                {
                    m_filtered.Add(m[i]);
                }
                old_Distance = m[i][2];
            }

            List<Vector3> maVs = new List<Vector3>();
            T_min = pirs.Tmax;
            T_max = pirs.Tmin;
            for (int i = 0; i < m_filtered.Count; i++)
            {
                for (int j = 3; j < 1003; j++)
                {
                    var t = (float)m_filtered[i][j] * pirs.TemperatureKoeff;
                    if (t < T_min) T_min = t;
                    if (t > T_max) T_max = t;
                    maVs.Add(new Vector3((float)m_filtered[i][2], pirs.GetXCoord(j - 3), t));
                }
            }

            return maVs;
        }

        /// <summary>
        /// Функция находит средние значения по оси Z исходя из исходных данных
        /// </summary>
        /// <param name="m">Повёрнутая матрица с отцентрированой по оси деталью</param>
        public List<Vector3> PixelizeArray(List<Vector3> m, ActSettings s, PirometersSettings pirs)//, RotatedRect rect)
        {
            List<Vector3> mo = new List<Vector3>();
            List<Vector3> xmo = new List<Vector3>();
            List<Vector3> mo_temp = new List<Vector3>();
            List<Vector3> mo_temp_old = new List<Vector3>();
            var x_max = m.Max(x => x.X);
            var x_min = m.Min(x => x.X);
            var y_max = m.Max(x => x.Y);
            var y_min = m.Min(x => x.Y);
            float la, la_old = 600;
            int x_count = (int)Math.Round((x_max - x_min) / s.SETTTINGS.CELLSETTINGS.CELL_WIDTH);
            int y_count = (int)Math.Round((y_max - y_min) / s.SETTTINGS.CELLSETTINGS.CELL_HEIGHT);
            save_ma = new float[x_count+1, y_count+1];

            for (int i = 0; i <= x_count; i++)
            {
                
                mo_temp = m.Where(e => e.X >= x_min + i * s.SETTTINGS.CELLSETTINGS.CELL_WIDTH && e.X <= x_min + (i + 1) * s.SETTTINGS.CELLSETTINGS.CELL_WIDTH).ToList();
                if (mo_temp.Count == 0) mo_temp = mo_temp_old;
                for (int j = 0; j <= y_count; j++)
                {
                    if (mo_temp.Where(e => e.Y >= y_min + j * s.SETTTINGS.CELLSETTINGS.CELL_HEIGHT && e.Y <= y_min + (j + 1) * s.SETTTINGS.CELLSETTINGS.CELL_HEIGHT).Any())
                    {
                        var l = mo_temp.Where(e => e.Y >= y_min + j * s.SETTTINGS.CELLSETTINGS.CELL_HEIGHT && e.Y <= y_min + (j + 1) * s.SETTTINGS.CELLSETTINGS.CELL_HEIGHT);
                        la = l.Average(e => e.Z);
                    }
                    else
                    {
                        la = la_old;
                        if (i > 0)
                        {
                            if (pirs.ForewardDistanceReverse && Dire || pirs.BackwardDistanceReverse && !Dire)
                            {
                                la = mo.Where(x => x.X == x_count - i + 1  && x.Y == j).FirstOrDefault().Z;
                            }
                            else
                            {
                                la = mo.Where(x => x.X == i - 1 && x.Y == j).FirstOrDefault().Z;
                            }
                        }
                    }
                    la_old = la;
                    if (pirs.ForewardDistanceReverse && Dire || pirs.BackwardDistanceReverse && !Dire)
                    {
                        mo.Add(new Vector3(x_count - i, j, la));
                        save_ma[x_count - i, j] = la;
                    }
                    else
                    {
                        mo.Add(new Vector3(i, j, la));
                        save_ma[i, j] = la;
                    }
                }
                mo_temp_old = mo_temp;
            }
            return mo;
        }





        /// <summary>
        /// Поворот матрицы на заданный угол, рад
        /// </summary>
        /// <param name="m">Исходная матрица</param>
        /// <param name="angle">Заданный угол, рад</param>
        /// <returns>Получившаяся матрица</returns>
        public List<Vector3> RotateMatrix(List<Vector3> m, float angle)
        {
            List<Vector3> ma = new List<Vector3>();
            var q = Quaternion.CreateFromAxisAngle(new Vector3(0, 0, 1), angle);
            foreach(var v in m)
            {
                lock (ma) ma.Add(Vector3.Transform(v, q));
            }
            return ma;
        }

        /// <summary>
        /// Функция создания графического файла поля температур
        /// </summary>
        /// <param name="m">Исходная матрица</param>
        /// <param name="scale_Koeff">Коэффициент масштабирования</param>
        /// <param name="s">Настройки пирометра</param>
        /// <param name="pirs">Настройка пирометра</param>
        public MemBox DrawMatrixBoxEmgu(List<Vector3> m, double scale_Koeff, ActSettings s, PirometersSettings pirs)
        {
            var x_max = m.Max(x => x.X);
            var x_min = m.Min(x => x.X);
            var y_max = m.Max(x => x.Y);
            var y_min = m.Min(x => x.Y);

            int sX = (int)(Math.Round((x_max - x_min) * scale_Koeff));
            int sY = (int)(Math.Round((y_max - y_min) * scale_Koeff));

            var mb = new MemBox();

            using (Image<Bgr, byte> b = new Image<Bgr, byte>(sX, sY))
            using (Image<Gray, byte> grayImage = new Image<Gray, byte>(sX, sY))
            {

                foreach (var v in m)
                {
                    //_logger.LogInformation("x = " + v.X + "; y = " + v.Y) ;
                    var rect = new Rectangle(
                                (int)(Math.Round((v.X - x_min) * scale_Koeff)),
                                (int)(Math.Round((v.Y - y_min) * scale_Koeff)),
                                (int)Math.Round(MaxDistanceCSV * scale_Koeff),//s.SETTTINGS.CELLSETTINGS.CELL_WIDTH,
                                (int)Math.Round(s.SETTTINGS.CELLSETTINGS.CELL_HEIGHT * scale_Koeff)
                                );
                    b.Draw(rect, new Bgr(GetColourNew(v.Z, pirs.Tmin, pirs.Tmax)), -1);
                    grayImage.Draw(rect, new Gray(GetColourGrayNew(v.Z, T_min, T_max)), -1);
                    
                }
                //b.Save(_mainSettings.pngfilelocation + "temperEMGU.jpg");
                using (Image<Gray, byte> segmentedImage = grayImage.InRange(new Gray(255 * _pirSettings.Where(x => x.PirNumber == s.PIR_NUMBER).FirstOrDefault().DetectionKoeff), new Gray(255)))
                {
                    //segmentedImage.Save(_mainSettings.pngfilelocation + "temperEMGUsegm.jpg");

                    using (Image<Bgr, byte> segmentedImageC = grayImage.Convert<Bgr, byte>())
                    using (Image<Bgr, byte> segmentedImageColo = segmentedImage.Convert<Bgr, byte>())
                    using (Mat hierarchy = new Mat())
                    using (VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint())
                    {
                        CvInvoke.FindContours(segmentedImage, contours, hierarchy, Emgu.CV.CvEnum.RetrType.External, ChainApproxMethod.ChainApproxSimple);
                        if (contours.Size == 0) throw new Exception("No contours found");
                        
                        //Find maximum contour
                        double ca_max = 0;
                        int contourNum = 0;
                        for (int i = 0; i < contours.Size; i++)
                        {
                            var ca = CvInvoke.ContourArea(contours[i], false);
                            if (ca > ca_max)
                            {
                                ca_max = ca;
                                contourNum = i;
                            }
                        }

                        using (VectorOfPoint contour = contours[contourNum])
                        using (VectorOfPoint nonZer = new VectorOfPoint())
                        {
                            RotatedRect rr = CvInvoke.MinAreaRect(contour);
                            CvInvoke.Polylines(segmentedImageC, Array.ConvertAll(rr.GetVertices(), Point.Round), true,
                                new Bgr(Color.Red).MCvScalar, 5);


                            mb.Pict1 = (pirs.ForewardDistanceReverse && Dire || pirs.BackwardDistanceReverse && !Dire) ? b.Flip(FlipType.Horizontal).ToJpegData(80) : b.ToJpegData(80);
                            //b.Save(_mainSettings.pngfilelocation + "temperEMGU.jpg");
                            mb.Pict2 = (pirs.ForewardDistanceReverse && Dire || pirs.BackwardDistanceReverse && !Dire) ? segmentedImageC.Flip(FlipType.Horizontal).ToJpegData(80) : segmentedImageC.ToJpegData(80);
                            //segmentedImageC.Save(_mainSettings.pngfilelocation + "temperEMGUsegmC.jpg");
                            mb.RR = new RotatedRect(new PointF(rr.Center.X / (float)scale_Koeff, rr.Center.Y / (float)scale_Koeff),
                                                    new SizeF(rr.Size.Width / (float)scale_Koeff, rr.Size.Height / (float)scale_Koeff),
                                                    rr.Angle);
                            return mb;
                        }
                    }
                }
            }
        }



        public Rectangle findMaxRectMatr(Matrix<byte> src)
        {

            float[,] W = new float[src.Rows, src.Cols];
            float[,] H = new float[src.Rows, src.Cols];

            var maxRect = new Rectangle(0, 0, 0, 0);
            float maxArea = 0;

            for (int r = 0; r < src.Rows; ++r)
            {
                for (int c = 0; c < src.Cols; ++c)
                {
                    if (src[r, c] == 255)
                    {
                        H[r, c] = 1 + ((r > 0) ? H[r - 1, c] : 0);
                        W[r, c] = 1 + ((c > 0) ? W[r, c - 1] : 0);
                    }

                    float minw = W[r, c];
                    for (int h = 0; h < H[r, c]; ++h)
                    {
                        minw = Math.Min(minw, W[r - h, c]);
                        float area = (h + 1) * minw;
                        if (area > maxArea)
                        {
                            maxArea = area;
                            maxRect = new Rectangle(c - (int)minw + 1, r - h, (c + 1) - (c - (int)minw + 1), (r + 1) - (r - h));// Point(c - minw + 1, r - h), Point(c+1, r+1));
                        }
                    }
                }
            }

            return maxRect;
        }


        /// <summary>
        /// Функция создания графического файла поля температур с учётом преобразования
        /// </summary>
        /// <param name="m">Матрица ячеек</param>
        /// <param name="gray">Флаг отрисовки поля в градациях серого</param>
        /// <param name="scale_Koeff">Коэффициент масштабирования</param>
        /// <param name="s">Настройки пирометра</param>
        public byte[] DrawPixelateMatrixCV(List<Vector3> m, bool gray, double scale_Koeff, ActSettings s, PirometersSettings pirs)
        {

            var x_count = m.Max(x => x.X);
            var y_count = m.Max(x => x.Y);
            T_min = m.Min(x=>x.Z);
            T_max = m.Max(x=>x.Z);


            byte[] res;
            var cw = 1;// (int)(s.SETTTINGS.CELLSETTINGS.CELL_WIDTH * scale_Koeff);
            var ch = 1;// (int)(s.SETTTINGS.CELLSETTINGS.CELL_HEIGHT * scale_Koeff);

            double sX = x_count * cw;
            double sY = y_count * ch;
            var clevel = 255 * _pirSettings.Where(x => x.PirNumber == s.PIR_NUMBER).FirstOrDefault().DetectionKoeff;
            using (Image<Bgr, byte> b = new Image<Bgr, byte>((int)sX, (int)sY))
            using (Image<Gray, byte> grayImage = new Image<Gray, byte>((int)sX, (int)sY))
            {


                foreach (var v in m)
                {
                    var rect = new Rectangle(
                        (int)(v.X * cw),
                        (int)(v.Y * ch),
                        cw,
                        ch
                    );

                    b.Draw(rect, new Bgr(GetColourNew(v.Z, pirs.Tmin, pirs.Tmax)), -1);
                    grayImage.Draw(rect, new Gray(GetColourGrayNew(v.Z, T_min, T_max)), -1);
                }

                using (Image<Gray, byte> segmentedImage = grayImage.InRange(new Gray(255 * _pirSettings.Where(x => x.PirNumber == s.PIR_NUMBER).FirstOrDefault().DetectionKoeff), new Gray(255)))
                using (Image<Gray, byte> newImageCopy = new Image<Gray, byte>((int)sX, (int)sY))
                using (Mat hierarchy = new Mat())
                using (VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint())
                {
                    CvInvoke.FindContours(segmentedImage, contours, hierarchy, Emgu.CV.CvEnum.RetrType.External, ChainApproxMethod.ChainApproxSimple);
                    if (contours.Size == 0) throw new Exception("No contours found");

                    //Find maximum contour
                    double ca_max = 0;
                    int contourNum = 0;
                    for (int i = 0; i < contours.Size; i++)
                    {
                        var ca = CvInvoke.ContourArea(contours[i], false);
                        if (ca > ca_max)
                        {
                            ca_max = ca;
                            contourNum = i;
                        }
                    }
                    newImageCopy.SetValue(255);

                    CvInvoke.Polylines(newImageCopy, contours[contourNum], true, new Bgr(Color.Black).MCvScalar);
                    Matrix<byte> matrix = new Matrix<byte>(newImageCopy.Rows, newImageCopy.Cols, newImageCopy.NumberOfChannels);
                    newImageCopy.CopyTo(matrix);

                    InscribedRect = findMaxRectMatr(matrix);
                    CvInvoke.Rectangle(b, InscribedRect, new Bgr(Color.Black).MCvScalar, 1);

                    CvInvoke.Polylines(b, contours[contourNum], true, new Bgr(Color.Black).MCvScalar);

                    CircumscribedRect = CvInvoke.BoundingRectangle(contours[contourNum]);
                    CvInvoke.Rectangle(b, CircumscribedRect, new Bgr(Color.Black).MCvScalar, 1);

                    res = b.ToJpegData(100);
                }
            }
            return res;
        }



        /// <summary>
        /// Функция получения цвета в зависимости от значения в заданном диапазоне
        /// </summary>
        /// <param name="v">Заданное значение</param>
        /// <param name="vmin">Нижнее значение диапазона</param>
        /// <param name="vmax">Верхнее значение диапазона</param>
        /// <returns>Цвет RGB</returns>
        public Color GetColourNew(double v, double vmin, double vmax)
        {
            double r = 1.0, g = 1.0, b = 1.0; // white
            double dv;

            if (v < vmin)
                v = vmin;
            if (v > vmax)
                v = vmax;
            dv = vmax - vmin;

            if (v < (vmin + 0.25 * dv))
            {
                r = 0;
                g = 4 * (v - vmin) / dv;
            }
            else if (v < (vmin + 0.5 * dv))
            {
                r = 0;
                b = 1 + 4 * (vmin + 0.25 * dv - v) / dv;
            }
            else if (v < (vmin + 0.75 * dv))
            {
                r = 4 * (v - vmin - 0.5 * dv) / dv;
                b = 0;
            }
            else
            {
                g = 1 + 4 * (vmin + 0.75 * dv - v) / dv;
                b = 0;
            }

            return Color.FromArgb(255, (int)(r * 255), (int)(g * 255), (int)(b * 255));
        }

        /// <summary>
        /// Функция получения цвета в зависимости от значения в заданном диапазоне в градациях серого
        /// </summary>
        /// <param name="v">Заданное значение</param>
        /// <param name="vmin">Нижнее значение диапазона</param>
        /// <param name="vmax">Верхнее значение диапазона</param>
        /// <returns>Цвет RGB</returns>
        public int GetColourGrayNew(double v, double vmin, double vmax)
        {
            double intensity = 1.0;
            double dv;

            if (v < vmin)
                v = vmin;
            if (v > vmax)
                v = vmax;
            dv = vmax - vmin;

            intensity = (v - vmin) / dv;

            return (int)(intensity * 255);

        }

        private bool DoesRectangleContainPoint(RotatedRect rectangle, PointF point)
        {

            //Get the corner points.
            PointF[] corners = rectangle.GetVertices();
            Point[] co = new Point[4];

            for (int i = 0; i < 4; i++)
            {
                co[i] = Point.Round(corners[i]);
            }

            VectorOfPoint countour = new VectorOfPoint(co);

            //Check if the point is within the rectangle.
            double indicator = CvInvoke.PointPolygonTest(countour, point, false);
            bool rectangleContainsPoint = (indicator >= 0);
            return rectangleContainsPoint;
        }

        public List<Vector3> CutVectorsByRotatedRect(List<Vector3> m, RotatedRect rectangle, PirometersSettings pirs)
        {
            List<Vector3> newList = new List<Vector3>();
            rectangle.Size.Height = (float)(rectangle.Size.Height + 2 * pirs.RectIncrementEachSize);
            rectangle.Size.Width = (float)(rectangle.Size.Width + 2 * pirs.RectIncrementEachSize);
            //if (pirs.BackwardDistanceReverse || pirs.ForewardDistanceReverse) rectangle.Center.X = rectangle.Center.X - 2 * pirs.RectIncrementEachSize;
            foreach (var v in m)
            {
                if (DoesRectangleContainPoint(rectangle, new PointF(v.X, v.Y))) newList.Add(v);
            };
            return newList;

        }

    }
}
