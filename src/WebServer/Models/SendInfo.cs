﻿using System;

namespace WebServer.Models
{
    public class SendInfo
    {
        public SendInfo(string command, short tryCount)
        {
            Command = command;
            TryCount = tryCount;
            CurrentCount = 0;
        }

        public string Command { get; set; }
        public short TryCount { get; set; }
        public short CurrentCount { get; set; }

        public void Reset()
        {
            
            Command = "";
            CurrentCount = 0;
        }

        public void CheckNReset()
        {
            if (CurrentCount > TryCount)
            {
                Console.WriteLine("Stop sending commands (tryCounts("+ TryCount+") is exeeded)");
                Reset();
            }
        }
    }
}
