﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using PLC_S7_Exchange.Core;
using PLC_S7_Exchange.Data;
using S7.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using WebServer.Settings;

namespace WebServer.Models
{
    public class TimedHostedService : IHostedService, IDisposable
    {
        private int executionCount = 0;
        private readonly ILogger<TimedHostedService> _logger;
        private readonly PlcS7Exchange _plc;
        private Timer _timer;
        private readonly PiroDataBlock _pdb;
        private byte[] b;
        private MainSettings _mainSettings { get; }
        private int BadPlcConnectionCounter;


        public TimedHostedService(ILogger<TimedHostedService> logger, PlcS7Exchange plc, PiroDataBlock pdb, IOptions<MainSettings> mainsettings)
        {
            _logger = logger;
            _plc = plc;
            _pdb = pdb;
            _mainSettings = mainsettings.Value;
        }

        public Task StartAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("Timed Hosted Service running.");

            _timer = new Timer(DoWork, null, TimeSpan.Zero,
                TimeSpan.FromMilliseconds(150));

            return Task.CompletedTask;
        }

        public PiroDataBlock GetPiroDataBlock()
        {
            return _pdb;
        }

        private void DoWork(object state)
        {
            var count = Interlocked.Increment(ref executionCount);
            b = _plc.Read_DBAsync().Result;
            try
            {
                if (b != null && b.Length == 68)
                {
                    BadPlcConnectionCounter = 0;
                    _pdb.WatchDog = S7.Net.Types.DInt.FromByteArray(b.Skip(0).Take(4).ToArray()); //Вотчдог
                    for (int i = 0; i < _pdb.pirlist.Count; i++)
                    {
                        _pdb.pirlist[i].storageON = b[4 + i * 16].SelectBit(0);
                        _pdb.pirlist[i].blowing = b[4 + i * 16].SelectBit(1);
                        _pdb.pirlist[i].piro_id = S7.Net.Types.Int.FromByteArray(b.Skip(4 + i * 16 + 2).Take(2).ToArray());
                        _pdb.pirlist[i].mat_id = S7.Net.Types.DInt.FromByteArray(b.Skip(4 + i * 16 + 4).Take(4).ToArray());
                        _pdb.pirlist[i].path_id = S7.Net.Types.Int.FromByteArray(b.Skip(4 + i * 16 + 8).Take(2).ToArray());
                        _pdb.pirlist[i].speed_path = S7.Net.Types.Real.FromByteArray(b.Skip(4 + i * 16 + 10).Take(4).ToArray());
                        _pdb.pirlist[i].IsAProduct = S7.Net.Types.Int.FromByteArray(b.Skip(4 + i * 16 + 14).Take(2).ToArray());
                    }
                }
                else
                {
                    //_logger.LogError("TimedHostedService DoWork Read Null from PLC");
                    BadPlcConnectionCounter++;
                    if (BadPlcConnectionCounter > 3) BadPlcConnectionCounter = 3;
                }

            }
            catch (Exception e)
            {
                _logger.LogError("TimedHostedService DoWork Exception:", e.Message);
            }

        }

        public Task StopAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("Timed Hosted Service is stopping.");

            _timer?.Change(TimeSpan.Zero, TimeSpan.FromSeconds(1));

            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _timer?.Dispose();
        }


        public bool PlcConnectOK()
        {
            return (BadPlcConnectionCounter == 3) ? false : true;
        }


    }
}

