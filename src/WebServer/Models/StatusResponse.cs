﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebServer.Models
{
    public class StatusResponse
    {
        /// <summary>
        /// Связь с ПЛК ULS в норме
        /// </summary>
        public bool PLCconnectOK { get; set; }

        /// <summary>
        /// Связь с ПО Landscan WCA в норме
        /// </summary>
        public bool WSconnectOK { get; set; }


        /// <summary>
        /// Связь с OPC DA в норме
        /// </summary>
        public bool OPCconnectOK { get; set; }


    }
}
