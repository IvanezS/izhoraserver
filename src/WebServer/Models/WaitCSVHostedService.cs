﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using WebServer.Settings;

namespace WebServer.Models
{
    public class WaitCSVHostedService : IHostedService, IDisposable
    {
        private readonly ILogger<WaitCSVHostedService> _logger;
        private readonly ILogger<CsvCalculation> _loggerCsv;
        private Timer _timer;

        private MainSettings _mainSettings { get; }
        private List<PirometersSettings> _pirSettings { get; }

        /// <summary>
        /// Вотчдог
        /// </summary>
        private int executionCount = 0;

        private FileSystemWatcher _watcher;
        private readonly object _sync = new object();

        //public static Semaphore semaphore = new Semaphore(4, 5);
        private IServiceProvider _scopeFactory;

        public WaitCSVHostedService(ILogger<WaitCSVHostedService> logger, ILogger<CsvCalculation> loggerCsv, IOptions<MainSettings> mainsettings, IServiceProvider scopeFactory, IOptions<List<PirometersSettings>> pirSettings)
        {
            _logger = logger;
            _loggerCsv = loggerCsv;
            _mainSettings = mainsettings.Value;
            _pirSettings = pirSettings.Value;
            // Create a new FileSystemWatcher
            _watcher = CreateFSW();

            _scopeFactory = scopeFactory;
        }

        private FileSystemWatcher CreateFSW()
        {
            // Create a new FileSystemWatcher and set its properties.
            FileSystemWatcher watcher = new FileSystemWatcher(_mainSettings.filelocation, "*.csv");
            //Path.Combine
            // Watch for changes in LastAccess and LastWrite times, and
            // the renaming of files or directories.
            watcher.NotifyFilter = NotifyFilters.FileName;

            // Add event handlers.
            watcher.Created += OnFileCreated;
            watcher.Changed += OnFileCreated;
            watcher.Error += Watcher_Error;

            // Begin watching.
            watcher.EnableRaisingEvents = true;

            watcher.IncludeSubdirectories = true;

            watcher.InternalBufferSize = 65536;

            return watcher;
        }

        private void Watcher_Error(object sender, ErrorEventArgs e)
        {
            _logger.LogError("Watcher_Error: " + e.GetException().Message);
            try
            {
                ((FileSystemWatcher)sender).Dispose();

                FileSystemWatcher copy;

                lock (_sync)
                {
                    if (_watcher != sender)
                        return;

                    if (_watcher == null)
                        return;

                    copy = _watcher;
                    _watcher = CreateFSW();
                }

                copy.EnableRaisingEvents = false;
                copy.Dispose();

            }
            catch (Exception ex)
            {
                _logger.LogError("Watcher_Error Ex: ", ex);
            }
        }

        public Task StartAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("Wait CSV file Hosted Service running.");

            _timer = new Timer(DoWork, null, TimeSpan.Zero, TimeSpan.FromMilliseconds(100));

            return Task.CompletedTask;
        }

        private void DoWork(object state)
        {
           var count = Interlocked.Increment(ref executionCount);

           //_logger.LogInformation("Wait CSV Hosted Service is working. Count: {Count}", count);
        }

        public Task StopAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("Wait CSV file Hosted Service is stopping.");

            _timer?.Change(0, Timeout.Infinite);

            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _timer?.Dispose();
        }


        /// <summary>
        /// Функция обработки события появления нового файла
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        public void OnFileCreated(object source, FileSystemEventArgs e)
        {

            Thread t = new Thread(() => UdpateAllFieldsAsync(e));
            t.Name = e.Name;
            t.Start();

            GC.Collect();
        }



        public void UdpateAllFieldsAsync(FileSystemEventArgs e)
        {
            try
            {
                //semaphore.WaitOne();

                using (var c = new CsvCalculation(e.FullPath, e.Name, _scopeFactory, _loggerCsv, _mainSettings, _pirSettings))
                {
                    c.FirstCalculation();
                }
            }
            catch (Exception ee)
            {
                _logger.LogInformation("--------------------------------\nException: " + ee + "\n--------------------------------");
                
                GC.Collect();
            }
            finally
            {
                //semaphore.Release();
            }
        }

        /// <summary>
        /// Обновление ячеек в соответствии с актуальными настройками пирометра данного скана
        /// </summary>
        /// <param name="scan_Id"></param>
        /// <returns></returns>
        public async Task UpdateAvarageTempFieldAsync(int scan_Id)
        {

            Thread t = new Thread(() => UpdateAvarageTempField(scan_Id));
            t.Name = "Scan "+ scan_Id;
            t.Start();

            t.Join();
        }

        public void UpdateAvarageTempField(int scan_Id)
        {
            try
            {
                using (var c = new CsvCalculation(scan_Id, _scopeFactory, _loggerCsv, _mainSettings, _pirSettings))
                {
                    c.UpdateAvarageTempFieldAsync().Wait();
                }

            }
            catch (Exception ee)
            {
                _logger.LogInformation("--------------------------------\nException: " + ee + "\n--------------------------------");

                GC.Collect();
            }
        }



    }
}

