﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebServer.Models
{
    public class FilterScans
    {
        public bool mat_id_option { get; set; }
        public bool pir_num_option { get; set; }
        public int mat_id { get; set; }
        public int pir_num { get; set; }
    }
}
