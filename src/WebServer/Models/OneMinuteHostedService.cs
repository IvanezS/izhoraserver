﻿using Data.Repositories;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WebServer.Settings;

namespace WebServer.Models
{
    public class OneMinuteHostedService : IHostedService, IDisposable
    {
        private readonly ILogger<OneMinuteHostedService> _logger;

        private Timer _timer;

        private MainSettings _mainSettings { get; }

        private IServiceProvider _scopeFactory;
        private readonly ScanRepository _SCAN;



        public OneMinuteHostedService(ILogger<OneMinuteHostedService> logger, IOptions<MainSettings> mainsettings, IServiceProvider scopeFactory)
        {
            _logger = logger;
            _mainSettings = mainsettings.Value;
            _scopeFactory = scopeFactory;
            var scope = _scopeFactory.CreateScope();
            _SCAN = scope.ServiceProvider.GetRequiredService<ScanRepository>();
        }


        public Task StartAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("One Minute Hosted Service running.");

            _timer = new Timer(DoWork, null, TimeSpan.Zero, TimeSpan.FromSeconds(60));

            return Task.CompletedTask;
        }

        private void DoWork(object state)
        {
           _logger.LogInformation("One Minute Hosted Service is working:");
           _logger.LogInformation("Quantity of deleted CSV files: " + ProcessDirectory(_mainSettings.filelocation, _mainSettings.CountOfFilesToStay));
        }

        /// <summary>
        /// This function removes old CSV file in the target directory except count of new files to stay
        /// </summary>
        /// <param name="targetDirectory"></param>
        /// <param name="CountOfFilesToStay"></param>
        /// <returns></returns>
        public static int ProcessDirectory(string targetDirectory, int CountOfFilesToStay)
        {
            // Process the list of files found in the directory.
            string[] fileEntries = Directory.GetFiles(targetDirectory, "*.csv");
            int DeletedFilesCount = 0;
            if (fileEntries.Length > CountOfFilesToStay)
            {
                List<FileInfo> fil = new List<FileInfo>();
                foreach (string fileName in fileEntries)
                {
                    FileInfo fi = new FileInfo(fileName);
                    fil.Add(fi);
                }
                var df = fil.Except(fil.OrderByDescending(x => x.CreationTime).Take(CountOfFilesToStay)).ToList();
                DeletedFilesCount = df.Count;
                foreach (var f in df)
                {
                    File.Delete(f.FullName);
                }
            }
            return DeletedFilesCount; 

        }


    
        public Task StopAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("One Minute Hosted Service is stopping.");

            _timer?.Change(0, Timeout.Infinite);

            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _timer?.Dispose();
        }

    }
}

