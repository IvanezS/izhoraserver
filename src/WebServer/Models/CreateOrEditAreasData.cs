﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebServer.Models
{
    public class CreateOrEditAreasData
    {

        public int ID { get; set; }

        /// <summary>
        /// Ссылка на запись о расчёте
        /// </summary>
        public int CALKDATA_ID { get; set; }

        /// <summary>
        /// Mинимальная температура в области
        /// </summary>
        public float MINT { get; set; }

        /// <summary>
        /// Средняя температура в области
        /// </summary>
        public float AVGT { get; set; }

        /// <summary>
        /// Максимальная температура в области
        /// </summary>
        public float MAXT { get; set; }

        /// <summary>
        /// Координа по оси X верхнего левого угла области 
        /// </summary>
        public int CELL_UP_LEFT_X { get; set; }

        /// <summary>
        /// Координа по оси Y верхнего левого угла области 
        /// </summary>
        public int CELL_UP_LEFT_Y { get; set; }

        /// <summary>
        /// Ширина области по кол-ву ячеек
        /// </summary>
        public int AREA_WIDTH { get; set; }

        /// <summary>
        /// Высота области по кол-ву ячеек
        /// </summary>
        public int AREA_HEIGHT { get; set; }

        /// <summary>
        /// Номер строки области
        /// </summary>
        public int ROW_NUM { get; set; }

        /// <summary>
        /// Номер столбца области
        /// </summary>
        public int COL_NUM { get; set; }

        /// <summary>
        /// Тип записи: 0 или null - область, 1 - обрезанная область, 2 - оригинальный прямоугольник
        /// </summary>
        public byte? TYPE_AREA { get; set; }
    }
}
