﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebServer.Models
{
    public class CreateOrEditAreasSet
    {
        /// <summary>
        /// Кол-во строк температурной области
        /// </summary>
        public int Aria_Row_Quant { get; set; }

        /// <summary>
        /// Кол-во столбцов температурной области
        /// </summary>
        public int Aria_Col_Quant { get; set; }
    }
}
