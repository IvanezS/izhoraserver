﻿using Microsoft.Extensions.FileSystemGlobbing;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using PLC_S7_Exchange.Core;
using PLC_S7_Exchange.Data;
using PLC_S7_Exchange.Settings;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using WebServer.Settings;

namespace WebServer.Models
{
    public class PDIHostedService : IHostedService, IDisposable
    {
        private readonly ILogger<PDIHostedService> _logger;
        private Timer _timer;
        private readonly PiroDataBlock _pdb;
        private static List<PDI_command> PDI_List = new List<PDI_command>();
        private MainSettings _mainSettings { get; }
        private PlcSettings _plcSettings { get; }
        private int BadWSConnectionCounter;
        private readonly PlcS7Exchange _plc;
        private short executionCount = 0;
        private TcpClient client { get; set; }
        private NetworkStream stream { get; set; }
        private bool[] Send_flags { get; set; } = { false, false, false, false};
        private List<SendInfo> CPDIMessageList { get; set; }
        private readonly object _sync = new object();

        public PDIHostedService(ILogger<PDIHostedService> logger, PiroDataBlock pdb, IOptions<MainSettings> mainsettings, PlcS7Exchange plc, IOptions<PlcSettings> plcSettings)
        {
            _logger = logger;
            _pdb = pdb;
            _mainSettings = mainsettings.Value;
            _plcSettings = plcSettings.Value;
            _plc = plc;

            CPDIMessageList = new List<SendInfo>() { 
                new SendInfo("", (short)_mainSettings.PdiTryConts), 
                new SendInfo("", (short)_mainSettings.PdiTryConts), 
                new SendInfo("", (short)_mainSettings.PdiTryConts), 
                new SendInfo("", (short)_mainSettings.PdiTryConts)
            };

            PDI_List.Add(new PDI_command() { StationNumber = 1 });
            PDI_List.Add(new PDI_command() { StationNumber = 2 });
            PDI_List.Add(new PDI_command() { StationNumber = 3 });
            PDI_List.Add(new PDI_command() { StationNumber = 4 });

            CLST_Create();
        }


        public void CLST_Create()
        {
            try
            {
                client = new TcpClient(_mainSettings.LandScanServerIP, _mainSettings.LandScanServerPort);
                stream = client.GetStream();
                stream.ReadTimeout = (int)(_mainSettings.PdiExchangeTimeIntervalMs * 0.95);
                stream.WriteTimeout = (int)(_mainSettings.PdiExchangeTimeIntervalMs * 0.95);
                _logger.LogInformation("Client & Stream created");
            }
            catch (Exception e)
            {
                _logger.LogError("Couldn't get stream from Landscan TCP client: {0}", e.Message);
            }
        }


        public Task StartAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("PDI Hosted Service running.");

            _timer = new Timer(DoWork, null, TimeSpan.Zero,
                TimeSpan.FromMilliseconds(_mainSettings.PdiExchangeTimeIntervalMs));

            return Task.CompletedTask;
        }

        public PiroDataBlock GetPiroDataBlock()
        {
            return _pdb;
        }

        private void DoWork(object state)
        {

            executionCount++;

            try
            {
                List<string> message = new List<string>();

                for (int i = 0; i < PDI_List.Count; i++)
                {
                    PDI_List[i].StorageOn = _pdb.pirlist[i].storageON ? (short)1 : (short)0;
                    PDI_List[i].ProductDirection = _pdb.pirlist[i].speed_path >= 50 ? (short)1 : (short)0;
                    PDI_List[i].ProductName = _pdb.pirlist[i].piro_id.ToString()
                                      + "_" + _pdb.pirlist[i].mat_id.ToString()
                                      + "_" + _pdb.pirlist[i].path_id.ToString()
                                      //+ "_" + (_pdb.pirlist[i].speed_path >= 50 ? "F" : "B")
                                      + "_";
                    PDI_List[i].ProductString1 = _pdb.pirlist[i].path_id.ToString();
                    PDI_List[i].ProductString2 = "speed:" + _pdb.pirlist[i].speed_path;
                    PDI_List[i].IsAProduct = _pdb.pirlist[i].IsAProduct;

                    if (!PDI_List[i].IsAProduct.Equals(PDI_List[i].IsAProduct_old)/* || !PDI_List[i].ProductName.Equals(PDI_List[i].ProductName_old)*/)
                    {
                        _logger.LogInformation("executionCount = " + executionCount + ", i = " + i);
                        _logger.LogInformation("_pdb.pirlist[0].IsAProduct = " + _pdb.pirlist[0].IsAProduct + ", _pdb.pirlist[1].IsAProduct = " + _pdb.pirlist[1].IsAProduct);
                        //PDI_List[i].IsAProduct_old = PDI_List[i].IsAProduct;
                        //PDI_List[i].ProductName_old = PDI_List[i].ProductName;

                        string cpdi = (PDI_List[i].CPDI + " " +
                                        PDI_List[i].StationNumber + " " +
                                        PDI_List[i].ProductName + " " +
                                        PDI_List[i].ProductWidth + " " +
                                        PDI_List[i].ProductLength + " " +
                                        PDI_List[i].ProductTargetTemp + " " +
                                        PDI_List[i].ProductTargetTempMin + " " +
                                        PDI_List[i].ProductTargetTempMax + " " +
                                        PDI_List[i].ProductMaxDisplayTemp + " " +
                                        PDI_List[i].ProductMinDisplayTemp + " " +
                                        PDI_List[i].ProductThickness + " " +
                                        PDI_List[i].ProductDirection + " " +
                                        PDI_List[i].ProductType + " " +
                                        PDI_List[i].IsAProduct + " " +
                                        PDI_List[i].StorageOn + " " +
                                        PDI_List[i].ProductString1 + " " +
                                        PDI_List[i].ProductString2 + "\n");
                        //message.Add(cpdi);
                        CPDIMessageList[i].Command = cpdi;
                        CPDIMessageList[i].CurrentCount = 0;

                        Send_flags[i] = PDI_List[i].IsAProduct == 1;
                        if (!Send_flags[i]) _pdb.pirlist[i].t = 600;
                    }

                    PDI_List[i].IsAProduct_old = PDI_List[i].IsAProduct;
                    PDI_List[i].ProductName_old = PDI_List[i].ProductName;
                }

                if (Send_flags[0]) message.Add("CCZD 1\n");
                if (Send_flags[0]) message.Add("CWWE 1\n");
                if (Send_flags[1]) message.Add("CCZD 2\n");
                if (Send_flags[1]) message.Add("CWWE 2\n");



                if (CPDIMessageList[0].Command != "") CPDIMessageList[0].CurrentCount++;
                if (CPDIMessageList[1].Command != "") CPDIMessageList[1].CurrentCount++;
                if (CPDIMessageList[2].Command != "") CPDIMessageList[2].CurrentCount++;
                if (CPDIMessageList[3].Command != "") CPDIMessageList[3].CurrentCount++;
                CPDIMessageList[0].CheckNReset();
                CPDIMessageList[1].CheckNReset();
                CPDIMessageList[2].CheckNReset();
                CPDIMessageList[3].CheckNReset();

                if (stream != null)
                {
                    SendPDI(new List<string>() { CPDIMessageList[0].Command, CPDIMessageList[1].Command, CPDIMessageList[2].Command, CPDIMessageList[3].Command });
                    if (message.Count > 0)
                    {
                        SendPDI(message);
                        SendToPLC();
                    }
                }
                else
                {
                    CLST_Create();
                }

            }
            catch (Exception e)
            {
                _logger.LogError("PDI HostedService DoWork Exception: {0}", e.Message);
                BadWSConnectionCounter++;
                if (BadWSConnectionCounter > 1)
                {
                    BadWSConnectionCounter = 1;
                }
                TcpReconnect();
            }

        }

        private void TcpReconnect()
        {
            BadWSConnectionCounter++;
            if (BadWSConnectionCounter > 1)
            {
                BadWSConnectionCounter = 1;
            }
            _logger.LogInformation("===> New TCP client creating");
            try {
                NewClientOnError();
                NewStreamOnError();
            }
            catch
            {
                _logger.LogInformation("===> Can't New TCP client create");
            }
        }

        private void NewClientOnError()
        {

            try
            {
                TcpClient copy;

                lock (_sync)
                {


                    if (client == null)
                        return;

                    copy = client;
                    client = new TcpClient(_mainSettings.LandScanServerIP, _mainSettings.LandScanServerPort);
                }

                copy.Dispose();

            }
            catch (Exception ex)
            {
                _logger.LogError("===> Can't New TCP client create: {0}", ex);
            }
        }

        private void NewStreamOnError()
        {

            try
            {
                NetworkStream copy;

                lock (_sync)
                {
                    if (stream == null)
                        return;

                    copy = stream;
                    stream = client.GetStream();
                    stream.ReadTimeout = (int)(_mainSettings.PdiExchangeTimeIntervalMs * 0.95);
                    stream.WriteTimeout = (int)(_mainSettings.PdiExchangeTimeIntervalMs * 0.95);
                }

                copy.Dispose();

            }
            catch (Exception ex)
            {
                _logger.LogError("===> Can't New stream create: {0} ", ex);
            }
        }



        public Task StopAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("PDI Hosted Service is stopping.");

            if (client != null)
            {
                stream.Close();
                client.Close();
                stream.Dispose();
                client.Dispose();
            }
            _timer?.Change(TimeSpan.Zero, TimeSpan.FromSeconds(1));

            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _timer?.Dispose();
        }

        private void SendPDI(List<string> message)
        {
            try
            {

                int i = 0;
                foreach (string m in message)
                {
                    if (m != "")
                    {
                        // Translate the passed message into ASCII and store it as a Byte array.
                        Byte[] data = System.Text.Encoding.ASCII.GetBytes(m);

                        // Send the message to the connected TcpServer.
                        stream.Write(data, 0, data.Length);
                        _logger.LogWarning("Sent: {0}", string.Join(" ", m));

                        // Receive the TcpServer.response.

                        // Buffer to store the response bytes.
                        data = new Byte[256];

                        // String to store the response ASCII representation.
                        String responseData = String.Empty;

                        // Read the first batch of the TcpServer response bytes.
                        Int32 bytes = stream.Read(data, 0, data.Length);
                        responseData = System.Text.Encoding.ASCII.GetString(data, 0, bytes);
                        _logger.LogWarning("Received: {0}", string.Join(" ", responseData));

                        var respArray = responseData.Split(" ");
                        if (respArray[0] == "RPDI")// && respArray[1] == "0")
                        {
                            CPDIMessageList[i].Reset();
                        }
                        if (respArray[0] == "RWWE" && respArray[1] == "1")
                        {
                            _pdb.pirlist[0].w = float.Parse(respArray[7]);
                        }
                        if (respArray[0] == "RWWE" && respArray[1] == "2")
                        {
                            _pdb.pirlist[1].w = float.Parse(respArray[7]);
                        }
                        if (respArray[0] == "RCZD" && respArray[1] == "1")
                        {
                            _pdb.pirlist[0].t = short.Parse(respArray[3]);
                            if (respArray[2] != "0" && respArray[2] != "4") Send_flags[0] = false;
                        }
                        if (respArray[0] == "RCZD" && respArray[1] == "2")
                        {
                            _pdb.pirlist[1].t = short.Parse(respArray[3]);
                            if (respArray[2] != "0" && respArray[2] != "4") Send_flags[1] = false;
                        }
                        if (responseData == "") throw new Exception("No data received");
                        BadWSConnectionCounter = 0;
                    }
                    i++;
                }
            }
            catch (ArgumentNullException e)
            {
                _logger.LogError("ArgumentNullException: {0}", e.Message);
                throw e;
            }
            catch (SocketException e)
            {
                _logger.LogError("SocketException: {0}", e.Message);
                throw e;
            }
            catch (Exception e)
            {
                _logger.LogError("SendPDI Exception: {0}", e.Message);
                throw;
            }
        }


        public bool LandConnectOK()
        {
            return (BadWSConnectionCounter > 0) ? false : true;
        }

        private void SendToPLC()
        {
            try
            {
                byte[] WriteBytes = new byte[_plcSettings.BytesToSend];
                BitConverter.GetBytes(executionCount).Reverse().ToArray().CopyTo(WriteBytes, 0);
                BitConverter.GetBytes(executionCount).Reverse().ToArray().CopyTo(WriteBytes, 2);
                BitConverter.GetBytes(_pdb.pirlist[0].t).Reverse().ToArray().CopyTo(WriteBytes, 4);
                BitConverter.GetBytes(_pdb.pirlist[1].t).Reverse().ToArray().CopyTo(WriteBytes, 6);
                BitConverter.GetBytes(_pdb.pirlist[2].t).Reverse().ToArray().CopyTo(WriteBytes, 8);
                BitConverter.GetBytes(_pdb.pirlist[3].t).Reverse().ToArray().CopyTo(WriteBytes, 10);
                BitConverter.GetBytes(_pdb.pirlist[0].w).Reverse().ToArray().CopyTo(WriteBytes, 12);
                BitConverter.GetBytes(_pdb.pirlist[1].w).Reverse().ToArray().CopyTo(WriteBytes, 16);
                _plc.Write_DB(_plcSettings.DbNum, _plcSettings.SendOffsetAddr, WriteBytes);
            }
            catch (Exception e)
            {
                _logger.LogError("Got Problem to send data to PLC: {0}", e);
            }

        }

    }
}

