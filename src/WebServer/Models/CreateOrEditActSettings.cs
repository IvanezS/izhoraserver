﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebServer.Models
{
    public class CreateOrEditActSettings
    {
        /// <summary>
        /// Id
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Номер пирометра
        /// </summary>
        public int PIR_NUMBER { get; set; }

        /// <summary>
        /// Ссылка на настройки
        /// </summary>
        public int SETTINGS_ID { get; set; }
    }
}
