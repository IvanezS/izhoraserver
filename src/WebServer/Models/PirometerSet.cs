﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebServer.Models
{
    public class PirometerSet
    {
        /// <summary>
        /// Номер пирометра
        /// </summary>
        public int PirNumber { get; set; }

        /// <summary>
        /// Высота установки пирометра, мм
        /// </summary>
        public int AB { get; set; }

        /// <summary>
        /// Угол наклона пирометра, град
        /// </summary>
        public int Ang { get; set; }

        /// <summary>
        /// Расстояние до раската, мм
        /// </summary>
        public int H { get; set; }

        /// <summary>
        /// Общая ширина контроля FE, мм
        /// </summary>
        public int FE { get; set; }

        /// <summary>
        /// Ширина раската CD, мм
        /// </summary>
        public int CD { get; set; }

        /// <summary>
        /// Количество значений температуры по ширине TK
        /// </summary>
        public int TK { get; set; }


        /// <summary>
        /// Нижняя граница диапазона измеряемых температур
        /// </summary>
        public int Tmin { get; set; }

        /// <summary>
        /// Верхняя граница диапазона измеряемых температур
        /// </summary>
        public int Tmax { get; set; }


        public PirometerSet(int PirNum, int Height, int angle, int raskat, int T_min, int T_max)
        {
            PirNumber = PirNum;
            AB = Height;
            Ang = angle;
            CD = raskat;
            H = (int)(((double)AB) / Math.Cos(Ang * Math.PI / 180));
            FE = (int)(2 * ((double)H + 35) * Math.Tan(40 * Math.PI / 180));
            TK = CD / FE * 1000;
            Tmin = T_min;
            Tmax = T_max;
        }

        public float GetXCoord(int i)
        {
            var XCoord = ((float)H + 35) * Math.Tan((-40 + 0.08 * i) * Math.PI / 180) + ((float)H + 35) * Math.Tan(40 * Math.PI / 180);
            return (float)XCoord;
        }


    }
}
