﻿using System;

namespace WebServer.Models
{
    [Serializable]
    public class Vector3Ser
    {
        public float X { get; set; }
        public float Y { get; set; }
        public float Z { get; set; }
    }
}
