﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebServer.Models
{
    /// <summary>
    /// PDI message received over the TCPIP/RS232 link must be in ASCII and of the following structure.All fields must be included in any message sent.
    /// </summary>
    public  class PDI_command
    {

        public PDI_command() 
        {
            CPDI = "CPDI";
            StationNumber = 1;
            ProductName = "Landscan1";
            ProductName_old = "Landscan1";
            ProductWidth = 0;
            ProductLength = 0;
            ProductTargetTemp = 0;
            ProductTargetTempMin = 0;
            ProductTargetTempMax = 0;
            ProductMaxDisplayTemp = 0;
            ProductMinDisplayTemp = 0;
            ProductThickness = 0;
            ProductDirection = 1;
            ProductType = "TEST";
            IsAProduct = 0;
            IsAProduct_old = 0;
            StorageOn = 0;
            StorageOn_old = 0;
            ProductString1 = "NA";
            ProductString2 = "NA";

        }

        public  string CPDI { get; set; }
        public  int StationNumber { get; set; }
        public  string ProductName { get; set; }
        public  string ProductName_old { get; set; }
        /// <summary>
        /// For information only at present
        /// </summary>
        public  int ProductWidth { get; set; } 

        /// <summary>
        /// These parameters are an integer value of the current display distance units (mm, metres etc.)
        /// </summary>
        public  int ProductLength { get; set; }

        /// <summary>
        /// These parameters are the expected temperature limits of the product. They are always an   integer value of the current temperature scale(Celsius / Fahrenheit).
        /// </summary>
        public  short ProductTargetTemp { get; set; }

        /// <summary>
        /// These parameters are the expected temperature limits of the product. They are always an   integer value of the current temperature scale(Celsius / Fahrenheit).
        /// </summary>
        public  short ProductTargetTempMin { get; set; }

        /// <summary>
        /// These parameters are the expected temperature limits of the product. They are always an   integer value of the current temperature scale(Celsius / Fahrenheit).
        /// </summary>
        public  short ProductTargetTempMax { get; set; }

        /// <summary>
        /// These parameters are the temperature ranges of the current displays. They are an integer value  of the current temperature scale.These values must be within the temperature range of the configured LANDSCAN head.
        /// </summary>
        public  short ProductMaxDisplayTemp { get; set; }

        /// <summary>
        /// These parameters are the temperature ranges of the current displays. They are an integer value  of the current temperature scale.These values must be within the temperature range of the configured LANDSCAN head.
        /// </summary>
        public  short ProductMinDisplayTemp { get; set; }


        /// <summary>
        /// These parameters are an integer value of the current display distance units (mm, metres etc.)
        /// </summary>
        public  int ProductThickness { get; set; }

        /// <summary>
        /// This parameter is for information only and indicates the direction of movement of the product. 
        /// ProductDirection = 1 the product is moving in a forward direction.
        /// ProductDirection = 0 the product is moving in the reverse direction.
        /// </summary>
        public  short ProductDirection { get; set; }

        public  string ProductType { get; set; }

        /// <summary>
        /// This parameter is used to define the start and end of the product. The software will only use the IsAProduct message when the Product Determination is set to Communications Control.
        /// IsAProduct = 1 - Start Product.
        /// IsAProduct = 0 - End Product.
        /// </summary>
        public  int IsAProduct { get; set; }
        public  int IsAProduct_old { get; set; }

        /// <summary>
        /// Defines the status of the product storage; the software will only use the StorageOn message if the station storage is set to On Communications Message.
        /// ProductStorage = 1 - product storage is active.
        /// ProductStorage = 0 - product storage is disabled.
        /// </summary>
        public  short StorageOn { get; set; }
        public  short StorageOn_old { get; set; }

        public  string ProductString1 { get; set; }
        public  string ProductString2 { get; set; }
    }
}
