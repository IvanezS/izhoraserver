﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebServer.Models
{
    public class CreateOrEditSettings
    {
        public int ID { get; set; }

        /// <summary>
        /// Ссылка на настройки по размерам ячеек
        /// </summary>
        public int CELL_SETTINGS_ID { get; set; }


        /// <summary>
        /// ССылка на настройки по выделению области под ячейки
        /// </summary>
        public int CUT_SETTINGS_ID { get; set; }


        /// <summary>
        /// Ссылка на настройки по кол-ву областей
        /// </summary>
        public int AREAS_SETTINGS_ID { get; set; }

    }
}
