﻿using Data.Repositories;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Threading;
using System.Threading.Tasks;
using WebServer.Settings;

namespace WebServer.Models
{
    public class OneDayHostedService : IHostedService, IDisposable
    {
        private readonly ILogger<OneDayHostedService> _logger;

        private Timer _timer;

        private MainSettings _mainSettings { get; }

        private IServiceProvider _scopeFactory;
        private readonly ScanRepository _SCAN;



        public OneDayHostedService(ILogger<OneDayHostedService> logger, IOptions<MainSettings> mainsettings, IServiceProvider scopeFactory)
        {
            _logger = logger;
            _mainSettings = mainsettings.Value;
            _scopeFactory = scopeFactory;
            var scope = _scopeFactory.CreateScope();
            _SCAN = scope.ServiceProvider.GetRequiredService<ScanRepository>();
        }


        public Task StartAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("One Day Hosted Service running.");

            _timer = new Timer(DoWork, null, TimeSpan.Zero, TimeSpan.FromSeconds(43200));

            return Task.CompletedTask;
        }

        private void DoWork(object state)
        {
           _logger.LogInformation("One Day Hosted Service is working:");
            try
            {
                _logger.LogInformation("Quantity of rows in DB with deleted BLOB: " + ProcessDatabaseAsync(_mainSettings.CountOfDaysInDBToStay).Result);
            }
            catch 
            {
                _logger.LogError("Can't delete BLOB in DB "); 
            }
            try
            {
                _logger.LogInformation("Quantity of rows in DB with deleted pics: " + ProcessDatabasePicsAsync(_mainSettings.CountOfDaysPicsInDBToStay).Result);
            }
            catch
            {
                _logger.LogError("Can't delete pics in DB ");
            }
            try
            {
                _logger.LogInformation("Quantity of rows in SCAN DB deleted: " + ProcessDatabaseScansAsync(_mainSettings.CountOfDaysInDBScansToStay).Result);
            }
            catch 
            {
                _logger.LogError("Can't delete BLOB in DB "); 
            }
}



        public async Task<int> ProcessDatabaseAsync(int TimeToStayInDays)
        {
            var count = await _SCAN.DeleteOldVectorsAsync(TimeToStayInDays);
            return await Task.FromResult(count);
        }

        public async Task<int> ProcessDatabasePicsAsync(int TimeToStayInDays)
        {
            var count = await _SCAN.DeleteOldPicsAsync(TimeToStayInDays);
            return await Task.FromResult(count);
        }


        public async Task<int> ProcessDatabaseScansAsync(int TimeToStayInDays)
        {
            var count = await _SCAN.DeleteScansAsync(TimeToStayInDays);
            return await Task.FromResult(count);
        }

        public Task StopAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("One Minute Hosted Service is stopping.");

            _timer?.Change(0, Timeout.Infinite);

            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _timer?.Dispose();
        }

    }
}

