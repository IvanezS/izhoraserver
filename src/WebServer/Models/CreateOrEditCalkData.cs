﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebServer.Models
{
    public class CreateOrEditCalkData
    {

        public int Id { get; set; }

        /// <summary>
        /// Cсылка на конкретный скан
        /// </summary>
        public int Scan_Id { get; set; }

        /// <summary>
        /// Cсылка на настройки
        /// </summary>
        public int Settings_Id { get; set; }

        /// <summary>
        /// Ссылка на запись о ячейках
        /// </summary>
        public int CellData_Id { get; set; }

        /// <summary>
        /// Ссылка на запись о прямоугольнике заготовки
        /// </summary>
        public int Rect_data_id { get; set; }

        /// <summary>
        /// Время расчёта
        /// </summary>
        public DateTime Calk_dt { get; set; }

    }
}
