﻿namespace WebServer.Models
{
    /// <summary>
    /// Настройки создания среднего поля температур
    /// </summary>
    public class CreateAndUpdateAllSettings
    {
        /// <summary>
        /// номер пирометра
        /// </summary>
        public int pnum { get; set; }

        /// <summary>
        /// Кол-во разбиений температурного поля по оси X при подсчёте средних значений
        /// </summary>
        public int x_count { get; set; }

        /// <summary>
        /// Кол-во разбиений температурного поля по оси Y при подсчёте средних значений
        /// </summary>
        public int y_count { get; set; }

        /// <summary>
        /// Отступ от края снизу, мм
        /// </summary>
        public int CutBottom { get; set; }

        /// <summary>
        /// Отступ от края сверху, мм
        /// </summary>
        public int CutTop { get; set; }

        /// <summary>
        /// Отступ от края слева, мм
        /// </summary>
        public int CutLeft { get; set; }

        /// <summary>
        /// Отступ от края справа, мм
        /// </summary>
        public int CutRight { get; set; }

        /// <summary>
        /// Кол-во разбиений на области по стобцам
        /// </summary>
        public int Aria_Col_Quant { get; set; }

        /// <summary>
        /// Кол-во разбиений на области по строкам
        /// </summary>
        public int Aria_Row_Quant { get; set; }

    }
}
