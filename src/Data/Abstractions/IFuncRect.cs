﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Data.Abstractions
{
    public interface IFuncRect
    {
        /// <summary>
        /// Функция декодирует исходный массив байт в список 3D векторов (float, float, float)
        /// </summary>
        /// <param name="arr">Исходный массив байт</param>
        /// <returns></returns>
        public List<Vector3> DecodeByteArray444(byte[] arr);

        /// <summary>
        /// Функция кодирует исходный список 3D векторов в массив байт (float, float, float)
        /// </summary>
        /// <param name="m">Исходный список 3D векторов</param>
        /// <returns></returns>
        public byte[] CodeToByteArray444(List<Vector3> m);
    }
}
