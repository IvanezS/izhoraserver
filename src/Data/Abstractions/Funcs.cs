﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;


namespace Data.Abstractions
{
    public class Funcs : IFuncCells, IFuncRect
    {
        /// <summary>
        /// Функция кодирует исходный список 3D векторов в массив байт (float, float, float)
        /// </summary>
        /// <param name="m">Исходный список 3D векторов</param>
        /// <returns></returns>
        public byte[] CodeToByteArray444(List<Vector3> m)
        {
            IEnumerable<byte> ByteArray = new byte[0];
            foreach (var v in m)
            {
                ByteArray = ByteArray.Concat(BitConverter.GetBytes(v.X).ToArray());
                ByteArray = ByteArray.Concat(BitConverter.GetBytes(v.Y).ToArray());
                ByteArray = ByteArray.Concat(BitConverter.GetBytes(v.Z).ToArray());

            }
            return ByteArray.ToArray();
        }


        /// <summary>
        /// Функция декодирует исходный массив байт в список 3D векторов (float, float, float)
        /// </summary>
        /// <param name="arr">Исходный массив байт</param>
        /// <returns></returns>
        public List<Vector3> DecodeByteArray444(byte[] arr)
        {
            List<Vector3> v3 = new List<Vector3>();
            for (int i = 0; i < arr.Length / 12; i++)
            {
                v3.Add(new Vector3()
                {
                    X = BitConverter.ToSingle(arr, i * 12),
                    Y = BitConverter.ToSingle(arr, i * 12 + 4),
                    Z = BitConverter.ToSingle(arr, i * 12 + 8),
                });
            }
            return v3;
        }

        /// <summary>
        /// Функция кодирует исходный список 3D векторов в массив байт (short, short, float)
        /// </summary>
        /// <param name="m">Исходный список 3D векторов</param>
        /// <returns></returns>
        public byte[] CodeToByteArray224(List<Vector3> m)
        {
            
            IEnumerable<byte> ByteArray = new byte[0];
            foreach (var v in m)
            {
                ByteArray = ByteArray.Concat(BitConverter.GetBytes((short)v.X).ToArray());
                ByteArray = ByteArray.Concat(BitConverter.GetBytes((short)v.Y).ToArray());
                ByteArray = ByteArray.Concat(BitConverter.GetBytes(v.Z).ToArray());

            }
            return ByteArray.ToArray();
        }

        /// <summary>
        /// Функция кодирует исходный список 3D векторов в массив байт (short, short, float)
        /// </summary>
        /// <param name="m">Исходный список 3D векторов</param>
        /// <returns></returns>
        public byte[] CodeToByteArray224_sorted(float[,] m)
        {
            var v = m.Rank;
            IEnumerable<byte> ByteArray = new byte[0];
            for (short i = 0; i < m.GetUpperBound(0); i++)
            {
                for (short j = 0; j < m.GetUpperBound(1); j++)
                {
                    ByteArray = ByteArray.Concat(BitConverter.GetBytes(i).ToArray());
                    ByteArray = ByteArray.Concat(BitConverter.GetBytes(j).ToArray());
                    ByteArray = ByteArray.Concat(BitConverter.GetBytes(m[i,j]).ToArray());
                }
            }
            return ByteArray.ToArray();
        }


        /// <summary>
        /// Функция декодирует исходный массив байт в список 3D векторов (short, short, float)
        /// </summary>
        /// <param name="arr">Исходный массив байт</param>
        /// <returns></returns>
        public List<Vector3> DecodeByteArray224(byte[] arr)
        {
            List<Vector3> v3 = new List<Vector3>();
            for (int i = 0; i < arr.Length / 8; i++)
            {
                v3.Add(new Vector3()
                {
                    X = BitConverter.ToInt16(arr, i * 8),
                    Y = BitConverter.ToInt16(arr, i * 8 + 2),
                    Z = BitConverter.ToSingle(arr, i * 8 + 4),
                });
            }
            return v3;
        }
    }
}
