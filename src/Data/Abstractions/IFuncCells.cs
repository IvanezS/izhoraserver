﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Data.Abstractions
{
    public interface IFuncCells
    {
        /// <summary>
        /// Функция декодирует исходный массив байт в список 3D векторов (short, short, float)
        /// </summary>
        /// <param name="arr">Исходный массив байт</param>
        /// <returns></returns>
        public List<Vector3> DecodeByteArray224(byte[] arr);


        /// <summary>
        /// Функция кодирует исходный список 3D векторов в массив байт (short, short, float)
        /// </summary>
        /// <param name="m">Исходный список 3D векторов</param>
        /// <returns></returns>
        public byte[] CodeToByteArray224(List<Vector3> m);
    }
}
