﻿using Microsoft.EntityFrameworkCore;
using Data.Models;

namespace Data.Data
{
    public class OracleDBcontext : DbContext
    {
        
        public DbSet<Scan> Scan { get; set; }
        public DbSet<CellsData> CellsData { get; set; }
        public DbSet<AreasData> AreasScans { get; set; }
        public DbSet<ActSettings> ActSettings { get; set; }
        public DbSet<Setttings> Settings { get; set; }
        public DbSet<CutSettings> CutSettings { get; set; }
        public DbSet<AreasSettings> AreasSettings { get; set; }
        public DbSet<CellSettings> CellSettings { get; set; }
        public DbSet<CalkData> CalkData { get; set; }
        public DbSet<RectData> RectData { get; set; }

        public OracleDBcontext(DbContextOptions<OracleDBcontext> dbContextOptions) : base(dbContextOptions)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ActSettings>().ToTable("ACT_SETTINGS").HasComment("pyrometer assignment set settings table").HasKey(x => x.ID);
            modelBuilder.Entity<ActSettings>().HasOne(bc => bc.SETTTINGS).WithMany().HasForeignKey(x => x.SETTINGS_ID);
            modelBuilder.Entity<ActSettings>().Property(bc => bc.ID).IsRequired(true).HasComment("record index of current table");
            modelBuilder.Entity<ActSettings>().Property(bc => bc.SETTINGS_ID).IsRequired(true).HasComment("record index of table \"SETTINGS\"");
            modelBuilder.Entity<ActSettings>().Property(bc => bc.PIR_NUMBER).IsRequired(true).HasComment("pyrometer number");

            modelBuilder.Entity<Setttings>().ToTable("SETTINGS").HasComment("pyrometer set settings table").HasKey(x => x.ID);
            modelBuilder.Entity<Setttings>().HasOne(bc => bc.AREASETTINGS).WithMany().HasForeignKey(x => x.AREAS_SETTINGS_ID);
            modelBuilder.Entity<Setttings>().HasOne(bc => bc.CELLSETTINGS).WithMany().HasForeignKey(x => x.CELL_SETTINGS_ID);
            modelBuilder.Entity<Setttings>().HasOne(bc => bc.CUTSETTINGS).WithMany().HasForeignKey(x => x.CUT_SETTINGS_ID);
            modelBuilder.Entity<Setttings>().Property(bc => bc.ID).IsRequired(true).HasComment("record index of current table");
            modelBuilder.Entity<Setttings>().Property(bc => bc.AREAS_SETTINGS_ID).IsRequired(true).HasComment("record index of table \"AREAS_SETTINGS\"");
            modelBuilder.Entity<Setttings>().Property(bc => bc.CELL_SETTINGS_ID).IsRequired(true).HasComment("record index of table \"CELL_SETTINGS\"");
            modelBuilder.Entity<Setttings>().Property(bc => bc.CUT_SETTINGS_ID).IsRequired(true).HasComment("record index of table \"CUT_SETTINGS\"");

            modelBuilder.Entity<AreasSettings>().ToTable("AREAS_SETTINGS").HasComment("areas partitioning settings table").HasKey(x => x.ID);
            modelBuilder.Entity<AreasSettings>().Property(x => x.ID).IsRequired(true).HasComment("record index of current table");
            modelBuilder.Entity<AreasSettings>().Property(x => x.ARIA_COL_QUANT).IsRequired(true).HasComment("number of area partitions by columns");
            modelBuilder.Entity<AreasSettings>().Property(x => x.ARIA_ROW_QUANT).IsRequired(true).HasComment("number of area partitions by rows");

            modelBuilder.Entity<CellSettings>().ToTable("CELL_SETTINGS").HasComment("cell size settings table").HasKey(x => x.ID);
            modelBuilder.Entity<CellSettings>().Property(x => x.ID).IsRequired(true).HasComment("record index of current table");
            modelBuilder.Entity<CellSettings>().Property(x => x.CELL_HEIGHT).IsRequired(true).HasComment("cell height, mm");
            modelBuilder.Entity<CellSettings>().Property(x => x.CELL_WIDTH).IsRequired(true).HasComment("cell width, mm");

            modelBuilder.Entity<CutSettings>().ToTable("CUT_SETTINGS").HasComment("rectangle cropping settings table").HasKey(x => x.ID);
            modelBuilder.Entity<CutSettings>().Property(x => x.ID).IsRequired(true).HasComment("record index of current table");
            modelBuilder.Entity<CutSettings>().Property(x => x.CUT_BOTTOM).IsRequired(true).HasComment("rectangle cropping from the bottom, mm");
            modelBuilder.Entity<CutSettings>().Property(x => x.CUT_LEFT).IsRequired(true).HasComment("rectangle cropping from the left, mm");
            modelBuilder.Entity<CutSettings>().Property(x => x.CUT_RIGHT).IsRequired(true).HasComment("rectangle cropping from the right, mm");
            modelBuilder.Entity<CutSettings>().Property(x => x.CUT_TOP).IsRequired(true).HasComment("rectangle cropping from the top, mm");

            modelBuilder.Entity<Scan>().ToTable("SCANS").HasComment("table of performed scans").HasKey(x => x.ID);
            modelBuilder.Entity<Scan>().Property(x => x.ID).IsRequired(true).HasComment("record index of current table");
            modelBuilder.Entity<Scan>().Property(x => x.PIR_NUMBER).IsRequired(true).HasComment("pyrometer number");
            modelBuilder.Entity<Scan>().Property(x => x.MAT_ID).IsRequired(true).HasComment("ingot number");
            modelBuilder.Entity<Scan>().Property(x => x.DT_FIX).IsRequired(true).HasComment("time of writing a row to this table");
            modelBuilder.Entity<Scan>().Property(x => x.FORWARD_DIRECTION).IsRequired(true).HasComment("scan travel direction");
            modelBuilder.Entity<Scan>().Property(x => x.PASS_NO).IsRequired(true).HasComment("pass number of the ingot through the pyrometer");
            modelBuilder.Entity<Scan>().Property(x => x.PICT_DEFAULT).HasMaxLength(1000000).HasComment("jpeg of the original thermogram");
            modelBuilder.Entity<Scan>().Property(x => x.CSV_FILE_NAME).IsRequired(true).HasComment("path to CSV file on Integration server");
            modelBuilder.Entity<Scan>().Property(x => x.DT_CSV).IsRequired(true).HasComment("time of writing first row to CSV file");

            modelBuilder.Entity<CalkData>().ToTable("CALK_DATA").HasComment("scan calculation table").HasKey(x => x.ID);
            modelBuilder.Entity<CalkData>().Property(x => x.ID).IsRequired(true).HasComment("record index of current table");
            modelBuilder.Entity<CalkData>().Property(x => x.CELLDATA_ID).IsRequired(true).HasComment("record index of table \"CELLS_DATA\"");
            modelBuilder.Entity<CalkData>().Property(x => x.RECT_DATA_ID).IsRequired(true).HasComment("record index of table \"RECT_DATA\"");
            modelBuilder.Entity<CalkData>().Property(x => x.SETTINGS_ID).IsRequired(true).HasComment("record index of table \"SETTINGS\"");
            modelBuilder.Entity<CalkData>().Property(x => x.SCAN_ID).IsRequired(true).HasComment("record index of table \"SCANS\"");
            modelBuilder.Entity<CalkData>().Property(x => x.CALK_DT).IsRequired(true).HasComment("time of writing a row to this table");
            modelBuilder.Entity<CalkData>().HasOne(bc => bc.SCAN).WithMany().HasForeignKey(uc => uc.SCAN_ID);
            modelBuilder.Entity<CalkData>().HasOne(bc => bc.SETTTINGS).WithMany().HasForeignKey(uc => uc.SETTINGS_ID);
            modelBuilder.Entity<CalkData>().HasOne(bc => bc.CELLSDATA).WithMany().HasForeignKey(uc => uc.CELLDATA_ID);
            modelBuilder.Entity<CalkData>().HasOne(bc => bc.RECTDATA).WithMany().HasForeignKey(uc => uc.RECT_DATA_ID);

            modelBuilder.Entity<AreasData>().ToTable("AREAS_DATA").HasComment("areas partitioning data table").HasKey(x => x.ID);
            modelBuilder.Entity<AreasData>().Property(x => x.ID).IsRequired(true).HasComment("record index of current table");
            modelBuilder.Entity<AreasData>().Property(x => x.CALKDATA_ID).IsRequired(true).HasComment("record index of table \"CALK_DATA\"");
            modelBuilder.Entity<AreasData>().Property(x => x.AREA_HEIGHT).IsRequired(true).HasComment("area height");
            modelBuilder.Entity<AreasData>().Property(x => x.AREA_WIDTH).IsRequired(true).HasComment("area width");
            modelBuilder.Entity<AreasData>().Property(x => x.AVGT).IsRequired(true).HasComment("average temperature of the area");
            modelBuilder.Entity<AreasData>().Property(x => x.MAXT).IsRequired(true).HasComment("maximum temperature of the area");
            modelBuilder.Entity<AreasData>().Property(x => x.MINT).IsRequired(true).HasComment("minimum temperature of the area");
            modelBuilder.Entity<AreasData>().Property(x => x.ROW_NUM).IsRequired(true).HasComment("area row index");
            modelBuilder.Entity<AreasData>().Property(x => x.COL_NUM).IsRequired(true).HasComment("area column index");
            modelBuilder.Entity<AreasData>().Property(x => x.CELL_UP_LEFT_X).IsRequired(true).HasComment("upper-left corner of the area (X-axis)");
            modelBuilder.Entity<AreasData>().Property(x => x.CELL_UP_LEFT_Y).IsRequired(true).HasComment("upper-left corner of the area (Y-axis)");
            modelBuilder.Entity<AreasData>().Property(x => x.TYPE_AREA).IsRequired(true).HasComment("type of area");
            modelBuilder.Entity<AreasData>().HasOne(bc => bc.CALKDATA).WithMany().HasForeignKey(uc => uc.CALKDATA_ID);
            
            modelBuilder.Entity<CellsData>().ToTable("CELLS_DATA").HasComment("cells data table").HasKey(x => x.ID);
            modelBuilder.Entity<CellsData>().Property(x => x.ID).IsRequired(true).HasComment("record index of current table");
            modelBuilder.Entity<CellsData>().Property(x => x.ROW_QUANT).IsRequired(true).HasComment("total number of rows of cells");
            modelBuilder.Entity<CellsData>().Property(x => x.COL_QUANT).IsRequired(true).HasComment("total number of columns of cells");
            modelBuilder.Entity<CellsData>().Property(x => x.CELLS_VECTORS).HasMaxLength(1000000).HasComment("blob array of vector cells");
            modelBuilder.Entity<CellsData>().Property(x => x.PICT_CELLS).HasMaxLength(1000000).HasComment("jpeg of the cells calculated thermogram");

            modelBuilder.Entity<RectData>().ToTable("RECT_DATA").HasKey(x => x.ID);
            modelBuilder.Entity<RectData>().Property(x => x.ID).IsRequired(true).HasComment("record index of current table");
            modelBuilder.Entity<RectData>().Property(x => x.INGOT_HEIGHT).IsRequired(true).HasComment("ingot rectangle height, mm");
            modelBuilder.Entity<RectData>().Property(x => x.INGOT_WIDTH).IsRequired(true).HasComment("ingot rectangle width, mm");
            modelBuilder.Entity<RectData>().Property(x => x.RECT_VECTORS).HasMaxLength(1000000).HasComment("blob array of primary measurements in rectangle");
            modelBuilder.Entity<RectData>().Property(x => x.PICT_RECT).HasMaxLength(1000000).HasComment("jpeg of the original thermogram in gray tones with a highlighted red rectangle");


        }
    }
}
