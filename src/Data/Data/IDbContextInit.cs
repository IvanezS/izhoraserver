﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Data
{
    public interface IDbContextInit
    {
        public void DbInit();
    }
}
