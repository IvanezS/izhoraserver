﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;


namespace Data.Data
{
    public class DbContextFactory
    {
        public IConfiguration Configuration { get; }

        public DbContextFactory(IConfiguration configuration) => Configuration = configuration;

        public OracleDBcontext Create()
        {
            var options = new DbContextOptionsBuilder<OracleDBcontext>()
                .UseOracle(Configuration.GetConnectionString("OracleDBConnection"))
                .Options;

            return new OracleDBcontext(options);
        }
    }
}
