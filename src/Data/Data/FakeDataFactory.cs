﻿using Data.Models;
using System;
using System.Collections.Generic;


namespace Data.Data
{
    public static class FakeDataFactory
    {


        public static IEnumerable<Setttings> setttings => new List<Setttings>()
        {
            new Setttings()
            {
                ID =  1,
                AREAS_SETTINGS_ID = 1,
                CELL_SETTINGS_ID = 1,
                CUT_SETTINGS_ID = 1
            }
        };



        public static IEnumerable<CutSettings> cutSettings => new List<CutSettings>()
        {
            new CutSettings()
            {
                ID =  1,
                CUT_BOTTOM = 100,
                CUT_LEFT = 50,
                CUT_RIGHT = 50,
                CUT_TOP = 100
            }
        };

        public static IEnumerable<CellSettings> cellSettings => new List<CellSettings>()
        {
            new CellSettings()
            {
                ID =  1,
                CELL_HEIGHT = 50,
                CELL_WIDTH = 50
            }
        };

        public static IEnumerable<CalkData> calkDatas => new List<CalkData>()
        {
            new CalkData()
            {
                ID =  1,
                CELLDATA_ID = 1,
                CALK_DT = DateTime.Now,
                RECT_DATA_ID = 1,
                SCAN_ID = 1,
                SETTINGS_ID = 1
            }
        };

        public static IEnumerable<AreasSettings> areasSettings => new List<AreasSettings>()
        {
            new AreasSettings()
            {
                ID =  1,
                ARIA_COL_QUANT = 5,
                ARIA_ROW_QUANT = 3
            }
        };


        public static IEnumerable<ActSettings> PiroSet => new List<ActSettings>()
        {
            new ActSettings()
            {
                ID =  1,
                PIR_NUMBER = 1,
                SETTINGS_ID = 1
            },
            new ActSettings()
            {
                ID =  2,
                PIR_NUMBER = 2,
                SETTINGS_ID = 1
            },
            new ActSettings()
            {
                ID =  3,
                PIR_NUMBER = 3,
                SETTINGS_ID = 1
            },
            new ActSettings()
            {
                ID =  4,
                PIR_NUMBER = 4,
                SETTINGS_ID = 1
            }
        };

        public static IEnumerable<Scan> S => new List<Scan>()
        {
            new Scan()
            {
                ID =  1,
                FORWARD_DIRECTION = true,
                MAT_ID = 12,
                DT_FIX = DateTime.Now,
                PASS_NO = 1,
                PIR_NUMBER = 1,
                CSV_FILE_NAME = "CSV_file_path"
            }
        };

        public static IEnumerable<CellsData> CellsScans => new List<CellsData>()
        {
            new CellsData()
            {
                ID =  1,

                ROW_QUANT = 200,
                COL_QUANT = 500
                
            }
        };

        public static IEnumerable<AreasData> areasDatas => new List<AreasData>()
        {
            new AreasData()
            {
                ID =  1,
                AREA_HEIGHT = 3,
                AREA_WIDTH = 3,
                CALKDATA_ID = 1,
                AVGT = 4,
                MINT = 3,
                MAXT = 5,
                CELL_UP_LEFT_X = 0,
                CELL_UP_LEFT_Y = 0,
                COL_NUM = 1,
                ROW_NUM = 1

            }
        };

        public static IEnumerable<RectData> rectDatas => new List<RectData>()
        {
            new RectData()
            {
                ID =  1,
                INGOT_HEIGHT = 1000,
                INGOT_WIDTH = 666

            }
        };

    }
}