﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Data
{
    public class DbContextInit : IDbContextInit
    {
        private readonly OracleDBcontext _db;

        public DbContextInit(OracleDBcontext db)
        {
            _db = db;
        }

        public void DbInit()
        {
            _db.Database.EnsureDeleted();
            //_db.Database.EnsureCreated();

            
            _db.AddRange(FakeDataFactory.cellSettings);
            _db.AddRange(FakeDataFactory.areasSettings);
            _db.AddRange(FakeDataFactory.cutSettings);
            _db.AddRange(FakeDataFactory.setttings);
            _db.AddRange(FakeDataFactory.PiroSet);
            _db.AddRange(FakeDataFactory.S);
            _db.AddRange(FakeDataFactory.CellsScans);
            _db.AddRange(FakeDataFactory.areasDatas);
            _db.AddRange(FakeDataFactory.rectDatas);
            _db.AddRange(FakeDataFactory.calkDatas);

            try
            {
                _db.SaveChanges();
            }
            catch (Exception exception ){}
        }


    }
}
