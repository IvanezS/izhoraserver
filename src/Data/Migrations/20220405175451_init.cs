﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Data.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AREAS_SETTINGS",
                columns: table => new
                {
                    ID = table.Column<int>(type: "NUMBER(10)", nullable: false, comment: "record index of current table")
                        .Annotation("Oracle:Identity", "1, 1"),
                    ARIA_ROW_QUANT = table.Column<int>(type: "NUMBER(10)", nullable: false, comment: "number of area partitions by rows"),
                    ARIA_COL_QUANT = table.Column<int>(type: "NUMBER(10)", nullable: false, comment: "number of area partitions by columns")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AREAS_SETTINGS", x => x.ID);
                },
                comment: "areas partitioning settings table");

            migrationBuilder.CreateTable(
                name: "CELL_SETTINGS",
                columns: table => new
                {
                    ID = table.Column<int>(type: "NUMBER(10)", nullable: false, comment: "record index of current table")
                        .Annotation("Oracle:Identity", "1, 1"),
                    CELL_HEIGHT = table.Column<int>(type: "NUMBER(10)", nullable: false, comment: "cell height, mm"),
                    CELL_WIDTH = table.Column<int>(type: "NUMBER(10)", nullable: false, comment: "cell width, mm")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CELL_SETTINGS", x => x.ID);
                },
                comment: "cell size settings table");

            migrationBuilder.CreateTable(
                name: "CELLS_DATA",
                columns: table => new
                {
                    ID = table.Column<int>(type: "NUMBER(10)", nullable: false, comment: "record index of current table")
                        .Annotation("Oracle:Identity", "1, 1"),
                    PICT_CELLS = table.Column<byte[]>(type: "BLOB", maxLength: 1000000, nullable: true, comment: "jpeg of the cells calculated thermogram"),
                    CELLS_VECTORS = table.Column<byte[]>(type: "BLOB", maxLength: 1000000, nullable: true, comment: "blob array of vector cells"),
                    ROW_QUANT = table.Column<int>(type: "NUMBER(10)", nullable: false, comment: "total number of rows of cells"),
                    COL_QUANT = table.Column<int>(type: "NUMBER(10)", nullable: false, comment: "total number of columns of cells")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CELLS_DATA", x => x.ID);
                },
                comment: "cells data table");

            migrationBuilder.CreateTable(
                name: "CUT_SETTINGS",
                columns: table => new
                {
                    ID = table.Column<int>(type: "NUMBER(10)", nullable: false, comment: "record index of current table")
                        .Annotation("Oracle:Identity", "1, 1"),
                    CUT_LEFT = table.Column<int>(type: "NUMBER(10)", nullable: false, comment: "rectangle cropping from the left, mm"),
                    CUT_RIGHT = table.Column<int>(type: "NUMBER(10)", nullable: false, comment: "rectangle cropping from the right, mm"),
                    CUT_TOP = table.Column<int>(type: "NUMBER(10)", nullable: false, comment: "rectangle cropping from the top, mm"),
                    CUT_BOTTOM = table.Column<int>(type: "NUMBER(10)", nullable: false, comment: "rectangle cropping from the bottom, mm")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CUT_SETTINGS", x => x.ID);
                },
                comment: "rectangle cropping settings table");

            migrationBuilder.CreateTable(
                name: "RECT_DATA",
                columns: table => new
                {
                    ID = table.Column<int>(type: "NUMBER(10)", nullable: false, comment: "record index of current table")
                        .Annotation("Oracle:Identity", "1, 1"),
                    PICT_RECT = table.Column<byte[]>(type: "BLOB", maxLength: 1000000, nullable: true, comment: "jpeg of the original thermogram in gray tones with a highlighted red rectangle"),
                    RECT_VECTORS = table.Column<byte[]>(type: "BLOB", maxLength: 1000000, nullable: true, comment: "blob array of primary measurements in rectangle"),
                    INGOT_HEIGHT = table.Column<float>(type: "BINARY_FLOAT", nullable: false, comment: "ingot rectangle height, mm"),
                    INGOT_WIDTH = table.Column<float>(type: "BINARY_FLOAT", nullable: false, comment: "ingot rectangle width, mm")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RECT_DATA", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "SCANS",
                columns: table => new
                {
                    ID = table.Column<int>(type: "NUMBER(10)", nullable: false, comment: "record index of current table")
                        .Annotation("Oracle:Identity", "1, 1"),
                    PIR_NUMBER = table.Column<int>(type: "NUMBER(10)", nullable: false, comment: "pyrometer number"),
                    MAT_ID = table.Column<int>(type: "NUMBER(10)", nullable: false, comment: "ingot number"),
                    PASS_NO = table.Column<int>(type: "NUMBER(10)", nullable: false, comment: "pass number of the ingot through the pyrometer"),
                    FORWARD_DIRECTION = table.Column<bool>(type: "NUMBER(1)", nullable: false, comment: "scan travel direction"),
                    PICT_DEFAULT = table.Column<byte[]>(type: "BLOB", maxLength: 1000000, nullable: true, comment: "jpeg of the original thermogram"),
                    DT_FIX = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: false, comment: "time of writing a row to this table"),
                    CSV_FILE_NAME = table.Column<string>(type: "NVARCHAR2(2000)", nullable: false, comment: "path to CSV file on Integration server"),
                    DT_CSV = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: false, comment: "time of writing first row to CSV file")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SCANS", x => x.ID);
                },
                comment: "table of performed scans");

            migrationBuilder.CreateTable(
                name: "SETTINGS",
                columns: table => new
                {
                    ID = table.Column<int>(type: "NUMBER(10)", nullable: false, comment: "record index of current table")
                        .Annotation("Oracle:Identity", "1, 1"),
                    CELL_SETTINGS_ID = table.Column<int>(type: "NUMBER(10)", nullable: false, comment: "record index of table \"CELL_SETTINGS\""),
                    CUT_SETTINGS_ID = table.Column<int>(type: "NUMBER(10)", nullable: false, comment: "record index of table \"CUT_SETTINGS\""),
                    AREAS_SETTINGS_ID = table.Column<int>(type: "NUMBER(10)", nullable: false, comment: "record index of table \"AREAS_SETTINGS\"")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SETTINGS", x => x.ID);
                    table.ForeignKey(
                        name: "FK_SETTINGS_AREAS_SETTINGS_AREAS_SETTINGS_ID",
                        column: x => x.AREAS_SETTINGS_ID,
                        principalTable: "AREAS_SETTINGS",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SETTINGS_CELL_SETTINGS_CELL_SETTINGS_ID",
                        column: x => x.CELL_SETTINGS_ID,
                        principalTable: "CELL_SETTINGS",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SETTINGS_CUT_SETTINGS_CUT_SETTINGS_ID",
                        column: x => x.CUT_SETTINGS_ID,
                        principalTable: "CUT_SETTINGS",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                },
                comment: "pyrometer set settings table");

            migrationBuilder.CreateTable(
                name: "ACT_SETTINGS",
                columns: table => new
                {
                    ID = table.Column<int>(type: "NUMBER(10)", nullable: false, comment: "record index of current table")
                        .Annotation("Oracle:Identity", "1, 1"),
                    PIR_NUMBER = table.Column<int>(type: "NUMBER(10)", nullable: false, comment: "pyrometer number"),
                    SETTINGS_ID = table.Column<int>(type: "NUMBER(10)", nullable: false, comment: "record index of table \"SETTINGS\"")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ACT_SETTINGS", x => x.ID);
                    table.ForeignKey(
                        name: "FK_ACT_SETTINGS_SETTINGS_SETTINGS_ID",
                        column: x => x.SETTINGS_ID,
                        principalTable: "SETTINGS",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                },
                comment: "pyrometer assignment set settings table");

            migrationBuilder.CreateTable(
                name: "CALK_DATA",
                columns: table => new
                {
                    ID = table.Column<int>(type: "NUMBER(10)", nullable: false, comment: "record index of current table")
                        .Annotation("Oracle:Identity", "1, 1"),
                    SCAN_ID = table.Column<int>(type: "NUMBER(10)", nullable: false, comment: "record index of table \"SCANS\""),
                    SETTINGS_ID = table.Column<int>(type: "NUMBER(10)", nullable: false, comment: "record index of table \"SETTINGS\""),
                    CELLDATA_ID = table.Column<int>(type: "NUMBER(10)", nullable: false, comment: "record index of table \"CELLS_DATA\""),
                    RECT_DATA_ID = table.Column<int>(type: "NUMBER(10)", nullable: false, comment: "record index of table \"RECT_DATA\""),
                    CALK_DT = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: false, comment: "time of writing a row to this table")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CALK_DATA", x => x.ID);
                    table.ForeignKey(
                        name: "FK_CALK_DATA_CELLS_DATA_CELLDATA_ID",
                        column: x => x.CELLDATA_ID,
                        principalTable: "CELLS_DATA",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CALK_DATA_RECT_DATA_RECT_DATA_ID",
                        column: x => x.RECT_DATA_ID,
                        principalTable: "RECT_DATA",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CALK_DATA_SCANS_SCAN_ID",
                        column: x => x.SCAN_ID,
                        principalTable: "SCANS",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CALK_DATA_SETTINGS_SETTINGS_ID",
                        column: x => x.SETTINGS_ID,
                        principalTable: "SETTINGS",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                },
                comment: "scan calculation table");

            migrationBuilder.CreateTable(
                name: "AREAS_DATA",
                columns: table => new
                {
                    ID = table.Column<int>(type: "NUMBER(10)", nullable: false, comment: "record index of current table")
                        .Annotation("Oracle:Identity", "1, 1"),
                    CALKDATA_ID = table.Column<int>(type: "NUMBER(10)", nullable: false, comment: "record index of table \"CALK_DATA\""),
                    MINT = table.Column<float>(type: "BINARY_FLOAT", nullable: false, comment: "minimum temperature of the area"),
                    AVGT = table.Column<float>(type: "BINARY_FLOAT", nullable: false, comment: "average temperature of the area"),
                    MAXT = table.Column<float>(type: "BINARY_FLOAT", nullable: false, comment: "maximum temperature of the area"),
                    CELL_UP_LEFT_X = table.Column<int>(type: "NUMBER(10)", nullable: false, comment: "upper-left corner of the area (X-axis)"),
                    CELL_UP_LEFT_Y = table.Column<int>(type: "NUMBER(10)", nullable: false, comment: "upper-left corner of the area (Y-axis)"),
                    AREA_WIDTH = table.Column<int>(type: "NUMBER(10)", nullable: false, comment: "area width"),
                    AREA_HEIGHT = table.Column<int>(type: "NUMBER(10)", nullable: false, comment: "area height"),
                    ROW_NUM = table.Column<int>(type: "NUMBER(10)", nullable: false, comment: "area row index"),
                    COL_NUM = table.Column<int>(type: "NUMBER(10)", nullable: false, comment: "area column index"),
                    TYPE_AREA = table.Column<byte>(type: "NUMBER(3)", nullable: false, comment: "type of area")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AREAS_DATA", x => x.ID);
                    table.ForeignKey(
                        name: "FK_AREAS_DATA_CALK_DATA_CALKDATA_ID",
                        column: x => x.CALKDATA_ID,
                        principalTable: "CALK_DATA",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                },
                comment: "areas partitioning data table");

            migrationBuilder.CreateIndex(
                name: "IX_ACT_SETTINGS_SETTINGS_ID",
                table: "ACT_SETTINGS",
                column: "SETTINGS_ID");

            migrationBuilder.CreateIndex(
                name: "IX_AREAS_DATA_CALKDATA_ID",
                table: "AREAS_DATA",
                column: "CALKDATA_ID");

            migrationBuilder.CreateIndex(
                name: "IX_CALK_DATA_CELLDATA_ID",
                table: "CALK_DATA",
                column: "CELLDATA_ID");

            migrationBuilder.CreateIndex(
                name: "IX_CALK_DATA_RECT_DATA_ID",
                table: "CALK_DATA",
                column: "RECT_DATA_ID");

            migrationBuilder.CreateIndex(
                name: "IX_CALK_DATA_SCAN_ID",
                table: "CALK_DATA",
                column: "SCAN_ID");

            migrationBuilder.CreateIndex(
                name: "IX_CALK_DATA_SETTINGS_ID",
                table: "CALK_DATA",
                column: "SETTINGS_ID");

            migrationBuilder.CreateIndex(
                name: "IX_SETTINGS_AREAS_SETTINGS_ID",
                table: "SETTINGS",
                column: "AREAS_SETTINGS_ID");

            migrationBuilder.CreateIndex(
                name: "IX_SETTINGS_CELL_SETTINGS_ID",
                table: "SETTINGS",
                column: "CELL_SETTINGS_ID");

            migrationBuilder.CreateIndex(
                name: "IX_SETTINGS_CUT_SETTINGS_ID",
                table: "SETTINGS",
                column: "CUT_SETTINGS_ID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ACT_SETTINGS");

            migrationBuilder.DropTable(
                name: "AREAS_DATA");

            migrationBuilder.DropTable(
                name: "CALK_DATA");

            migrationBuilder.DropTable(
                name: "CELLS_DATA");

            migrationBuilder.DropTable(
                name: "RECT_DATA");

            migrationBuilder.DropTable(
                name: "SCANS");

            migrationBuilder.DropTable(
                name: "SETTINGS");

            migrationBuilder.DropTable(
                name: "AREAS_SETTINGS");

            migrationBuilder.DropTable(
                name: "CELL_SETTINGS");

            migrationBuilder.DropTable(
                name: "CUT_SETTINGS");
        }
    }
}
