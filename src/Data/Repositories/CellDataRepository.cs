﻿using Data.Data;
using Microsoft.EntityFrameworkCore;
using Data.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public class CellDataRepository : IRepository<CellsData>
    {
        //private readonly OracleDBcontext _db;
        private readonly DbContextFactory _dbContextFactory;

        public CellDataRepository(DbContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public async Task CreateAsync(CellsData item)
        {
            try
            {
                using (var _db = _dbContextFactory.Create())
                {
                    await _db.CellsData.AddAsync(item);
                    await _db.SaveChangesAsync();
                }
            }
            catch (Exception ee)
            {
                throw ee;
            }
        }

        public async Task CreateRangeAsync(List<CellsData> list)
        {
            try
            {
                using (var _db = _dbContextFactory.Create())
                {
                    await _db.CellsData.AddRangeAsync(list);
                    await _db.SaveChangesAsync();
                }
            }
            catch (Exception ee)
            {
                throw ee;
            }
        }



        public async Task DeleteAsync(int id)
        {
            using (var _db = _dbContextFactory.Create())
            {
                var xxx = await _db.ActSettings.AnyAsync(x => x.ID == id);
                if (!xxx) throw new ArgumentException($"Объекта с ID = {id} не существует");
                _db.CellsData.Remove(_db.CellsData.Find(id));
                await _db.SaveChangesAsync();
            }
        }

        public async Task<IEnumerable<CellsData>> GetAllAsync()
        {
            using (var _db = _dbContextFactory.Create())
            {
                return await _db.CellsData.ToListAsync();
            }
        }

        public async Task<CellsData> GetByIdAsync(int id)
        {
            using (var _db = _dbContextFactory.Create())
            {
                return await _db.CellsData.FirstOrDefaultAsync(x => x.ID == id);
            }
        }

        public async Task UpdateAsync(CellsData item)
        {
            using (var _db = _dbContextFactory.Create())
            {
                var xxx = await _db.CellsData.AnyAsync(x => x.ID == item.ID);
                if (!xxx) throw new ArgumentException($"Объекта с ID = {item.ID} не существует");
                _db.Entry(item).State = EntityState.Modified;
                await _db.SaveChangesAsync();
            }
        }
    }
}
