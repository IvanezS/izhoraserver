﻿using Data.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public interface IRepository<T> where T : BaseEntity
    {
        Task<IEnumerable<T>> GetAllAsync();

        Task<T> GetByIdAsync(int id);

        Task CreateAsync(T item);

        Task CreateRangeAsync(List<T> item);

        Task UpdateAsync(T item);

        Task DeleteAsync(int id);

    }
}
