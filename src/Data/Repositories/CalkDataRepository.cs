﻿using Data.Data;
using Microsoft.EntityFrameworkCore;
using Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public class CalkDataRepository : IRepository<CalkData>
    {
        //private readonly OracleDBcontext _db;
        private readonly DbContextFactory _dbContextFactory;
        public CalkDataRepository(DbContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public async Task CreateAsync(CalkData item)
        {
            try
            {
                using (var _db = _dbContextFactory.Create())
                {
                    await _db.CalkData.AddAsync(item);
                    await _db.SaveChangesAsync();
                }
            }
            catch (Exception ee)
            {
                throw ee;
            }
        }

        public async Task CreateRangeAsync(List<CalkData> list)
        {
            try
            {
                using (var _db = _dbContextFactory.Create())
                {
                    await _db.CalkData.AddRangeAsync(list);
                    await _db.SaveChangesAsync();
                }
            }
            catch (Exception ee)
            {
                throw ee;
            }
        }



        public async Task DeleteAsync(int id)
        {
            using (var _db = _dbContextFactory.Create())
            {
                var xxx = await _db.CalkData.AnyAsync(x => x.ID == id);
                if (!xxx) throw new ArgumentException($"Объекта с ID = {id} не существует");
                _db.CalkData.Remove(_db.CalkData.Find(id));
                await _db.SaveChangesAsync();
            }
        }

        public async Task<IEnumerable<CalkData>> GetAllAsync()
        {
            using (var _db = _dbContextFactory.Create())
            {
                return await _db.CalkData.ToListAsync();
            }
        }

        public async Task<IEnumerable<CalkData>> GetByScanIdAsync(int scan_id)
        {
            using (var _db = _dbContextFactory.Create())
            {
                return await _db.CalkData.Where(d => d.SCAN_ID == scan_id).OrderBy(e => e.CELLDATA_ID)
                .Include(sc => sc.SCAN)
                .Include(cd => cd.CELLSDATA)
                .Include(rd => rd.RECTDATA)
                .Include(e => e.SETTTINGS).ThenInclude(k => k.AREASETTINGS)
                .Include(e => e.SETTTINGS).ThenInclude(k => k.CELLSETTINGS)
                .Include(e => e.SETTTINGS).ThenInclude(k => k.CUTSETTINGS)
                .ToListAsync();
            }
        }

        public async Task<IEnumerable<CalkData>> GetByRectIdAsync(int Rect_data_id)
        {
            using (var _db = _dbContextFactory.Create())
            {
                return await _db.CalkData.Where(d => d.RECT_DATA_ID == Rect_data_id)
                //.Include(sc => sc.Scan)
                //.Include(cd => cd.CellsData)
                //.Include(rd => rd.RectData)
                //.Include(e => e.Settings)
                .ToListAsync();
            }
        }


        public async Task<IEnumerable<CalkData>> GetByCellIdAsync(int Cell_data_id)
        {
            using (var _db = _dbContextFactory.Create())
            {
                return await _db.CalkData.Where(d => d.CELLDATA_ID == Cell_data_id)
                //.Include(sc => sc.Scan)
                //.Include(cd => cd.CellsData)
                //.Include(rd => rd.RectData)
                //.Include(e => e.Settings)
                .ToListAsync();
            }
        }

        public async Task<IEnumerable<CalkData>> GetBySettings_IdAsync(int Settings_Id)
        {
            using (var _db = _dbContextFactory.Create())
            {
                return await _db.CalkData.Where(d => d.SETTINGS_ID == Settings_Id)
                //.Include(sc => sc.Scan)
                //.Include(cd => cd.CellsData)
                //.Include(rd => rd.RectData)
                //.Include(e => e.Settings)
                .ToListAsync();
            }
        }

        public async Task<CalkData> GetByIdAsync(int id)
        {
            using (var _db = _dbContextFactory.Create())
            {
                return await _db.CalkData.Include(sc => sc.SCAN).Include(cd => cd.CELLSDATA).Include(rd => rd.RECTDATA).Include(e => e.SETTTINGS).FirstOrDefaultAsync(x => x.ID == id);
            }
        }

        public async Task UpdateAsync(CalkData item)
        {
            using (var _db = _dbContextFactory.Create())
            {
                var xxx = await _db.CalkData.AnyAsync(x => x.ID == item.ID);
                if (!xxx) throw new ArgumentException($"Объекта с ID = {item.ID} не существует");
                _db.Entry(item).State = EntityState.Modified;
                await _db.SaveChangesAsync();
            }
        }
    }
}
