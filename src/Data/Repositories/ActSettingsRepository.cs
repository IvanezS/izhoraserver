﻿using Data.Data;
using Microsoft.EntityFrameworkCore;
using Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public class ActSettingsRepository : IRepository<ActSettings>
    {
        //private readonly OracleDBcontext _db;
        private readonly DbContextFactory _dbContextFactory;

        public ActSettingsRepository(DbContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public async Task CreateAsync(ActSettings item)
        {
            try
            {
                using (var _db = _dbContextFactory.Create())
                {
                    await _db.ActSettings.AddAsync(item);
                    await _db.SaveChangesAsync();
                }
            }
            catch (Exception ee)
            {
                throw ee;
            }
        }

        public async Task CreateRangeAsync(List<ActSettings> item)
        {
            try
            {
                using (var _db = _dbContextFactory.Create())
                {
                    await _db.ActSettings.AddRangeAsync(item);
                    await _db.SaveChangesAsync();
                }
            }
            catch (Exception ee)
            {
                throw ee;
            }
        }

        public async Task DeleteAsync(int id)
        {
            using (var _db = _dbContextFactory.Create())
            {
                var xxx = await _db.ActSettings.AnyAsync(x => x.ID == id);
                if (!xxx) throw new ArgumentException($"Объекта с ID = {id} не существует");
                _db.ActSettings.Remove(_db.ActSettings.Find(id));
                await _db.SaveChangesAsync();
            }
        }

        public async Task<IEnumerable<ActSettings>> GetAllAsync()
        {
            using (var _db = _dbContextFactory.Create())
            {
                return await _db.ActSettings
                .Include(x => x.SETTTINGS).ThenInclude(c => c.AREASETTINGS)
                .Include(y => y.SETTTINGS).ThenInclude(e => e.CELLSETTINGS)
                .Include(z => z.SETTTINGS).ThenInclude(w => w.CUTSETTINGS)
                .ToListAsync();
            }
        }

        public async Task<ActSettings> GetByIdAsync(int id)
        {
            using (var _db = _dbContextFactory.Create())
            {
                return await _db.ActSettings
                .Include(x => x.SETTTINGS).ThenInclude(c => c.AREASETTINGS)
                .Include(y => y.SETTTINGS).ThenInclude(e => e.CELLSETTINGS)
                .Include(z => z.SETTTINGS).ThenInclude(w => w.CUTSETTINGS)
                .FirstOrDefaultAsync(x => x.ID == id);
            }
        }

        public async Task<ActSettings> GetByCalkAsync(int calkdata_id)
        {
            using (var _db = _dbContextFactory.Create())
            {
                return await _db.ActSettings
                .Where(s => s.SETTINGS_ID == _db.CalkData.Where(e => e.ID == calkdata_id).FirstOrDefault().SETTINGS_ID)
                .Include(x => x.SETTTINGS).ThenInclude(c => c.AREASETTINGS)
                .Include(y => y.SETTTINGS).ThenInclude(e => e.CELLSETTINGS)
                .Include(z => z.SETTTINGS).ThenInclude(w => w.CUTSETTINGS)
                .FirstOrDefaultAsync();
            }
        }

        public async Task UpdateAsync(ActSettings item)
        {
            using (var _db = _dbContextFactory.Create())
            {
                var xxx = await _db.ActSettings.AnyAsync(x => x.ID == item.ID);
                if (!xxx) throw new ArgumentException($"Объекта с ID = {item.ID} не существует");
                _db.Entry(item).State = EntityState.Modified;
                await _db.SaveChangesAsync();
            }
        }
    }
}
