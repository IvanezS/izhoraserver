﻿using Data.Data;
using Microsoft.EntityFrameworkCore;
using Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public class AreasDataRepository : IRepository<AreasData>
    {
        //private readonly OracleDBcontext _db;
        private readonly DbContextFactory _dbContextFactory;

        public AreasDataRepository(DbContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public async Task CreateAsync(AreasData item)
        {
            try
            {
                using (var _db = _dbContextFactory.Create())
                {
                    await _db.AreasScans.AddAsync(item);
                    await _db.SaveChangesAsync();
                }
            }
            catch (Exception ee)
            {
                throw ee;
            }
        }

        public async Task CreateRangeAsync(List<AreasData> item)
        {
            try
            {
                using (var _db = _dbContextFactory.Create())
                {
                    await _db.AreasScans.AddRangeAsync(item);
                    await _db.SaveChangesAsync();
                }
            }
            catch (Exception ee)
            {
                throw ee;
            }
        }

        public async Task DeleteAsync(int id)
        {
            using (var _db = _dbContextFactory.Create())
            {
                var xxx = await _db.AreasScans.AnyAsync(x => x.ID == id);
                if (!xxx) throw new ArgumentException($"Объекта с ID = {id} не существует");
                _db.AreasScans.Remove(_db.AreasScans.Find(id));
                await _db.SaveChangesAsync();
            }
        }

        public async Task<IEnumerable<AreasData>> GetAllAsync()
        {
            using (var _db = _dbContextFactory.Create())
            {
                return await _db.AreasScans.ToListAsync();
            }

        }


        public async Task<IEnumerable<AreasData>> GetWithCalkAsync(int CalkData_Id)
        {
            using (var _db = _dbContextFactory.Create())
            {
                return await _db.AreasScans.Where(w => w.CALKDATA_ID == CalkData_Id).ToListAsync();
            }
        }

        public async Task<AreasData> GetByIdAsync(int id)
        {
            using (var _db = _dbContextFactory.Create())
            {
                return await _db.AreasScans.FirstOrDefaultAsync(x => x.ID == id);
            }
        }

        public async Task UpdateAsync(AreasData item)
        {
            using (var _db = _dbContextFactory.Create())
            {
                var xxx = await _db.AreasScans.AnyAsync(x => x.ID == item.ID);
                if (!xxx) throw new ArgumentException($"Объекта с ID = {item.ID} не существует");
                _db.Entry(item).State = EntityState.Modified;
                await _db.SaveChangesAsync();
            }
        }
    }
}
