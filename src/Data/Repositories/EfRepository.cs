﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Data.Models;
using Data.Data;

namespace Data.Repositories
{
    public class EfRepository<T> : IRepository<T> where T: BaseEntity
    {
        //private readonly OracleDBcontext _db;
        private readonly DbContextFactory _dbContextFactory;
        public EfRepository(DbContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public async Task CreateAsync(T item)
        {
            using (var _db = _dbContextFactory.Create())
            {
                var xxx = await _db.Set<T>().AnyAsync(x => x.ID == item.ID);
                if (xxx) throw new ArgumentException($"Объект с ID = {item.ID} уже существует");
                await _db.Set<T>().AddAsync(item);
                await _db.SaveChangesAsync();
            }
        }

        public async Task CreateRangeAsync(List<T> item)
        {
            try
            {
                using (var _db = _dbContextFactory.Create())
                {
                    await _db.Set<T>().AddRangeAsync(item);
                    await _db.SaveChangesAsync();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task DeleteAsync(int id)
        {
            using (var _db = _dbContextFactory.Create())
            {
                var xxx = await _db.Set<T>().AnyAsync(x => x.ID == id);
                if (!xxx) throw new ArgumentException($"Объекта с ID = {id} не существует");
                _db.Set<T>().Remove(_db.Set<T>().Find(id));
                await _db.SaveChangesAsync();
            }
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            using (var _db = _dbContextFactory.Create())
            {
                return await _db.Set<T>().OrderByDescending(x => x.ID).Take(1000).ToListAsync();
            }
        }

        public async Task<T> GetByIdAsync(int id)
        {
            using (var _db = _dbContextFactory.Create())
            {
                var xxx = await _db.Set<T>().FirstOrDefaultAsync(x => x.ID == id);
                return xxx;
            }
        }

        public async Task UpdateAsync(T item)
        {
            using (var _db = _dbContextFactory.Create())
            {
                var xxx = await _db.Set<T>().AnyAsync(x => x.ID == item.ID);
                if (!xxx) throw new ArgumentException($"Объекта с ID = {item.ID} не существует");
                _db.Entry(item).State = EntityState.Modified;
                await _db.SaveChangesAsync();
            }
        }


    }
}