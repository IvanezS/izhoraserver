﻿using Data.Data;
using Microsoft.EntityFrameworkCore;
using Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public class ScanRepository : IRepository<Scan>
    {
        //private readonly OracleDBcontext _db;
        private readonly DbContextFactory _dbContextFactory;

        public ScanRepository(DbContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public async Task CreateAsync(Scan item)
        {
            try
            {
                using (var _db = _dbContextFactory.Create())
                {
                    await _db.Scan.AddAsync(item);
                    await _db.SaveChangesAsync();
                }

            }
            catch (Exception ee)
            {
                throw ee;
            }
        }

        public async Task CreateRangeAsync(List<Scan> list)
        {
            try
            {
                using (var _db = _dbContextFactory.Create())
                {
                    await _db.Scan.AddRangeAsync(list);
                    await _db.SaveChangesAsync();
                }

            }
            catch (Exception ee)
            {
                throw ee;
            }
        }



        public async Task DeleteAsync(int id)
        {
            using (var _db = _dbContextFactory.Create())
            {
                var xxx = await _db.Scan.AnyAsync(x => x.ID == id);
                if (!xxx) throw new ArgumentException($"Объекта с ID = {id} не существует");
                _db.Scan.Remove(_db.Scan.Find(id));
                await _db.SaveChangesAsync();
            }
        }

        public async Task<IEnumerable<Scan>> GetAllAsync()
        {
            using (var _db = _dbContextFactory.Create())
            {
                return await _db.Scan.OrderByDescending(x => x.ID).Take(1000).ToListAsync();
            }
        }

        public async Task<IEnumerable<Scan>> GetFilteredAsync(bool mat_id_option, bool pir_num_option, int mat_id, int pir_num)
        {
            using (var _db = _dbContextFactory.Create())
            {
                var fscans = await _db.Scan.OrderByDescending(x => x.ID).Take(10000).ToListAsync();
                var matidfiltered = mat_id_option ? fscans.Where(x => x.MAT_ID == mat_id) : fscans;
                var pirnumfiltered = pir_num_option ? matidfiltered.Where(x => x.PIR_NUMBER == pir_num) : matidfiltered;
                return pirnumfiltered;
            }
        }


        public async Task<Scan> GetByIdAsync(int id)
        {
            using (var _db = _dbContextFactory.Create())
            {
                return await _db.Scan.FirstOrDefaultAsync(x => x.ID == id);
            }
        }

        public async Task UpdateAsync(Scan item)
        {
            using (var _db = _dbContextFactory.Create())
            {
                var xxx = await _db.Scan.AnyAsync(x => x.ID == item.ID);
                if (!xxx) throw new ArgumentException($"Объекта с ID = {item.ID} не существует");
                _db.Entry(item).State = EntityState.Modified;
                await _db.SaveChangesAsync();
            }
        }

        public async Task<int> DeleteScansAsync(int TimeToStayInDays)
        {
            int count = 0;
            using (var _db = _dbContextFactory.Create())
            {
                var oldScans = _db.Scan.
                    Where(x => x.DT_FIX < (DateTime.Now - TimeSpan.FromDays(TimeToStayInDays)))
                    .ToList();
                count = oldScans.Count;
                foreach (var os in oldScans)
                {
                    _db.Entry(os).State = EntityState.Deleted;
                }
                await _db.SaveChangesAsync();
            }
            return await Task.FromResult(count);
        }

        public async Task<int> DeleteOldVectorsAsync(int TimeToStayInDays)
        {
            using (var _db = _dbContextFactory.Create())
            {
                int count = 0;
                var cds = _db.CalkData.
                    Where(x => x.CALK_DT < (DateTime.Now - TimeSpan.FromDays(TimeToStayInDays)) && x.CALK_DT > (DateTime.Now - TimeSpan.FromDays(TimeToStayInDays + 1)))
                    .ToList();
                if (cds.Count > 0)
                {
                    foreach (var c in cds)
                    {
                        var rect = await _db.RectData.Where(x => x.ID == c.RECT_DATA_ID && x.RECT_VECTORS != null).FirstOrDefaultAsync();
                        if (rect != null)
                        {
                            if (rect.RECT_VECTORS != null)
                            {
                                rect.RECT_VECTORS = null;
                                _db.Entry(rect).State = EntityState.Modified;

                                count++;
                            }
                        }

                    }
                    await _db.SaveChangesAsync();
                }

                return await Task.FromResult(count);
            }
        }

        public async Task<int> DeleteOldPicsAsync(int TimeToStayInDays)
        {
            using (var _db = _dbContextFactory.Create())
            {
                int count = 0;
                var cds = _db.CalkData.
                    Where(x => x.CALK_DT < (DateTime.Now - TimeSpan.FromDays(TimeToStayInDays)) && x.CALK_DT > (DateTime.Now - TimeSpan.FromDays(TimeToStayInDays + 1)))
                    .ToList();
                if (cds.Count > 0)
                {
                    foreach (var c in cds)
                    {
                        var rect = await _db.RectData.Where(x => x.ID == c.RECT_DATA_ID && x.PICT_RECT != null).FirstOrDefaultAsync();
                        if (rect != null)
                        {
                            if (rect.PICT_RECT != null)
                            {
                                rect.PICT_RECT = null;
                                _db.Entry(rect).State = EntityState.Modified;

                                count++;
                            }
                        }

                    }
                    await _db.SaveChangesAsync();
                }

                return await Task.FromResult(count);
            }
        }

    }
}
