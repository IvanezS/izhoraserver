﻿using Data.Data;
using Microsoft.EntityFrameworkCore;
using Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public class SettingsRepository : IRepository<Setttings>
    {
        //private readonly OracleDBcontext _db;
        private readonly DbContextFactory _dbContextFactory;

        public SettingsRepository(DbContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public async Task CreateAsync(Setttings item)
        {
            using (var _db = _dbContextFactory.Create())
            {
                var list = await _db.Settings.ToListAsync();
                bool AreasSettings = _db.AreasSettings.Any(x => x.ID == item.AREAS_SETTINGS_ID);
                bool CutSettings = _db.CutSettings.Any(x => x.ID == item.CUT_SETTINGS_ID);
                bool CellSettings = _db.CellSettings.Any(x => x.ID == item.CELL_SETTINGS_ID);
                var IsExist = list.Exists(x => x.AREAS_SETTINGS_ID == item.AREAS_SETTINGS_ID && x.CELL_SETTINGS_ID == item.CELL_SETTINGS_ID && x.CUT_SETTINGS_ID == item.CUT_SETTINGS_ID);

                if (!(AreasSettings && CutSettings && CellSettings)) throw new ArgumentException("Некорректные данные");

                try
                {
                    if (!IsExist)
                    {
                        await _db.Settings.AddAsync(item);
                        await _db.SaveChangesAsync();
                    }
                    else
                    {
                        throw new ArgumentException("Уже существует такая запись");
                    }
                }
                catch (Exception ee)
                {
                    throw ee;
                }
            }
        }

        public async Task CreateRangeAsync(List<Setttings> item)
        {
            try
            {
                using (var _db = _dbContextFactory.Create())
                {
                    await _db.Settings.AddRangeAsync(item);
                    await _db.SaveChangesAsync();
                }
            }
            catch (Exception ee)
            {
                throw ee;
            }
        }

        public async Task DeleteAsync(int id)
        {
            using (var _db = _dbContextFactory.Create())
            {
                var xxx = await _db.Settings.AnyAsync(x => x.ID == id);
                if (!xxx) throw new ArgumentException($"Объекта с ID = {id} не существует");
                _db.Settings.Remove(_db.Settings.Find(id));
                await _db.SaveChangesAsync();
            }
        }

        public async Task<IEnumerable<Setttings>> GetAllAsync()
        {
            using (var _db = _dbContextFactory.Create())
            {
                return await _db.Settings
                .Include(c => c.AREASETTINGS)
                .Include(e => e.CELLSETTINGS)
                .Include(w => w.CUTSETTINGS)
                .ToListAsync();
            }
        }

        public async Task<Setttings> GetByIdAsync(int id)
        {
            using (var _db = _dbContextFactory.Create())
            {
                return await _db.Settings
                .Include(c => c.AREASETTINGS)
                .Include(e => e.CELLSETTINGS)
                .Include(w => w.CUTSETTINGS)
                .FirstOrDefaultAsync(x => x.ID == id);
            }
        }



        public async Task UpdateAsync(Setttings item)
        {
            using (var _db = _dbContextFactory.Create())
            {
                var xxx = await _db.Settings.AnyAsync(x => x.ID == item.ID);
                if (!xxx) throw new ArgumentException($"Объекта с ID = {item.ID} не существует");
                _db.Entry(item).State = EntityState.Modified;
                await _db.SaveChangesAsync();
            }
        }
    }
}
