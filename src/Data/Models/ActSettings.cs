﻿namespace Data.Models
{
    /// <summary>
    /// Настройки пирометров
    /// </summary>
    public class ActSettings : BaseEntity
    {
        /// <summary>
        /// Номер пирометра
        /// </summary>
        public int PIR_NUMBER { get; set; }

        /// <summary>
        /// Ссылка на настройки
        /// </summary>
        public int SETTINGS_ID { get; set; }

        public virtual Setttings SETTTINGS { get; set; }
    }
}
