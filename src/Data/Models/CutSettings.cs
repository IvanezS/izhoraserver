﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Models
{
    /// <summary>
    /// Настройки геометрии термограммы ячеек
    /// </summary>
    public class CutSettings : BaseEntity
    {
        /// <summary>
        /// Отсечение слева, мм
        /// </summary>
        public int CUT_LEFT { get; set; }

        /// <summary>
        /// Отсечение справа, мм
        /// </summary>
        public int CUT_RIGHT { get; set; }

        /// <summary>
        /// Отсечение сверху, мм
        /// </summary>
        public int CUT_TOP { get; set; }

        /// <summary>
        /// Отсечение снизу, мм
        /// </summary>
        public int CUT_BOTTOM { get; set; }
    }
}
