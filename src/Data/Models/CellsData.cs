﻿namespace Data.Models
{
    /// <summary>
    /// Термограмма из ячеек
    /// </summary>
    public class CellsData : BaseEntity
    {
        /// <summary>
        /// Изображение заготовки с ячейками
        /// </summary>
        public byte[] PICT_CELLS { get; set; }

        /// <summary>
        /// Список векторов ячеек
        /// </summary>
        public byte[] CELLS_VECTORS { get; set; }

        /// <summary>
        /// Кол-во ячеек по ширине
        /// </summary>
        public int ROW_QUANT { get; set; }

        /// <summary>
        /// Кол-во ячеек по высоте
        /// </summary>
        public int COL_QUANT { get; set; }
    }
}
