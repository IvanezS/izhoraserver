﻿namespace Data.Models
{
    /// <summary>
    /// Настройки ячеек
    /// </summary>
    public class CellSettings : BaseEntity
    {
        /// <summary>
        /// Высота ячейки, мм
        /// </summary>
        public int CELL_HEIGHT { get; set; }

        /// <summary>
        /// Ширина ячейки, мм
        /// </summary>
        public int CELL_WIDTH { get; set; }
    }
}
