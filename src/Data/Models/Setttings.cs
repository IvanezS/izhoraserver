﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Models
{
    /// <summary>
    /// Общие настройки
    /// </summary>
    public class Setttings : BaseEntity
    {
        /// <summary>
        /// Ссылка на настройки по размерам ячеек
        /// </summary>
        public int CELL_SETTINGS_ID { get; set; }
        public virtual CellSettings CELLSETTINGS { get; set; }

        /// <summary>
        /// ССылка на настройки по выделению области под ячейки
        /// </summary>
        public int CUT_SETTINGS_ID { get; set; }
        public virtual CutSettings CUTSETTINGS { get; set; }

        /// <summary>
        /// Ссылка на настройки по кол-ву областей
        /// </summary>
        public int AREAS_SETTINGS_ID { get; set; }
        public virtual AreasSettings AREASETTINGS { get; set; }
    }
}
