﻿namespace Data.Models
{
    /// <summary>
    /// Настройка разбиения по областям
    /// </summary>
    public class AreasSettings : BaseEntity
    {
        /// <summary>
        /// Кол-во строк температурной области
        /// </summary>
        public int ARIA_ROW_QUANT { get; set; }

        /// <summary>
        /// Кол-во столбцов температурной области
        /// </summary>
        public int ARIA_COL_QUANT { get; set; }

    }
}
