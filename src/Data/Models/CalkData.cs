﻿using System;

namespace Data.Models
{
    /// <summary>
    /// Основная расчётная таблица
    /// </summary>
    public class CalkData : BaseEntity
    {
        /// <summary>
        /// Cсылка на конкретный скан
        /// </summary>
        public int SCAN_ID { get; set; }
        public virtual Scan SCAN { get; set; }

        /// <summary>
        /// Cсылка на настройки
        /// </summary>
        public int SETTINGS_ID { get; set; }
        public virtual Setttings SETTTINGS { get; set; }


        /// <summary>
        /// Ссылка на запись о ячейках
        /// </summary>
        public int CELLDATA_ID { get; set; }
        public virtual CellsData CELLSDATA { get; set; }

        /// <summary>
        /// Ссылка на запись о прямоугольнике заготовки
        /// </summary>
        public int RECT_DATA_ID { get; set; }
        public virtual RectData RECTDATA { get; set; }

        /// <summary>
        /// Время расчёта
        /// </summary>
        public DateTime CALK_DT { get; set; }

    }
}
