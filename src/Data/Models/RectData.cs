﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Models
{
    /// <summary>
    /// Исходная термограмма заготовки
    /// </summary>
    public class RectData : BaseEntity
    {
        /// <summary>
        /// Изображение с выделенной заготовкой
        /// </summary>
        public byte[] PICT_RECT { get; set; }

        /// <summary>
        /// Список векторов внутри обнаруженной, повёрнутой заготовки
        /// </summary>
        public byte[] RECT_VECTORS { get; set; }

        /// <summary>
        /// Высота обнаруженной, повёрнутой заготовки, мм
        /// </summary>
        public float INGOT_HEIGHT { get; set; }

        /// <summary>
        /// Ширина обнаруженной, повёрнутой заготовки, мм
        /// </summary>
        public float INGOT_WIDTH { get; set; }
    }
}
