﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Models
{
    /// <summary>
    /// Одно сканирование
    /// </summary>
    public class Scan : BaseEntity
    {

        /// <summary>
        /// Номер пирометра
        /// </summary>
        public int PIR_NUMBER { get; set; }

        /// <summary>
        /// Номер заготовки
        /// </summary>
        public int MAT_ID { get; set; }

        /// <summary>
        /// Номер прохода при сканировании(для клети)
        /// </summary>
        public int PASS_NO { get; set; }

        /// <summary>
        /// Направление сканирования
        /// </summary>
        public bool FORWARD_DIRECTION { get; set; }

        /// <summary>
        /// Изображение исходного температурного поля
        /// </summary>
        public byte[] PICT_DEFAULT { get; set; }

        /// <summary>
        /// Дата время сканирования
        /// </summary>
        public DateTime DT_FIX { get; set; }

        /// <summary>
        /// Имя файла с исходными данными
        /// </summary>
        public string CSV_FILE_NAME { get; set; }
       
        /// <summary>
        /// Дата время из CSV файла
        /// </summary>
        public DateTime? DT_CSV { get; set; }


    }
}
