﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using PLC_S7_Exchange.Settings;
using S7.Net;

namespace PLC_S7_Exchange.Core
{
    public class PlcS7Exchange : IDataPlcExchange
    {
        private readonly PlcSettings _plcOption;
        private readonly ILogger _logger;
        private object _locker = new object();
        private byte[] ReadedBytes;

        public PlcS7Exchange(IOptions<PlcSettings> plcOption, ILogger<PlcS7Exchange> logger)
        {
            _logger = logger;
            _plcOption = plcOption.Value;
        }

        /// <summary>
        /// Чтение блока данных
        /// </summary>
        public async Task<byte[]> Read_DBAsync()
        {
            try
            {
                using (var plc = new Plc((CpuType)Enum.Parse(typeof(CpuType), _plcOption.PlcType), _plcOption.IPaddress, _plcOption.Rack, _plcOption.Slot))
                {
                    plc.Open();
                    ReadedBytes = null;
                    ReadedBytes = await plc.ReadBytesAsync(DataType.DataBlock, _plcOption.DbNum, _plcOption.StartAddr, _plcOption.BytesToRead);
                    plc.Close();
                    //if (ReadedBytes.Length == _plcOption.BytesToRead) return ReadedBytes;
                    //return null;
                    return ReadedBytes;
                }
            }
            catch (Exception ee)
            {
                //_logger.LogError("---------------> Exception event in Read_DB:" + ee.Message);
                return null;
            }
        }

        /// <summary>
        /// Запись блока данных
        /// </summary>
        /// <param name="DB_Num">Номер дата блока</param>
        /// <param name="Offsett">Смещение в блоке данных</param>
        /// <param name="db">Массив записываемых значений</param>
        /// <returns></returns>
        public int Write_DB(int DB_Num, int Offsett, byte[] db)
        {
            lock (_locker)
            {
                try
                {
                    using (var plc = new Plc((CpuType)Enum.Parse(typeof(CpuType), _plcOption.PlcType), _plcOption.IPaddress, _plcOption.Rack, _plcOption.Slot))
                    {
                        plc.Open();
                        plc.WriteBytes(DataType.DataBlock, DB_Num, Offsett, db);
                        plc.Close();
                    }
                    return 0;
                }
                catch (Exception ee)
                {
                    _logger.LogInformation("---------------> Exception event in Write_DB:" + ee.Message);
                    return 1;
                }
            }
        }

        /// <summary>
        /// Запись бита в блок данных
        /// </summary>
        /// <param name="DB_Num">Номер дата блока</param>
        /// <param name="startByteAdr">Смещение в блоке данных</param>
        /// <param name="bitAdr">Номер бита</param>
        /// <param name="value">Значение бита</param>
        /// <returns></returns>
        public int WriteBit(int DB_Num, int startByteAdr, int bitAdr, int value)
        {
            lock (_locker)
            {
                if (value < 0 || value > 1) throw new ArgumentException("Value must be 0 or 1", nameof(value));
                try
                {
                    using (var plc = new Plc((CpuType)Enum.Parse(typeof(CpuType), _plcOption.PlcType), _plcOption.IPaddress, _plcOption.Rack, _plcOption.Slot))
                    {
                        plc.Open();
                        plc.WriteBit(DataType.DataBlock, DB_Num, startByteAdr, bitAdr, value);
                        _logger.LogInformation("---------------> Bit {0}.{1} sended", startByteAdr, bitAdr);
                        plc.Close();
                    }
                    return 0;
                }
                catch (Exception ee)
                {
                    _logger.LogInformation("---------------> Exception event in WriteBit:" + ee.Message);
                    return 1;
                }
            }
        }


    }
}
