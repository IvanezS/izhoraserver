﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PLC_S7_Exchange.Core
{
    public interface IDataPlcExchange
    {
        //public bool ConnectPlc();
        //public bool DisConnectPlc();
        Task<byte[]> Read_DBAsync();
        int Write_DB(int DB_Num, int Offsett, byte[] db);
        int WriteBit(int DB_Num, int startByteAdr, int bitAdr, int value);
    }
}
