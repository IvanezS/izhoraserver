﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PLC_S7_Exchange.Settings
{
    public class PlcSettings
    {
        public string PlcType { get; set; }
        public string IPaddress { get; set; }
        public short Rack { get; set; }
        public short Slot { get; set; }
        public int DbNum { get; set; }
        public int StartAddr { get; set; }
        public int BytesToRead { get; set; }
        public int SendOffsetAddr { get; set; }
        public int BytesToSend { get; set; }
    }
}
