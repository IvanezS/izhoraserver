﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PLC_S7_Exchange.Data
{
    public class PiroDataBlock
    {
        public int WatchDog { get; set; }
        public List<Pir> pirlist { get; set; } = new List<Pir>();

        public PiroDataBlock()
        {
            pirlist.Add(new Pir());
            pirlist.Add(new Pir());
            pirlist.Add(new Pir());
            pirlist.Add(new Pir());
        }

        /// <summary>
        /// Статус Landscan 
        /// </summary>
        public short status { get; set; }


        /// <summary>
        /// Вотчдог счётчик наличия связи
        /// </summary>
        public short Counter { get; set; }

    }

    public class Pir
    {
        /// <summary>
        /// Сохранение данных
        /// </summary>
        public bool storageON { get; set; }
        
        /// <summary>
        /// Сдув
        /// </summary>
        public bool blowing { get; set; }
        
        /// <summary>
        /// Номер пирометра
        /// </summary>
        public short piro_id { get; set; } 
        
        /// <summary>
        /// Номер заготовки
        /// </summary>
        public int mat_id { get; set; }
        
        /// <summary>
        /// Номер прогона заготовки
        /// </summary>
        public short path_id { get; set; }
        
        /// <summary>
        /// Скорость прогона заготовки
        /// </summary>
        public float speed_path { get; set; }

        /// <summary>
        /// Наличие заготовки
        /// </summary>
        public short IsAProduct { get; set; }


        /// <summary>
        /// Пиковая температура пирометра (на отправку)
        /// </summary>
        public short t { get; set; }

        /// <summary>
        /// Ширина сляба (на отправку)
        /// </summary>
        public float w { get; set; }

    }


}